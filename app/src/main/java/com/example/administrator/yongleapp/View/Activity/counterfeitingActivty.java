package com.example.administrator.yongleapp.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.administrator.yongleapp.Base.BaseActivity;
import com.example.administrator.yongleapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class counterfeitingActivty extends BaseActivity {

    @Bind(R.id.imageView_back)
    ImageView imageViewBack;
    @Bind(R.id.editText_fangwei)
    EditText editTextFangwei;
    @Bind(R.id.button_chaxun)
    Button buttonChaxun;
    private Context mContext = this;
    @Override
    public int getLayoutId() {
        return R.layout.activity_counterfeiting_activty;
    }

    @Override
    public void initView() {
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        buttonChaxun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,NothingActivity.class);
                Bundle bundle = new Bundle();
                Editable string = editTextFangwei.getText();
                Log.i("<-->>","<<==>>"+string);
                bundle.putString("choose", String.valueOf(string));
                // 让intent携带包
                intent.putExtras(bundle);
                // 启动页面跳转
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
