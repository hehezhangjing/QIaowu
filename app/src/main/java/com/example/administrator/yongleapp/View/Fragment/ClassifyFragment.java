package com.example.administrator.yongleapp.View.Fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.administrator.yongleapp.Base.BaseFragment;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Fragment.zhangjing.AllFragment;
import com.example.administrator.yongleapp.adapter.ClassifyFragmentAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2016/7/4.
 * 分类页面
 */
public class ClassifyFragment extends BaseFragment {
    private List<Fragment> list = new ArrayList<>();
    private ClassifyFragmentAdapter adapter = null;


    @Bind(R.id.textView_classify)
    TextView textViewClassify;
    @Bind(R.id.tabLayout_classify)
    TabLayout tabLayoutClassify;
    @Bind(R.id.viewPager_classify)
    ViewPager viewPagerClassify;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_classify;
    }

    @Override
    public void initView() {
        String[] arrTabTitles = getResources().getStringArray(R.array.arrTabTitles);
        for (int i = 0; i < arrTabTitles.length; i++) {
            AllFragment fragment = AllFragment.newInstance(i);
            list.add(fragment);
        }
        adapter = new ClassifyFragmentAdapter(getChildFragmentManager(), list, arrTabTitles);
        viewPagerClassify.setAdapter(adapter);
        tabLayoutClassify.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayoutClassify.setupWithViewPager(viewPagerClassify);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.textView_classify)
    public void onClick() {
    }
}
