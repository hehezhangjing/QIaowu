package com.example.administrator.yongleapp.View.Activity.impl;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
	private List<Fragment> list = null;
 	private String[]arrTabTitle = null;
	public ViewPagerAdapter(FragmentManager fm, List<Fragment> list, String[] arrTabTitle) {
		super(fm);
		this.list = list;
		this.arrTabTitle = arrTabTitle;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return arrTabTitle[position];
	}
}
