package com.example.administrator.yongleapp.Presenter.liujiahe.zhangjing;

import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ZDetailsBean;
import com.example.administrator.yongleapp.Medol.liujiahe.zhangjing.ZDetailsModel;
import com.example.administrator.yongleapp.Medol.liujiahe.zhangjing.ZDetailsModelImpl;
import com.example.administrator.yongleapp.View.Fragment.zhangjing.ZDetails;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 19:02
 * 类描述：
 */
public class ZDetailsPresenterImpl implements ZDetailsPresenter{
    private ZDetails zDetails;
    private ZDetailsModel zDetailsModel;

    public ZDetailsPresenterImpl(ZDetails zDetails) {
        this.zDetails = zDetails;
        this.zDetailsModel = new ZDetailsModelImpl();
    }

    @Override
    public void loadInfo(String type_id) {
        zDetailsModel.loadInfo(type_id, new ZDetailsModelImpl.OnLoadInfoListListener() {
            @Override
            public void onSuccess(ZDetailsBean bean) {
                zDetails.addInfo(bean);
            }

            @Override
            public void onFailure(String msg, Exception ex) {

            }
        });
    }

}
