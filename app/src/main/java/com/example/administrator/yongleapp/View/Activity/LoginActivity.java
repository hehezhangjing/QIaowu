package com.example.administrator.yongleapp.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.yongleapp.Bean.liujiahe.UserBean;
import com.example.administrator.yongleapp.Presenter.liujiahe.UserPre;
import com.example.administrator.yongleapp.Presenter.liujiahe.impl.UserPreImpl;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.utils.Code;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * ----------BigGod be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━
 * Administrator on 2016/7/9.
 * Class description:登陆页面
 * Created by:qzn
 * Creation time:2016/7/9.
 * Modified by:qzn
 * Modified time:2016/7/9.
 * Modified remarks:
 */
public class LoginActivity extends AppCompatActivity {
    @Bind(R.id.imageView_login_returen)
    ImageView imageViewLoginReturen;
    @Bind(R.id.editText_username_login)
    EditText editTextUsernameLogin;
    @Bind(R.id.editText_psw_login)
    EditText editTextPswLogin;
    @Bind(R.id.editText_inputCode)
    EditText editTextInputCode;
    @Bind(R.id.imageView_code)
    ImageView imageViewCode;
    @Bind(R.id.textView_refCode)
    TextView textViewRefCode;
    @Bind(R.id.textView_regist)
    TextView textViewRegist;
    @Bind(R.id.textView_fogpsw)
    TextView textViewFogpsw;
    @Bind(R.id.textView_login)
    TextView textViewLogin;
    @Bind(R.id.textView)
    TextView textView;
    @Bind(R.id.linearLayout_thirdlogin_)
    LinearLayout linearLayoutThirdlogin;
    private UserPre userPre;
    private UserBean user;
    private Context context = this;
    private String getCode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initData();
        initView();
    }

    private void initView() {
        imageViewCode.setImageBitmap(Code.getInstance().getBitmap());
        getCode = Code.getInstance().getCode(); //获取显示的验证码
        textViewRefCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewCode.setImageBitmap(Code.getInstance().getBitmap());
                getCode = Code.getInstance().getCode();
            }
        });
        userPre = new UserPreImpl();
        user = new UserBean();
    }

    private void initData() {


    }


    @OnClick({R.id.imageView_login_returen, R.id.textView_refCode, R.id
            .textView_regist, R.id.textView_fogpsw, R.id.textView_login})
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {


            case R.id.imageView_login_returen:
                LoginActivity.this.finish();
                break;
            case R.id.textView_refCode:

                break;
            case R.id.textView_regist:
                intent.setClass(context, RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.textView_fogpsw:
                break;
            case R.id.textView_login:
                String code = editTextInputCode.getText() + "";
                if (code == null || code.equals("")) {
                    Toast.makeText(LoginActivity.this, "没有填写验证码", Toast.LENGTH_SHORT).show();
                } else if (!code.equals(getCode)) {
                    Toast.makeText(LoginActivity.this, "验证码填写不正确", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, "操作成功", Toast.LENGTH_SHORT).show();
                    String username = editTextUsernameLogin.getText() + "";
                    String psw = editTextPswLogin.getText() + "";
                    user.setUsername(username);
                    user.setPassword(psw);
                    userPre.login(user, context);

                }

                break;
        }
    }
}

