package com.example.administrator.yongleapp.Medol.liujiahe.zhangjing;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 13:41
 * 类描述：
 */
public interface AllModel {
    void loadInfo(String id,int pager,AllModelImpl.OnLoadInfoListListener listener);
}
