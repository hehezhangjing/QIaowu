package com.example.administrator.yongleapp.Bean.liujiahe.zhangjing;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：YongLeApp
 * BY：zzZ ON 2016/7/5 14:58
 * 类描述：
 */
public class ClassifyBean {

    /**
     * code : 0
     * message : 请求成功
     * bonus : 0
     * timestamp : 2016-07-05 14:15:23
     */

    @SerializedName("result")
    private ResultBean result;
    /**
     * page : {"nextPageNo":2,"totalPageCount":3,"lastPageNo":3,"totalCount":23,"pageNo":1,
     * "pageSize":10,"firstPageNo":1,"previousPageNo":1}
     * records : [{"isRobTicket":0,"status":0,"onlineSeatPath":"newonline-7258.html",
     * "finishDate":"2018-01-02","publishchannel":"a,b,c,d,f,g","angleNO":4,
     * "beginDate":"2018-01-01","productId":"144686757","newbegindate":null,"onlineseat":1,
     * "productMarkings":1,"name":"测试实名购票(请勿购买)",
     * "imgPath":"/upload/2016/07/01/AfterTreatment/1467340737724_s1m4-0.jpg","path":null,
     * "special":null,"venueName":"工人体育场","venueId":143638,"isrobseat":0,"minPrice":1},
     * {"isRobTicket":0,"status":1,"onlineSeatPath":null,"finishDate":"2016-07-31",
     * "publishchannel":"a,b,c,d,f,g,e","angleNO":5,"beginDate":"2016-07-29",
     * "productId":"141202443","newbegindate":null,"onlineseat":0,"productMarkings":0,
     * "name":"2016张北草原音乐节","imgPath":"/upload/2016/06/27/AfterTreatment/1467003375807_f6h7-0
     * .jpg","path":null,"special":null,"venueName":"张北中都原始草原度假村","venueId":45384503,
     * "isrobseat":0,"minPrice":170},{"isRobTicket":1,"status":0,"onlineSeatPath":"newonline-7358
     * .html","finishDate":"2016-08-20 20:00","publishchannel":"a,b,c,d","angleNO":3,
     * "beginDate":"2016-08-20 20:00","productId":"140957020","newbegindate":null,"onlineseat":1,
     * "productMarkings":1,"name":"测试商品【请勿购买】",
     * "imgPath":"/upload/2016/06/26/AfterTreatment/1466954499059_g1b2-0.jpg","path":null,
     * "special":null,"venueName":"乐视体育生态中心 LeSports Center","venueId":143719,"isrobseat":0,
     * "minPrice":1},{"isRobTicket":1,"status":0,"onlineSeatPath":"newonline-6347.html",
     * "finishDate":"2016-08-27 19:30","publishchannel":"a,b,c,d,f,g,e","angleNO":3,
     * "beginDate":"2016-08-27 19:30","productId":"128568679","newbegindate":null,"onlineseat":1,
     * "productMarkings":1,"name":"卓依婷依然记得演艺30周年巡回演唱会北京站",
     * "imgPath":"/upload/2016/06/08/AfterTreatment/1465350681885_d2n5-0.jpg","path":null,
     * "special":null,"venueName":"汇源空间","venueId":1645812,"isrobseat":0,"minPrice":550},
     * {"isRobTicket":1,"status":0,"onlineSeatPath":"newonline-5959.html",
     * "finishDate":"2016-09-03 19:30","publishchannel":"a,b,c,d,f,g,e","angleNO":3,
     * "beginDate":"2016-09-03 19:30","productId":"89133116","newbegindate":null,"onlineseat":1,
     * "productMarkings":1,"name":"周传雄时不知归世界巡回演唱会\u2014北京站",
     * "imgPath":"/upload/2016/03/02/AfterTreatment/1456908588690_x3n6-0.jpg","path":null,
     * "special":null,"venueName":"乐视体育生态中心 LeSports Center","venueId":143719,"isrobseat":0,
     * "minPrice":395},{"isRobTicket":1,"status":1,"onlineSeatPath":null,"finishDate":"2016-08-27
     * 19:30","publishchannel":"a,b,c,d,f,g","angleNO":3,"beginDate":"2016-08-27 19:30",
     * "productId":"146046285","newbegindate":null,"onlineseat":0,"productMarkings":0,"name":"陈伟霆
     * Inside Me 2016巡回演唱会\u2014北京站",
     * "imgPath":"/upload/2016/07/05/AfterTreatment/1467686996733_r6y4-0.jpg","path":null,
     * "special":null,"venueName":"乐视体育生态中心 LeSports Center","venueId":143719,"isrobseat":0,
     * "minPrice":380},{"isRobTicket":0,"status":0,"onlineSeatPath":null,"finishDate":"2016-08-05
     * 19:30","publishchannel":"a,b,c,d,f,g,e","angleNO":6,"beginDate":"2016-08-05 19:30",
     * "productId":"141211875","newbegindate":null,"onlineseat":0,"productMarkings":0,
     * "name":"2016 SUG 北京演唱会","imgPath":"/upload/2016/06/27/AfterTreatment/1467003633612_i9q0-0
     * .jpg","path":null,"special":null,"venueName":"糖果TANGO（原星光现场音乐厅）","venueId":143622,
     * "isrobseat":0,"minPrice":380},{"isRobTicket":1,"status":1,"onlineSeatPath":null,
     * "finishDate":"2016-07-17","publishchannel":"a,b,c,d","angleNO":3,"beginDate":"2016-07-15",
     * "productId":"137198814","newbegindate":null,"onlineseat":0,"productMarkings":0,"name":"映客
     * 2016 BIGBANG MADE [V.I.P] TOUR IN BEIJING",
     * "imgPath":"/upload/2016/06/30/AfterTreatment/1467275732831_s4q3-0.jpg","path":null,
     * "special":null,"venueName":"乐视体育生态中心 LeSports Center","venueId":143719,"isrobseat":0,
     * "minPrice":680},{"isRobTicket":1,"status":1,"onlineSeatPath":null,"finishDate":"2016-07-23
     * 19:30","publishchannel":"a,b,c,d,f,g","angleNO":3,"beginDate":"2016-07-23 19:30",
     * "productId":"134326913","newbegindate":null,"onlineseat":0,"productMarkings":0,
     * "name":"希天才秀\u2014金希澈北京见面会",
     * "imgPath":"/upload/2016/06/17/AfterTreatment/1466159869391_r2s9-0.jpg","path":null,
     * "special":null,"venueName":"北京奥体中心体育馆","venueId":11604700,"isrobseat":0,"minPrice":780},
     * {"isRobTicket":1,"status":1,"onlineSeatPath":null,"finishDate":"2016-10-29 19:30",
     * "publishchannel":"a,b,c,d,f,g","angleNO":3,"beginDate":"2016-10-29 19:30",
     * "productId":"133335333","newbegindate":null,"onlineseat":0,"productMarkings":0,"name":"
     * 蔡健雅2016巡回演唱会 列穆尼亚 LEMURIA\u2014北京站",
     * "imgPath":"/upload/2016/06/15/AfterTreatment/1465979242388_d7l8-0.jpg","path":null,
     * "special":null,"venueName":"首都体育馆","venueId":143817,"isrobseat":0,"minPrice":280}]
     */

    @SerializedName("data")
    private DataBean data;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ResultBean {
        @SerializedName("code")
        private int code;
        @SerializedName("message")
        private String message;
        @SerializedName("bonus")
        private int bonus;
        @SerializedName("timestamp")
        private String timestamp;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getBonus() {
            return bonus;
        }

        public void setBonus(int bonus) {
            this.bonus = bonus;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

    public static class DataBean {
        /**
         * nextPageNo : 2
         * totalPageCount : 3
         * lastPageNo : 3
         * totalCount : 23
         * pageNo : 1
         * pageSize : 10
         * firstPageNo : 1
         * previousPageNo : 1
         */

        @SerializedName("page")
        private PageBean page;
        /**
         * isRobTicket : 0
         * status : 0
         * onlineSeatPath : newonline-7258.html
         * finishDate : 2018-01-02
         * publishchannel : a,b,c,d,f,g
         * angleNO : 4
         * beginDate : 2018-01-01
         * productId : 144686757
         * newbegindate : null
         * onlineseat : 1
         * productMarkings : 1
         * name : 测试实名购票(请勿购买)
         * imgPath : /upload/2016/07/01/AfterTreatment/1467340737724_s1m4-0.jpg
         * path : null
         * special : null
         * venueName : 工人体育场
         * venueId : 143638
         * isrobseat : 0
         * minPrice : 1
         */

        @SerializedName("records")
        private List<RecordsBean> records;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class PageBean {
            @SerializedName("nextPageNo")
            private int nextPageNo;
            @SerializedName("totalPageCount")
            private int totalPageCount;
            @SerializedName("lastPageNo")
            private int lastPageNo;
            @SerializedName("totalCount")
            private int totalCount;
            @SerializedName("pageNo")
            private int pageNo;
            @SerializedName("pageSize")
            private int pageSize;
            @SerializedName("firstPageNo")
            private int firstPageNo;
            @SerializedName("previousPageNo")
            private int previousPageNo;

            public int getNextPageNo() {
                return nextPageNo;
            }

            public void setNextPageNo(int nextPageNo) {
                this.nextPageNo = nextPageNo;
            }

            public int getTotalPageCount() {
                return totalPageCount;
            }

            public void setTotalPageCount(int totalPageCount) {
                this.totalPageCount = totalPageCount;
            }

            public int getLastPageNo() {
                return lastPageNo;
            }

            public void setLastPageNo(int lastPageNo) {
                this.lastPageNo = lastPageNo;
            }

            public int getTotalCount() {
                return totalCount;
            }

            public void setTotalCount(int totalCount) {
                this.totalCount = totalCount;
            }

            public int getPageNo() {
                return pageNo;
            }

            public void setPageNo(int pageNo) {
                this.pageNo = pageNo;
            }

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public int getFirstPageNo() {
                return firstPageNo;
            }

            public void setFirstPageNo(int firstPageNo) {
                this.firstPageNo = firstPageNo;
            }

            public int getPreviousPageNo() {
                return previousPageNo;
            }

            public void setPreviousPageNo(int previousPageNo) {
                this.previousPageNo = previousPageNo;
            }
        }

        public static class RecordsBean {
            @SerializedName("isRobTicket")
            private int isRobTicket;
            @SerializedName("status")
            private int status;
            @SerializedName("onlineSeatPath")
            private String onlineSeatPath;
            @SerializedName("finishDate")
            private String finishDate;
            @SerializedName("publishchannel")
            private String publishchannel;
            @SerializedName("angleNO")
            private int angleNO;
            @SerializedName("beginDate")
            private String beginDate;
            @SerializedName("productId")
            private String productId;
            @SerializedName("newbegindate")
            private Object newbegindate;
            @SerializedName("onlineseat")
            private int onlineseat;
            @SerializedName("productMarkings")
            private int productMarkings;
            @SerializedName("name")
            private String name;
            @SerializedName("imgPath")
            private String imgPath;
            @SerializedName("path")
            private Object path;
            @SerializedName("special")
            private Object special;
            @SerializedName("venueName")
            private String venueName;
            @SerializedName("venueId")
            private int venueId;
            @SerializedName("isrobseat")
            private int isrobseat;
            @SerializedName("minPrice")
            private int minPrice;

            public int getIsRobTicket() {
                return isRobTicket;
            }

            public void setIsRobTicket(int isRobTicket) {
                this.isRobTicket = isRobTicket;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getOnlineSeatPath() {
                return onlineSeatPath;
            }

            public void setOnlineSeatPath(String onlineSeatPath) {
                this.onlineSeatPath = onlineSeatPath;
            }

            public String getFinishDate() {
                return finishDate;
            }

            public void setFinishDate(String finishDate) {
                this.finishDate = finishDate;
            }

            public String getPublishchannel() {
                return publishchannel;
            }

            public void setPublishchannel(String publishchannel) {
                this.publishchannel = publishchannel;
            }

            public int getAngleNO() {
                return angleNO;
            }

            public void setAngleNO(int angleNO) {
                this.angleNO = angleNO;
            }

            public String getBeginDate() {
                return beginDate;
            }

            public void setBeginDate(String beginDate) {
                this.beginDate = beginDate;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public Object getNewbegindate() {
                return newbegindate;
            }

            public void setNewbegindate(Object newbegindate) {
                this.newbegindate = newbegindate;
            }

            public int getOnlineseat() {
                return onlineseat;
            }

            public void setOnlineseat(int onlineseat) {
                this.onlineseat = onlineseat;
            }

            public int getProductMarkings() {
                return productMarkings;
            }

            public void setProductMarkings(int productMarkings) {
                this.productMarkings = productMarkings;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImgPath() {
                return imgPath;
            }

            public void setImgPath(String imgPath) {
                this.imgPath = imgPath;
            }

            public Object getPath() {
                return path;
            }

            public void setPath(Object path) {
                this.path = path;
            }

            public Object getSpecial() {
                return special;
            }

            public void setSpecial(Object special) {
                this.special = special;
            }

            public String getVenueName() {
                return venueName;
            }

            public void setVenueName(String venueName) {
                this.venueName = venueName;
            }

            public int getVenueId() {
                return venueId;
            }

            public void setVenueId(int venueId) {
                this.venueId = venueId;
            }

            public int getIsrobseat() {
                return isrobseat;
            }

            public void setIsrobseat(int isrobseat) {
                this.isrobseat = isrobseat;
            }

            public int getMinPrice() {
                return minPrice;
            }

            public void setMinPrice(int minPrice) {
                this.minPrice = minPrice;
            }
        }
    }
}
