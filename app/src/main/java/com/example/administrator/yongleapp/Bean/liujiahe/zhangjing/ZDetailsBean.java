package com.example.administrator.yongleapp.Bean.liujiahe.zhangjing;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/7 13:51
 * 类描述：
 */
public class ZDetailsBean {

    /**
     * code : 0
     * message : 请求成功
     * bonus : 0
     * timestamp : 2016-07-07 13:47:45
     */

    @SerializedName("result")
    private ResultBean result;
    /**
     * product : {"ifmaster":1,"robTicketTime":null,
     * "img":"/upload/2016/07/02/AfterTreatment/1467436599293_z4j1-0.jpg",
     * "isShowOnlineseatButton":true,"endDate":"2016-07-09 19:35","maxseatcount":6,
     * "beginDate":"2016-07-09 19:35","isShadeProduct":false,"isShowPaperBuyButton":false,
     * "newbegindate":null,"onlineseat":1,"shadeMessage":null,"pay_time_limit":15,"truebuy":true,
     * "trueNameBuyMessage":"应公安系统要求，本项目需要使用有效身份证件或护照购票","dztype":null,"status":0,"ifsystem":1,
     * "isSpeciateEticket":false,"isShowDaiDingRegistButton":false,"onlineSeatUrl":"/seat/load
     * .jsonp","insured":1,"isShowLjBuyButton":false,"isTheaterOnlineSeatProduct":false,
     * "buy_affiche":null,"favoriteMe":0,"onlineSeatPath":"newonline-6547.html","maxPrice":360,
     * "angleNO":4,"isShowEticketButton":false,"ifinvoiceinfo":0,"ifinvoice":1,
     * "introduction1":"温馨提示：请准备购票的球迷仔细阅读以下购票须知\u2014\u20141、本赛事不支持银行转帐的购票方式；2
     * 、为尽可能地保证公平，让更多球迷朋友买到心仪的门票，购票下单后15分钟内未支付将被取消订单回滚库存，大家可密切刷新网页进行购买；3
     * 、部分银行信用卡网上支付有限额，请大家提前跟卡片所属银行核实限额，以免耽误付款；4、本赛事实名制购票！为了确保广大球迷的利益，本场赛事门票每人、每张身份证或每张护照均限购1
     * 次，最多3张，注意：同一注册ID、同一收货地址及姓名、同一手机号、同一支付方式账号均视为同一人；对于异常订购行为，永乐票务有权取消相应订单；5
     * 、本场比赛购票仅支持上门自取，不支持快递配送及前台上门购票；请您在收到取票短信后，凭本人身份证到永乐前台取票，取票时间：周一至周日9:00\u201418:00
     * ，取票地址：北京市东城区王家园胡同16号中国儿童福利大厦西门1层（富华大厦向东50米路南）6、望观众朋友文明观赛、共享激情；7、1.2以下儿童谢绝观赛，1
     * .2以上儿童凭票入场。<br/>你豪取胜利，我们陪你君临天下；你艰难前行，我们陪你东山再起！<br/>永乐票务特携手轻停停车，送您免费停车券！<br/>1、活动时间",
     * "name":"2016赛季中超联赛 北京国安VS天津泰达","ticketAreaImg":null,"isLimit":1,"isrobseat":0,
     * "iforganizer":0,"limitCount":1,"circularProducts":[{"status":0,"name":"2016赛季中超联赛
     * 北京国安VS天津泰达","img":"/upload/2016/07/02/AfterTreatment/1467436599293_z4j1-0.jpg",
     * "pay_time_limit":15,"endDate":"2016-07-09","venue":{"address":"朝阳区三里屯工体北路工人体育场",
     * "name":"工人体育场","venuesId":143638,"coordinates":"116.45382,39.936813","city":"北京市"},
     * "beginDate":"2016-07-09","productId":145107813}],"isRobTicket":0,"shoppingcart":0,
     * "publishchannel":"a,b,c,d,f,g","venue":{"address":"朝阳区三里屯工体北路工人体育场","name":"工人体育场",
     * "venuesId":143638,"coordinates":"116.45382,39.936813","city":"北京市"},"transitPageUrl":null,
     * "integralMultiple":1,"buy_auota_policy":null,"productId":145107813,"iscomment":1,"pzlx":1,
     * "precautions":"a)演出详情仅供参考，具体信息以现场为准；<\/br>b)1.2米以下儿童谢绝入场，1.2米以上儿童需持票入场；<\/br>c)
     * 演出票品具有唯一性、时效性等特殊属性，如非活动变更、活动取消、票品错误的原因外，不提供退换票品服务，购票时请务必仔细核对并审慎下单。","special":null,
     * "minPrice":120}
     * asks : [{"EMAILPHONE":"135****8938","INFO":"你们这个取票码不会比赛当天发吧",
     * "ANSWERINFO":"亲，出票后系统自动发送取票码，请您注意查收。感谢支持，下载永乐APP尽享多彩生活！","CREATETIME":"2016-07-07
     * 11:49:00","ANSWERTIME":"2016-07-07 13:45:44","ROWNUM_":1},{"EMAILPHONE":"182****0582",
     * "INFO":"买不了了吗？","ANSWERINFO":"亲！有票哦，可正常购买。感谢支持，下载永乐APP尽享多彩生活！","CREATETIME":"2016-07-07
     * 08:57:24","ANSWERTIME":"2016-07-07 08:57:53","ROWNUM_":2},{"EMAILPHONE":"清凉一夏旧人归",
     * "INFO":"取票码还没发怎么办","ANSWERINFO":"亲！订单还没有打印好票纸，打印好会直接短信通知您的哦。感谢支持，下载永乐APP尽享多彩生活！",
     * "CREATETIME":"2016-07-06 18:25:31","ANSWERTIME":"2016-07-07 09:24:17","ROWNUM_":3},
     * {"EMAILPHONE":"A.👻Steven","INFO":"写\u201c发货中\u201d了的意思是就可以去取票了吗",
     * "ANSWERINFO":"亲！待您收到取票码之后才可以过去取票哦。感谢支持，下载永乐APP尽享多彩生活！","CREATETIME":"2016-07-06 17:58:40",
     * "ANSWERTIME":"2016-07-07 09:27:05","ROWNUM_":4},{"EMAILPHONE":"Farewell","INFO":"打不开啊",
     * "ANSWERINFO":"亲！乐乐尝试可以哦。感谢支持，下载永乐APP尽享多彩生活！","CREATETIME":"2016-07-06 14:09:31",
     * "ANSWERTIME":"2016-07-06 14:14:35","ROWNUM_":5}]
     * robSeatProduct : false
     * records : [{"timestamp":1451216100000,"time":"19:35","date":"2016-07-09"}]
     */

    @SerializedName("data")
    private DataBean data;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ResultBean {
        @SerializedName("code")
        private int code;
        @SerializedName("message")
        private String message;
        @SerializedName("bonus")
        private int bonus;
        @SerializedName("timestamp")
        private String timestamp;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getBonus() {
            return bonus;
        }

        public void setBonus(int bonus) {
            this.bonus = bonus;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

    public static class DataBean {
        /**
         * ifmaster : 1
         * robTicketTime : null
         * img : /upload/2016/07/02/AfterTreatment/1467436599293_z4j1-0.jpg
         * isShowOnlineseatButton : true
         * endDate : 2016-07-09 19:35
         * maxseatcount : 6
         * beginDate : 2016-07-09 19:35
         * isShadeProduct : false
         * isShowPaperBuyButton : false
         * newbegindate : null
         * onlineseat : 1
         * shadeMessage : null
         * pay_time_limit : 15
         * truebuy : true
         * trueNameBuyMessage : 应公安系统要求，本项目需要使用有效身份证件或护照购票
         * dztype : null
         * status : 0
         * ifsystem : 1
         * isSpeciateEticket : false
         * isShowDaiDingRegistButton : false
         * onlineSeatUrl : /seat/load.jsonp
         * insured : 1
         * isShowLjBuyButton : false
         * isTheaterOnlineSeatProduct : false
         * buy_affiche : null
         * favoriteMe : 0
         * onlineSeatPath : newonline-6547.html
         * maxPrice : 360
         * angleNO : 4
         * isShowEticketButton : false
         * ifinvoiceinfo : 0
         * ifinvoice : 1
         * introduction1 : 温馨提示：请准备购票的球迷仔细阅读以下购票须知——1、本赛事不支持银行转帐的购票方式；2
         * 、为尽可能地保证公平，让更多球迷朋友买到心仪的门票，购票下单后15分钟内未支付将被取消订单回滚库存，大家可密切刷新网页进行购买；3
         * 、部分银行信用卡网上支付有限额，请大家提前跟卡片所属银行核实限额，以免耽误付款；4、本赛事实名制购票！为了确保广大球迷的利益，本场赛事门票每人、每张身份证或每张护照均限购1
         * 次，最多3张，注意：同一注册ID、同一收货地址及姓名、同一手机号、同一支付方式账号均视为同一人；对于异常订购行为，永乐票务有权取消相应订单；5
         * 、本场比赛购票仅支持上门自取，不支持快递配送及前台上门购票；请您在收到取票短信后，凭本人身份证到永乐前台取票，取票时间：周一至周日9:00—18:00
         * ，取票地址：北京市东城区王家园胡同16号中国儿童福利大厦西门1层（富华大厦向东50米路南）6、望观众朋友文明观赛、共享激情；7、1.2以下儿童谢绝观赛，1
         * .2以上儿童凭票入场。<br/>你豪取胜利，我们陪你君临天下；你艰难前行，我们陪你东山再起！<br/>永乐票务特携手轻停停车，送您免费停车券！<br/>1、活动时间
         * name : 2016赛季中超联赛 北京国安VS天津泰达
         * ticketAreaImg : null
         * isLimit : 1
         * isrobseat : 0
         * iforganizer : 0
         * limitCount : 1
         * circularProducts : [{"status":0,"name":"2016赛季中超联赛 北京国安VS天津泰达",
         * "img":"/upload/2016/07/02/AfterTreatment/1467436599293_z4j1-0.jpg",
         * "pay_time_limit":15,"endDate":"2016-07-09","venue":{"address":"朝阳区三里屯工体北路工人体育场",
         * "name":"工人体育场","venuesId":143638,"coordinates":"116.45382,39.936813","city":"北京市"},
         * "beginDate":"2016-07-09","productId":145107813}]
         * isRobTicket : 0
         * shoppingcart : 0
         * publishchannel : a,b,c,d,f,g
         * venue : {"address":"朝阳区三里屯工体北路工人体育场","name":"工人体育场","venuesId":143638,
         * "coordinates":"116.45382,39.936813","city":"北京市"}
         * transitPageUrl : null
         * integralMultiple : 1.0
         * buy_auota_policy : null
         * productId : 145107813
         * iscomment : 1
         * pzlx : 1
         * precautions : a)演出详情仅供参考，具体信息以现场为准；</br>b)1.2米以下儿童谢绝入场，1.2米以上儿童需持票入场；</br>c)
         * 演出票品具有唯一性、时效性等特殊属性，如非活动变更、活动取消、票品错误的原因外，不提供退换票品服务，购票时请务必仔细核对并审慎下单。
         * special : null
         * minPrice : 120
         */

        @SerializedName("product")
        private ProductBean product;
        @SerializedName("robSeatProduct")
        private boolean robSeatProduct;
        /**
         * EMAILPHONE : 135****8938
         * INFO : 你们这个取票码不会比赛当天发吧
         * ANSWERINFO : 亲，出票后系统自动发送取票码，请您注意查收。感谢支持，下载永乐APP尽享多彩生活！
         * CREATETIME : 2016-07-07 11:49:00
         * ANSWERTIME : 2016-07-07 13:45:44
         * ROWNUM_ : 1
         */

        @SerializedName("asks")
        private List<AsksBean> asks;
        /**
         * timestamp : 1451216100000
         * time : 19:35
         * date : 2016-07-09
         */

        @SerializedName("records")
        private List<RecordsBean> records;

        public ProductBean getProduct() {
            return product;
        }

        public void setProduct(ProductBean product) {
            this.product = product;
        }

        public boolean isRobSeatProduct() {
            return robSeatProduct;
        }

        public void setRobSeatProduct(boolean robSeatProduct) {
            this.robSeatProduct = robSeatProduct;
        }

        public List<AsksBean> getAsks() {
            return asks;
        }

        public void setAsks(List<AsksBean> asks) {
            this.asks = asks;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class ProductBean {
            @SerializedName("ifmaster")
            private int ifmaster;
            @SerializedName("robTicketTime")
            private Object robTicketTime;
            @SerializedName("img")
            private String img;
            @SerializedName("isShowOnlineseatButton")
            private boolean isShowOnlineseatButton;
            @SerializedName("endDate")
            private String endDate;
            @SerializedName("maxseatcount")
            private int maxseatcount;
            @SerializedName("beginDate")
            private String beginDate;
            @SerializedName("isShadeProduct")
            private boolean isShadeProduct;
            @SerializedName("isShowPaperBuyButton")
            private boolean isShowPaperBuyButton;
            @SerializedName("newbegindate")
            private Object newbegindate;
            @SerializedName("onlineseat")
            private int onlineseat;
            @SerializedName("shadeMessage")
            private Object shadeMessage;
            @SerializedName("pay_time_limit")
            private int payTimeLimit;
            @SerializedName("truebuy")
            private boolean truebuy;
            @SerializedName("trueNameBuyMessage")
            private String trueNameBuyMessage;
            @SerializedName("dztype")
            private Object dztype;
            @SerializedName("status")
            private int status;
            @SerializedName("ifsystem")
            private int ifsystem;
            @SerializedName("isSpeciateEticket")
            private boolean isSpeciateEticket;
            @SerializedName("isShowDaiDingRegistButton")
            private boolean isShowDaiDingRegistButton;
            @SerializedName("onlineSeatUrl")
            private String onlineSeatUrl;
            @SerializedName("insured")
            private int insured;
            @SerializedName("isShowLjBuyButton")
            private boolean isShowLjBuyButton;
            @SerializedName("isTheaterOnlineSeatProduct")
            private boolean isTheaterOnlineSeatProduct;
            @SerializedName("buy_affiche")
            private Object buyAffiche;
            @SerializedName("favoriteMe")
            private int favoriteMe;
            @SerializedName("onlineSeatPath")
            private String onlineSeatPath;
            @SerializedName("maxPrice")
            private int maxPrice;
            @SerializedName("angleNO")
            private int angleNO;
            @SerializedName("isShowEticketButton")
            private boolean isShowEticketButton;
            @SerializedName("ifinvoiceinfo")
            private int ifinvoiceinfo;
            @SerializedName("ifinvoice")
            private int ifinvoice;
            @SerializedName("introduction1")
            private String introduction1;
            @SerializedName("name")
            private String name;
            @SerializedName("ticketAreaImg")
            private Object ticketAreaImg;
            @SerializedName("isLimit")
            private int isLimit;
            @SerializedName("isrobseat")
            private int isrobseat;
            @SerializedName("iforganizer")
            private int iforganizer;
            @SerializedName("limitCount")
            private int limitCount;
            @SerializedName("isRobTicket")
            private int isRobTicket;
            @SerializedName("shoppingcart")
            private int shoppingcart;
            @SerializedName("publishchannel")
            private String publishchannel;
            /**
             * address : 朝阳区三里屯工体北路工人体育场
             * name : 工人体育场
             * venuesId : 143638
             * coordinates : 116.45382,39.936813
             * city : 北京市
             */

            @SerializedName("venue")
            private VenueBean venue;
            @SerializedName("transitPageUrl")
            private Object transitPageUrl;
            @SerializedName("integralMultiple")
            private double integralMultiple;
            @SerializedName("buy_auota_policy")
            private Object buyAuotaPolicy;
            @SerializedName("productId")
            private int productId;
            @SerializedName("iscomment")
            private int iscomment;
            @SerializedName("pzlx")
            private int pzlx;
            @SerializedName("precautions")
            private String precautions;
            @SerializedName("special")
            private Object special;
            @SerializedName("minPrice")
            private int minPrice;
            /**
             * status : 0
             * name : 2016赛季中超联赛 北京国安VS天津泰达
             * img : /upload/2016/07/02/AfterTreatment/1467436599293_z4j1-0.jpg
             * pay_time_limit : 15
             * endDate : 2016-07-09
             * venue : {"address":"朝阳区三里屯工体北路工人体育场","name":"工人体育场","venuesId":143638,
             * "coordinates":"116.45382,39.936813","city":"北京市"}
             * beginDate : 2016-07-09
             * productId : 145107813
             */

            @SerializedName("circularProducts")
            private List<CircularProductsBean> circularProducts;

            public int getIfmaster() {
                return ifmaster;
            }

            public void setIfmaster(int ifmaster) {
                this.ifmaster = ifmaster;
            }

            public Object getRobTicketTime() {
                return robTicketTime;
            }

            public void setRobTicketTime(Object robTicketTime) {
                this.robTicketTime = robTicketTime;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public boolean isIsShowOnlineseatButton() {
                return isShowOnlineseatButton;
            }

            public void setIsShowOnlineseatButton(boolean isShowOnlineseatButton) {
                this.isShowOnlineseatButton = isShowOnlineseatButton;
            }

            public String getEndDate() {
                return endDate;
            }

            public void setEndDate(String endDate) {
                this.endDate = endDate;
            }

            public int getMaxseatcount() {
                return maxseatcount;
            }

            public void setMaxseatcount(int maxseatcount) {
                this.maxseatcount = maxseatcount;
            }

            public String getBeginDate() {
                return beginDate;
            }

            public void setBeginDate(String beginDate) {
                this.beginDate = beginDate;
            }

            public boolean isIsShadeProduct() {
                return isShadeProduct;
            }

            public void setIsShadeProduct(boolean isShadeProduct) {
                this.isShadeProduct = isShadeProduct;
            }

            public boolean isIsShowPaperBuyButton() {
                return isShowPaperBuyButton;
            }

            public void setIsShowPaperBuyButton(boolean isShowPaperBuyButton) {
                this.isShowPaperBuyButton = isShowPaperBuyButton;
            }

            public Object getNewbegindate() {
                return newbegindate;
            }

            public void setNewbegindate(Object newbegindate) {
                this.newbegindate = newbegindate;
            }

            public int getOnlineseat() {
                return onlineseat;
            }

            public void setOnlineseat(int onlineseat) {
                this.onlineseat = onlineseat;
            }

            public Object getShadeMessage() {
                return shadeMessage;
            }

            public void setShadeMessage(Object shadeMessage) {
                this.shadeMessage = shadeMessage;
            }

            public int getPayTimeLimit() {
                return payTimeLimit;
            }

            public void setPayTimeLimit(int payTimeLimit) {
                this.payTimeLimit = payTimeLimit;
            }

            public boolean isTruebuy() {
                return truebuy;
            }

            public void setTruebuy(boolean truebuy) {
                this.truebuy = truebuy;
            }

            public String getTrueNameBuyMessage() {
                return trueNameBuyMessage;
            }

            public void setTrueNameBuyMessage(String trueNameBuyMessage) {
                this.trueNameBuyMessage = trueNameBuyMessage;
            }

            public Object getDztype() {
                return dztype;
            }

            public void setDztype(Object dztype) {
                this.dztype = dztype;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getIfsystem() {
                return ifsystem;
            }

            public void setIfsystem(int ifsystem) {
                this.ifsystem = ifsystem;
            }

            public boolean isIsSpeciateEticket() {
                return isSpeciateEticket;
            }

            public void setIsSpeciateEticket(boolean isSpeciateEticket) {
                this.isSpeciateEticket = isSpeciateEticket;
            }

            public boolean isIsShowDaiDingRegistButton() {
                return isShowDaiDingRegistButton;
            }

            public void setIsShowDaiDingRegistButton(boolean isShowDaiDingRegistButton) {
                this.isShowDaiDingRegistButton = isShowDaiDingRegistButton;
            }

            public String getOnlineSeatUrl() {
                return onlineSeatUrl;
            }

            public void setOnlineSeatUrl(String onlineSeatUrl) {
                this.onlineSeatUrl = onlineSeatUrl;
            }

            public int getInsured() {
                return insured;
            }

            public void setInsured(int insured) {
                this.insured = insured;
            }

            public boolean isIsShowLjBuyButton() {
                return isShowLjBuyButton;
            }

            public void setIsShowLjBuyButton(boolean isShowLjBuyButton) {
                this.isShowLjBuyButton = isShowLjBuyButton;
            }

            public boolean isIsTheaterOnlineSeatProduct() {
                return isTheaterOnlineSeatProduct;
            }

            public void setIsTheaterOnlineSeatProduct(boolean isTheaterOnlineSeatProduct) {
                this.isTheaterOnlineSeatProduct = isTheaterOnlineSeatProduct;
            }

            public Object getBuyAffiche() {
                return buyAffiche;
            }

            public void setBuyAffiche(Object buyAffiche) {
                this.buyAffiche = buyAffiche;
            }

            public int getFavoriteMe() {
                return favoriteMe;
            }

            public void setFavoriteMe(int favoriteMe) {
                this.favoriteMe = favoriteMe;
            }

            public String getOnlineSeatPath() {
                return onlineSeatPath;
            }

            public void setOnlineSeatPath(String onlineSeatPath) {
                this.onlineSeatPath = onlineSeatPath;
            }

            public int getMaxPrice() {
                return maxPrice;
            }

            public void setMaxPrice(int maxPrice) {
                this.maxPrice = maxPrice;
            }

            public int getAngleNO() {
                return angleNO;
            }

            public void setAngleNO(int angleNO) {
                this.angleNO = angleNO;
            }

            public boolean isIsShowEticketButton() {
                return isShowEticketButton;
            }

            public void setIsShowEticketButton(boolean isShowEticketButton) {
                this.isShowEticketButton = isShowEticketButton;
            }

            public int getIfinvoiceinfo() {
                return ifinvoiceinfo;
            }

            public void setIfinvoiceinfo(int ifinvoiceinfo) {
                this.ifinvoiceinfo = ifinvoiceinfo;
            }

            public int getIfinvoice() {
                return ifinvoice;
            }

            public void setIfinvoice(int ifinvoice) {
                this.ifinvoice = ifinvoice;
            }

            public String getIntroduction1() {
                return introduction1;
            }

            public void setIntroduction1(String introduction1) {
                this.introduction1 = introduction1;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getTicketAreaImg() {
                return ticketAreaImg;
            }

            public void setTicketAreaImg(Object ticketAreaImg) {
                this.ticketAreaImg = ticketAreaImg;
            }

            public int getIsLimit() {
                return isLimit;
            }

            public void setIsLimit(int isLimit) {
                this.isLimit = isLimit;
            }

            public int getIsrobseat() {
                return isrobseat;
            }

            public void setIsrobseat(int isrobseat) {
                this.isrobseat = isrobseat;
            }

            public int getIforganizer() {
                return iforganizer;
            }

            public void setIforganizer(int iforganizer) {
                this.iforganizer = iforganizer;
            }

            public int getLimitCount() {
                return limitCount;
            }

            public void setLimitCount(int limitCount) {
                this.limitCount = limitCount;
            }

            public int getIsRobTicket() {
                return isRobTicket;
            }

            public void setIsRobTicket(int isRobTicket) {
                this.isRobTicket = isRobTicket;
            }

            public int getShoppingcart() {
                return shoppingcart;
            }

            public void setShoppingcart(int shoppingcart) {
                this.shoppingcart = shoppingcart;
            }

            public String getPublishchannel() {
                return publishchannel;
            }

            public void setPublishchannel(String publishchannel) {
                this.publishchannel = publishchannel;
            }

            public VenueBean getVenue() {
                return venue;
            }

            public void setVenue(VenueBean venue) {
                this.venue = venue;
            }

            public Object getTransitPageUrl() {
                return transitPageUrl;
            }

            public void setTransitPageUrl(Object transitPageUrl) {
                this.transitPageUrl = transitPageUrl;
            }

            public double getIntegralMultiple() {
                return integralMultiple;
            }

            public void setIntegralMultiple(double integralMultiple) {
                this.integralMultiple = integralMultiple;
            }

            public Object getBuyAuotaPolicy() {
                return buyAuotaPolicy;
            }

            public void setBuyAuotaPolicy(Object buyAuotaPolicy) {
                this.buyAuotaPolicy = buyAuotaPolicy;
            }

            public int getProductId() {
                return productId;
            }

            public void setProductId(int productId) {
                this.productId = productId;
            }

            public int getIscomment() {
                return iscomment;
            }

            public void setIscomment(int iscomment) {
                this.iscomment = iscomment;
            }

            public int getPzlx() {
                return pzlx;
            }

            public void setPzlx(int pzlx) {
                this.pzlx = pzlx;
            }

            public String getPrecautions() {
                return precautions;
            }

            public void setPrecautions(String precautions) {
                this.precautions = precautions;
            }

            public Object getSpecial() {
                return special;
            }

            public void setSpecial(Object special) {
                this.special = special;
            }

            public int getMinPrice() {
                return minPrice;
            }

            public void setMinPrice(int minPrice) {
                this.minPrice = minPrice;
            }

            public List<CircularProductsBean> getCircularProducts() {
                return circularProducts;
            }

            public void setCircularProducts(List<CircularProductsBean> circularProducts) {
                this.circularProducts = circularProducts;
            }

            public static class VenueBean {
                @SerializedName("address")
                private String address;
                @SerializedName("name")
                private String name;
                @SerializedName("venuesId")
                private int venuesId;
                @SerializedName("coordinates")
                private String coordinates;
                @SerializedName("city")
                private String city;

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public int getVenuesId() {
                    return venuesId;
                }

                public void setVenuesId(int venuesId) {
                    this.venuesId = venuesId;
                }

                public String getCoordinates() {
                    return coordinates;
                }

                public void setCoordinates(String coordinates) {
                    this.coordinates = coordinates;
                }

                public String getCity() {
                    return city;
                }

                public void setCity(String city) {
                    this.city = city;
                }
            }

            public static class CircularProductsBean {
                @SerializedName("status")
                private int status;
                @SerializedName("name")
                private String name;
                @SerializedName("img")
                private String img;
                @SerializedName("pay_time_limit")
                private int payTimeLimit;
                @SerializedName("endDate")
                private String endDate;
                /**
                 * address : 朝阳区三里屯工体北路工人体育场
                 * name : 工人体育场
                 * venuesId : 143638
                 * coordinates : 116.45382,39.936813
                 * city : 北京市
                 */

                @SerializedName("venue")
                private VenueBean venue;
                @SerializedName("beginDate")
                private String beginDate;
                @SerializedName("productId")
                private int productId;

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getImg() {
                    return img;
                }

                public void setImg(String img) {
                    this.img = img;
                }

                public int getPayTimeLimit() {
                    return payTimeLimit;
                }

                public void setPayTimeLimit(int payTimeLimit) {
                    this.payTimeLimit = payTimeLimit;
                }

                public String getEndDate() {
                    return endDate;
                }

                public void setEndDate(String endDate) {
                    this.endDate = endDate;
                }

                public VenueBean getVenue() {
                    return venue;
                }

                public void setVenue(VenueBean venue) {
                    this.venue = venue;
                }

                public String getBeginDate() {
                    return beginDate;
                }

                public void setBeginDate(String beginDate) {
                    this.beginDate = beginDate;
                }

                public int getProductId() {
                    return productId;
                }

                public void setProductId(int productId) {
                    this.productId = productId;
                }

                public static class VenueBean {
                    @SerializedName("address")
                    private String address;
                    @SerializedName("name")
                    private String name;
                    @SerializedName("venuesId")
                    private int venuesId;
                    @SerializedName("coordinates")
                    private String coordinates;
                    @SerializedName("city")
                    private String city;

                    public String getAddress() {
                        return address;
                    }

                    public void setAddress(String address) {
                        this.address = address;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public int getVenuesId() {
                        return venuesId;
                    }

                    public void setVenuesId(int venuesId) {
                        this.venuesId = venuesId;
                    }

                    public String getCoordinates() {
                        return coordinates;
                    }

                    public void setCoordinates(String coordinates) {
                        this.coordinates = coordinates;
                    }

                    public String getCity() {
                        return city;
                    }

                    public void setCity(String city) {
                        this.city = city;
                    }
                }
            }
        }

        public static class AsksBean {
            @SerializedName("EMAILPHONE")
            private String EMAILPHONE;
            @SerializedName("INFO")
            private String INFO;
            @SerializedName("ANSWERINFO")
            private String ANSWERINFO;
            @SerializedName("CREATETIME")
            private String CREATETIME;
            @SerializedName("ANSWERTIME")
            private String ANSWERTIME;
            @SerializedName("ROWNUM_")
            private int ROWNUM;

            public String getEMAILPHONE() {
                return EMAILPHONE;
            }

            public void setEMAILPHONE(String EMAILPHONE) {
                this.EMAILPHONE = EMAILPHONE;
            }

            public String getINFO() {
                return INFO;
            }

            public void setINFO(String INFO) {
                this.INFO = INFO;
            }

            public String getANSWERINFO() {
                return ANSWERINFO;
            }

            public void setANSWERINFO(String ANSWERINFO) {
                this.ANSWERINFO = ANSWERINFO;
            }

            public String getCREATETIME() {
                return CREATETIME;
            }

            public void setCREATETIME(String CREATETIME) {
                this.CREATETIME = CREATETIME;
            }

            public String getANSWERTIME() {
                return ANSWERTIME;
            }

            public void setANSWERTIME(String ANSWERTIME) {
                this.ANSWERTIME = ANSWERTIME;
            }

            public int getROWNUM() {
                return ROWNUM;
            }

            public void setROWNUM(int ROWNUM) {
                this.ROWNUM = ROWNUM;
            }
        }

        public static class RecordsBean {
            @SerializedName("timestamp")
            private long timestamp;
            @SerializedName("time")
            private String time;
            @SerializedName("date")
            private String date;

            public long getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(long timestamp) {
                this.timestamp = timestamp;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }
        }
    }
}
