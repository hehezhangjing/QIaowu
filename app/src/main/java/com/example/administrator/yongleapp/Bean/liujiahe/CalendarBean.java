package com.example.administrator.yongleapp.Bean.liujiahe;

import java.util.List;

/**
 * Created by Administrator on 2016/7/6.
 */
public class CalendarBean {

    /**
     * code : 0
     * message : 请求成功
     * bonus : 0
     * timestamp : 2016-07-05 09:12:39
     */

    private ResultBean result;
    /**
     * productCalendar : [{"count":0,"day":"2016-06-21"},{"count":4,"day":"2014-09-10"},{"count":2,"day":"2014-09-11"},{"count":0,"day":"2016-08-07"}]
     * page : {"nextPageNo":2,"totalPageCount":6,"lastPageNo":6,"totalCount":53,"pageNo":1,"pageSize":10,"firstPageNo":1,"previousPageNo":1}
     * records : [{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/22/AfterTreatment/1466558687337_e8p8-0.jpg","name":"北京人民艺术剧院演出话剧：《晚餐》","finishDate":"2016-07-25","special":null,"beginDate":"2016-07-08","angleNO":6,"venueId":143816,"venueName":"人艺实验剧场","minPrice":60,"productId":"137466686"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/07/AfterTreatment/1465280618578_m6z3-0.jpg","name":"开心一夏盛夏戏剧嘉年华 开心麻花邀请剧目《如果\u2022我不是我》","finishDate":"2016-07-24","special":null,"beginDate":"2016-07-08","angleNO":4,"venueId":52098097,"venueName":"A33剧场","minPrice":50,"productId":"128217567"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/28/AfterTreatment/1467096589263_f6l3-0.jpg","name":"愤怒的小鸟嘉年华","finishDate":"2016-08-05","special":null,"beginDate":"2016-07-08","angleNO":6,"venueId":47660764,"venueName":"悠唐购物中心","minPrice":100,"productId":"141811859"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/13/AfterTreatment/1465805050996_j1p9-0.jpg","name":"首都剧场60年纪念演出|2016首都剧场精品剧目邀请展演 话剧：《大教堂》","finishDate":"2016-07-10","special":null,"beginDate":"2016-07-08","angleNO":6,"venueId":143648,"venueName":"首都剧场","minPrice":40,"productId":"131436334"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/21/AfterTreatment/1466499070627_j5z7-0.jpg","name":"\u201c拳力前行\u201dWBO职业拳击系列赛","finishDate":"2016-11-05","special":"【温馨提示】电子票验证地点：北一门入口。","beginDate":"2016-07-08","angleNO":4,"venueId":143709,"venueName":"国家游泳中心\u2014水立方","minPrice":100,"productId":"131103994"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/13/AfterTreatment/1465790272401_v0n7-0.jpg","name":"爱乐汇\u20222016贝肯熊亲子音乐剧《今天,运气真好!!》\u2014\u2014那个熊不再倒霉？","finishDate":"2016-07-09","special":"【特惠信息】即日起购买本场演出门票即可享受五折优惠(80、100价位除外)","beginDate":"2016-07-08","angleNO":6,"venueId":481533,"venueName":"中国人民大学","minPrice":80,"productId":"131309855"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/03/08/AfterTreatment/1457431541436_o8d8-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会北京站","finishDate":"2016-07-10","special":"【温馨提示】由于演出火爆，第一波门票现已售完，乐乐正在抓紧沟通补票事宜，请您做好缺货登记，后期补票会第一时间以短信或电话的形式通知您，感谢您的配合。","beginDate":"2016-07-08","angleNO":3,"venueId":143719,"venueName":"乐视体育生态中心 LeSports Center","minPrice":580,"productId":"83027246"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/22/AfterTreatment/1461296572654_r3r4-0.jpg","name":"儿童剧《马兰花》","finishDate":"2016-07-10","special":"【超值套票】仅￥180元，即可购买价值200元双人套票(100*2)<br/>【超值套票】仅￥260元，即可购买价值300元家庭套票(100*3)<br/>【超值套票】仅￥340元，即可购买价值360元双人套票(180*2)<br/>【超值套票】仅￥500元，即可购买价值540元家庭套票(180*3)","beginDate":"2016-07-08","angleNO":6,"venueId":143618,"venueName":"中国儿童艺术剧院","minPrice":50,"productId":"105144158"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/21/AfterTreatment/1461229689178_j5l8-0.jpg","name":"2016第三届城市戏剧节\u2022北京站-悬疑推理剧《安的秘密》","finishDate":"2016-07-10","special":null,"beginDate":"2016-07-08","angleNO":6,"venueId":45913662,"venueName":"中华世纪坛","minPrice":99,"productId":"104966414"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/05/AfterTreatment/1462436519636_f1k4-0.jpg","name":"央华\u20222016赖声川导演戏剧作品演出季舞台剧《宝岛一村》","finishDate":"2016-07-09","special":"【超值套票】仅￥1000元，即可购买价值1160元双人套票(580*2)<br/>【超值套票】仅￥1500元，即可购买价值1760元双人套票(880*2)","beginDate":"2016-07-08","angleNO":6,"venueId":143645,"venueName":"保利剧院","minPrice":180,"productId":"110008397"}]
     */

    private DataBean data;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ResultBean {
        private int code;
        private String message;
        private int bonus;
        private String timestamp;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getBonus() {
            return bonus;
        }

        public void setBonus(int bonus) {
            this.bonus = bonus;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

    public static class DataBean {
        /**
         * nextPageNo : 2
         * totalPageCount : 6
         * lastPageNo : 6
         * totalCount : 53
         * pageNo : 1
         * pageSize : 10
         * firstPageNo : 1
         * previousPageNo : 1
         */

        private PageBean page;
        /**
         * count : 0
         * day : 2016-06-21
         */

        private List<ProductCalendarBean> productCalendar;
        /**
         * status : 0
         * newbegindate : null
         * productMarkings : 0
         * imgPath : /upload/2016/06/22/AfterTreatment/1466558687337_e8p8-0.jpg
         * name : 北京人民艺术剧院演出话剧：《晚餐》
         * finishDate : 2016-07-25
         * special : null
         * beginDate : 2016-07-08
         * angleNO : 6
         * venueId : 143816
         * venueName : 人艺实验剧场
         * minPrice : 60
         * productId : 137466686
         */

        private List<RecordsBean> records;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<ProductCalendarBean> getProductCalendar() {
            return productCalendar;
        }

        public void setProductCalendar(List<ProductCalendarBean> productCalendar) {
            this.productCalendar = productCalendar;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class PageBean {
            private int nextPageNo;
            private int totalPageCount;
            private int lastPageNo;
            private int totalCount;
            private int pageNo;
            private int pageSize;
            private int firstPageNo;
            private int previousPageNo;

            public int getNextPageNo() {
                return nextPageNo;
            }

            public void setNextPageNo(int nextPageNo) {
                this.nextPageNo = nextPageNo;
            }

            public int getTotalPageCount() {
                return totalPageCount;
            }

            public void setTotalPageCount(int totalPageCount) {
                this.totalPageCount = totalPageCount;
            }

            public int getLastPageNo() {
                return lastPageNo;
            }

            public void setLastPageNo(int lastPageNo) {
                this.lastPageNo = lastPageNo;
            }

            public int getTotalCount() {
                return totalCount;
            }

            public void setTotalCount(int totalCount) {
                this.totalCount = totalCount;
            }

            public int getPageNo() {
                return pageNo;
            }

            public void setPageNo(int pageNo) {
                this.pageNo = pageNo;
            }

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public int getFirstPageNo() {
                return firstPageNo;
            }

            public void setFirstPageNo(int firstPageNo) {
                this.firstPageNo = firstPageNo;
            }

            public int getPreviousPageNo() {
                return previousPageNo;
            }

            public void setPreviousPageNo(int previousPageNo) {
                this.previousPageNo = previousPageNo;
            }
        }

        public static class ProductCalendarBean {
            private int count;
            private String day;

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }
        }

        public static class RecordsBean {
            private int status;
            private Object newbegindate;
            private int productMarkings;
            private String imgPath;
            private String name;
            private String finishDate;
            private Object special;
            private String beginDate;
            private int angleNO;
            private int venueId;
            private String venueName;
            private int minPrice;
            private String productId;

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public Object getNewbegindate() {
                return newbegindate;
            }

            public void setNewbegindate(Object newbegindate) {
                this.newbegindate = newbegindate;
            }

            public int getProductMarkings() {
                return productMarkings;
            }

            public void setProductMarkings(int productMarkings) {
                this.productMarkings = productMarkings;
            }

            public String getImgPath() {
                return imgPath;
            }

            public void setImgPath(String imgPath) {
                this.imgPath = imgPath;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getFinishDate() {
                return finishDate;
            }

            public void setFinishDate(String finishDate) {
                this.finishDate = finishDate;
            }

            public Object getSpecial() {
                return special;
            }

            public void setSpecial(Object special) {
                this.special = special;
            }

            public String getBeginDate() {
                return beginDate;
            }

            public void setBeginDate(String beginDate) {
                this.beginDate = beginDate;
            }

            public int getAngleNO() {
                return angleNO;
            }

            public void setAngleNO(int angleNO) {
                this.angleNO = angleNO;
            }

            public int getVenueId() {
                return venueId;
            }

            public void setVenueId(int venueId) {
                this.venueId = venueId;
            }

            public String getVenueName() {
                return venueName;
            }

            public void setVenueName(String venueName) {
                this.venueName = venueName;
            }

            public int getMinPrice() {
                return minPrice;
            }

            public void setMinPrice(int minPrice) {
                this.minPrice = minPrice;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }
        }
    }
}
