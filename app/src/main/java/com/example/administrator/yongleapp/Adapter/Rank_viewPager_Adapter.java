package com.example.administrator.yongleapp.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Administrator on 2016/5/19.
 */
public class Rank_viewPager_Adapter extends FragmentPagerAdapter {
    private List<Fragment>list=null;
private String[] arrtab=null;
    public Rank_viewPager_Adapter(FragmentManager fm, List<Fragment> list, String[] arrtab) {
        super(fm);
        this.list=list;
        this.arrtab=arrtab;
    }
    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }
    @Override
    public int getCount() {
        return arrtab.length;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return arrtab[position];
    }
    public void load(List<Fragment> _list,String[] _arrtab){
        this.list=_list;
        this.arrtab=arrtab;
        notifyDataSetChanged();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }
}
