package com.example.administrator.yongleapp.utils;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：YongLeApp
 * BY：zzZ ON 2016/7/5 14:15
 * 类描述：
 */
public class Constant {
    //全部分类url
    public final  static String URL_ALL = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=&time_range=&sort_type=1&page_size=10&page_no=1&nc=300";
    //演唱会url
    public final  static String URL_CONCERT = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=142450&time_range=&sort_type=1&page_size=10&page_no=%d&nc=300";
    //话剧舞台剧url
    public final  static String URL_THEATRE = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=142452&time_range=&sort_type=1&page_size=10&page_no=%d&nc=300";
    //音乐会url
    public final  static String URL_MUSIC = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=142452&time_range=&sort_type=1&page_size=10&page_no=%d&nc=300";
    //舞蹈芭蕾url
    public final  static String URL_DANCE = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=142453&time_range=&sort_type=1&page_size=10&page_no=%d&nc=300";
    //戏曲文艺url
    public final  static String URL_DRAMAR = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=142454&time_range=&sort_type=1&page_size=10&page_no=%d&nc=300";
    //体育赛事url
    public final  static String URL_SPORTS = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=142455&time_range=&sort_type=1&page_size=10&page_no=%d&nc=300";
    //儿童亲子url
    public final  static String URL_CHILD = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=142458&time_range=&sort_type=1&page_size=10&page_no=%d&nc=300";
    //休闲娱乐url
    public final  static String URL_PLAY = "http://api.228.cn/products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&product_category_id=142456&time_range=&sort_type=1&page_size=10&page_no=%d&nc=300";

    //详情
    public final static String URL_INFO = "http://api.228.cn/products/145107813?access_phone_type=android&app_version=My4yLjM%3D&token=&res_type=2";

    //基类
    public final static String URL_BASE = "http://api.228.cn/";
}
