package com.example.administrator.yongleapp.Presenter.liujiahe.impl;

import android.content.Context;
import android.widget.Toast;

import com.example.administrator.yongleapp.Bean.liujiahe.UserBean;
import com.example.administrator.yongleapp.Medol.liujiahe.UserModel;
import com.example.administrator.yongleapp.Medol.liujiahe.UserModelImpl;
import com.example.administrator.yongleapp.Presenter.liujiahe.UserPre;

/**
 * ----------BigGod be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━
 * Administrator on 2016/7/9.
 * Class description
 * Created by:qzn
 * Creation time:2016/7/9.
 * Modified by:qzn
 * Modified time:2016/7/9.
 * Modified remarks:
 */
public class UserPreImpl implements UserPre {
    private UserModel userModel;

    public UserPreImpl() {
        this.userModel = new UserModelImpl();
    }

    @Override
    public void login(UserBean user, final Context context) {
        userModel.login(user, context, new UserModelImpl.UserLitster() {
            @Override
            public void onSucc() {
                Toast.makeText(context, "登陆成功", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail() {
                Toast.makeText(context, "登陆失败", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void regist(UserBean user, final Context context) {
        userModel.register(user, context, new UserModelImpl.UserLitster() {
            @Override
            public void onSucc() {
                Toast.makeText(context, "注册成功", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFail() {
                Toast.makeText(context, "注册失败", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
