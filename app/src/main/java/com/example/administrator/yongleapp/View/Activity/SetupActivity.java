package com.example.administrator.yongleapp.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.administrator.yongleapp.Base.BaseActivity;
import com.example.administrator.yongleapp.R;
import com.xys.libzxing.zxing.activity.CaptureActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SetupActivity extends BaseActivity {

    @Bind(R.id.imageView_back)
    ImageView imageViewBack;
    @Bind(R.id.listView_main)
    ListView listViewMain;
    @Bind(R.id.listView_service)
    ListView listViewService;
    private Context mContent = this;
    private SimpleAdapter adapter = null;
    private List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    private SimpleAdapter adapter1 = null;
    private List<Map<String, Object>> list1 = new ArrayList<Map<String, Object>>();
    @Override
    public int getLayoutId() {
        return R.layout.activity_setup;
    }

    @Override
    protected void onResume() {
        super.onResume();
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void initView() {
        final String[] str = new String[]{"验票", "防伪编码查询", "意见反馈", "关于永乐APP", "新手指导", "公告"};
        for (int i = 0; i < str.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("username", str[i]);
            list.add(map);
        }
        adapter = new SimpleAdapter(mContent, list,
                R.layout.setupactivity_item, new String[]{"username"}, new int[]{
                R.id.textViewmingzi});
        listViewMain.setAdapter(adapter);
        listViewMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Map<String, Object> map = list.get(position);
                String data = (String) map.get("username");
                if (data.equals("验票")) {
                    //二维码扫描
                    startActivityForResult(new Intent(mContent, CaptureActivity.class), 0);
                }else if (data.equals("防伪编码查询")){
                    Intent intent = new Intent(mContent,counterfeitingActivty.class);
                    startActivity(intent);
                }

                Toast.makeText(mContent, data, Toast.LENGTH_SHORT).show();
            }
        });
        initView1();
    }

    public void initView1() {
        String[] str = new String[]{"400客服电话咨询"};
        for (int i = 0; i < str.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("username", str[i]);
            list1.add(map);
        }
        adapter1 = new SimpleAdapter(mContent, list1,
                R.layout.service_item, new String[]{"username"}, new int[]{
                R.id.textView_service});
        listViewService.setAdapter(adapter1);
        listViewService.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Uri uri = Uri.parse("tel:15010038680");
                Intent it = new Intent(Intent.ACTION_DIAL, uri);
                startActivity(it);
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }
    //二维码扫描完以后的一个回调方法
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            Bundle bundle = data.getExtras();

            Intent intent = new Intent(mContent,TcketChecking.class);
            // 定义携带数据的包对象
            Bundle bundle1 = new Bundle();
            // 往包中放置数据
            bundle1.putString("choose_first",bundle.getString("result"));
            // 让intent携带包
            intent.putExtras(bundle1);
            // 启动页面跳转
            startActivity(intent);
        }
    }
}
