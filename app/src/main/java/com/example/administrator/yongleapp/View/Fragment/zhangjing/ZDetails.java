package com.example.administrator.yongleapp.View.Fragment.zhangjing;

import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ZDetailsBean;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 19:05
 * 类描述：
 */
public interface ZDetails {
    void addInfo(ZDetailsBean infoBean);
}
