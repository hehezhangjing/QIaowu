package com.example.administrator.yongleapp.View.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.yongleapp.Base.BaseFragment;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Activity.SetupActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ActivityFragment extends BaseFragment {
    @Bind(R.id.textView_empty)
    TextView textViewEmpty;
    @Bind(R.id.listView_main1)
    ListView listViewMain1;
    @Bind(R.id.listView_main2)
    ListView listViewMain2;
    @Bind(R.id.imageView_touxiang)
    ImageView imageViewTouxiang;
    @Bind(R.id.textView_shezhi)
    TextView textViewShezhi;
    @Bind(R.id.listView_main)
    ListView listViewMain;
    private SimpleAdapter adapter = null;
    private SimpleAdapter adapter1 = null;
    private SimpleAdapter adapter2 = null;
    private List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> list1 = new ArrayList<Map<String, Object>>();


    @Override
    public int getLayoutId() {
        return R.layout.frangment_film;
    }

    @Override
    public void onResume() {
        super.onResume();
        textViewShezhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SetupActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void initView() {
        String[] str = new String[]{"我的订单", "现金劵"};
        int[] str1 = new int[]{R.drawable.userinfo01, R.drawable.userinfo03};
        for (int i = 0; i < str.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("username", str[i]);
            map.put("imgId", str1[i]);
            list.add(map);
        }
        adapter = new SimpleAdapter(getContext(), list,
                R.layout.item_listview_main, new String[]{"username", "imgId"}, new int[]{
                R.id.mingzi,
                R.id.imageViewtupain});
        listViewMain.setAdapter(adapter);
        listViewMain.setEmptyView(textViewEmpty);
        listViewMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Map<String, Object> map = list.get(position);
                String data = map.get("username") + ":" + map.get("phone")
                        + ":" + map.get("imp");
                Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();
            }
        });
        initView1();
    }

    private void initView1() {
        String[] str = new String[]{"我的收藏", "我的提问", "缺货登记", "艺人订阅"};
        int[] icon = new int[]{R.drawable.userinfo04, R.drawable.userinfo05, R.drawable.userinfo06, R.drawable.icon_main_yirendingyue};
        for (int i = 0; i < str.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("username", str[i]);
            map.put("imgId", icon[i]);
            list1.add(map);
        }
        adapter1 = new SimpleAdapter(getContext(), list1,
                R.layout.item_listview_main, new String[]{"username", "imgId"}, new int[]{
                R.id.mingzi,
                R.id.imageViewtupain});
        listViewMain1.setAdapter(adapter1);
        listViewMain1.setEmptyView(textViewEmpty);
        listViewMain1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Map<String, Object> map = list1.get(position);
                String data = map.get("username") + ":" + map.get("phone")
                        + ":" + map.get("imp");
                Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();
            }
        });
        initView2();
    }

    private void initView2() {
        String[] str = new String[]{"收货地址"};
        int[] icon = new int[]{R.drawable.dizhi};
        for (int i = 0; i < str.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("username", str[i]);
            map.put("imgId", icon[i]);
            list2.add(map);
        }
        adapter2 = new SimpleAdapter(getContext(), list2,
                R.layout.item_listview_main, new String[]{"username", "imgId"}, new int[]{
                R.id.mingzi,
                R.id.imageViewtupain});
        listViewMain2.setAdapter(adapter2);
        listViewMain2.setEmptyView(textViewEmpty);
        listViewMain2.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Map<String, Object> map = list2.get(position);
                String data = map.get("username") + ":" + map.get("phone")
                        + ":" + map.get("imp");
                Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);


    }
}
