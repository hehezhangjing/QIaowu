package com.example.administrator.yongleapp.View.Activity.impl;

/**
 * Created by Administrator on 2016/7/6.
 */
public interface ActivityImpl<T> {
    void onSuccess(T bean);
    void onFailure();
}
