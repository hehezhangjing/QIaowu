package com.example.administrator.yongleapp.Adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.yongleapp.Base.BaseRcAdapterHelper;
import com.example.administrator.yongleapp.Base.BaseRcQuickAdapter;
import com.example.administrator.yongleapp.Bean.liujiahe.RankBean;
import com.example.administrator.yongleapp.Bean.liujiahe.RecommendBean;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.utils.liujiaheConstant;

/**
 * Created by Administrator on 2016/7/6.
 */
public class Rank_listview_Adapter extends BaseRcQuickAdapter<RankBean.DataBean.RankingListBean, BaseRcAdapterHelper> {

    public Rank_listview_Adapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    protected void convert(BaseRcAdapterHelper helper, RankBean.DataBean.RankingListBean item) {
      ImageView imageView=  helper.getImageView(R.id.image_recommend_list);
       TextView textView1=helper.getTextView(R.id.Text1_recommend_list);
        TextView textView2=helper.getTextView(R.id.Text2_recommend_list);
        TextView textView3=helper.getTextView(R.id.Text3_recommend_list);
        TextView textView4=helper.getTextView(R.id.Text4_recommend_list);

        textView1.setText(item.getName());
        if (item.getBeginDate().equals(item.getFinishDate())) {
            textView2.setText(item.getBeginDate());
        }
        else{
            textView2.setText(item.getBeginDate()+"至"+item.getFinishDate());
        }
       textView3.setText(item.getVenueName());
        textView4.setText(item.getMinPrice() + "元起");
        Glide.with(context).load(liujiaheConstant.URL+ item.getImgPath())
                .placeholder(R.drawable.img_product_default_bg)
                .error(R.drawable.img_product_default_bg)
                .into(imageView);
    }
}
