package com.example.administrator.yongleapp.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.administrator.yongleapp.R;
import com.xys.libzxing.zxing.activity.CaptureActivity;


import butterknife.Bind;
import butterknife.ButterKnife;

public class ThinkChange extends AppCompatActivity {

    @Bind(R.id.thinkChange)
    Button thinkChange;
    @Bind(R.id.textViewjieguo)
    TextView textViewjieguo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_think_change);
        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            Bundle bundle = data.getExtras();
            String result = bundle.getString("result");
            textViewjieguo.setText(result);
        }
    }

    public void clickView(View view) {
        startActivityForResult(new Intent(ThinkChange.this, CaptureActivity.class),0);
    }
}
