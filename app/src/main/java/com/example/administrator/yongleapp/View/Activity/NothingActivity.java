package com.example.administrator.yongleapp.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.yongleapp.Base.BaseActivity;
import com.example.administrator.yongleapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NothingActivity extends BaseActivity {

    @Bind(R.id.imageView_back)
    ImageView imageViewBack;
    @Bind(R.id.bianma_textView)
    TextView bianmaTextView;
    @Bind(R.id.button)
    Button button;
    private Context mContext = this;
    @Override
    public int getLayoutId() {
        return R.layout.activity_nothing;
    }


    @Override
    public void initView() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String choose_first = bundle.getString("choose");
        bianmaTextView.setText(choose_first);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,counterfeitingActivty.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
