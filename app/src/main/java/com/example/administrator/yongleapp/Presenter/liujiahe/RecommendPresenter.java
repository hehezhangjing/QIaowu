package com.example.administrator.yongleapp.Presenter.liujiahe;

import android.util.Log;

import com.example.administrator.yongleapp.Bean.liujiahe.RecommendBean;
import com.example.administrator.yongleapp.Medol.liujiahe.RecommendMedol;
import com.example.administrator.yongleapp.Presenter.liujiahe.impl.RecommendPresenterImpl;
import com.example.administrator.yongleapp.View.Fragment.RecommendFragment;
import com.example.administrator.yongleapp.View.Fragment.liujiahe.impl.RecommendFragemntImpl;

/**
 * Created by Administrator on 2016/7/5.
 */
public class RecommendPresenter implements RecommendPresenterImpl {
    private RecommendFragment recommendFragemnt;
    private RecommendMedol recommendMedol;

    public RecommendPresenter(RecommendFragment recommendFragemnt){
        this.recommendFragemnt=recommendFragemnt;
        recommendMedol=new RecommendMedol();
    }


    @Override
    public void loadInfo() {
        recommendMedol.loadNetworkData(new RecommendMedol.OnLoadInfoListListener() {
            @Override
            public void onSuccess(RecommendBean recommendBean) {

                recommendFragemnt.onSuccess(recommendBean);
            }

            @Override
            public void onFailure() {
                recommendFragemnt.onFailure();
            }
        });
    }
}
