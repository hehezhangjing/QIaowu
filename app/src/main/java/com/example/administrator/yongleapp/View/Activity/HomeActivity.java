package com.example.administrator.yongleapp.View.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.administrator.yongleapp.Base.FragmentTabUtils;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Fragment.ActivityFragment;
import com.example.administrator.yongleapp.View.Fragment.ClassifyFragment;
import com.example.administrator.yongleapp.View.Fragment.FilmFragment;
import com.example.administrator.yongleapp.View.Fragment.RecommendFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    @Bind(R.id.layout_framelayout_main)
    FrameLayout layoutFramelayoutMain;
    @Bind(R.id.radiobutton_recommend)
    RadioButton radiobuttonRecommend;
    @Bind(R.id.radiobutton_classify)
    RadioButton radiobuttonClassify;
    @Bind(R.id.radiobutton_film)
    RadioButton radiobuttonFilm;
    @Bind(R.id.radiobutton_activity)
    RadioButton radiobuttonActivity;
    @Bind(R.id.radioGroup_main)
    RadioGroup radioGroupMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        initView();




    }


    private void initView() {

        int earchWidth = getResources().getDisplayMetrics().widthPixels / 4;
        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(earchWidth , RadioGroup
                .LayoutParams.WRAP_CONTENT );

        final RadioButton[] bariobutton={radiobuttonRecommend,radiobuttonClassify,radiobuttonFilm,radiobuttonActivity};
        for (int i = 0; i < bariobutton.length; i++) {
            bariobutton[i].setButtonDrawable(android.R.drawable.screen_background_light_transparent);
            bariobutton[i].setLayoutParams(params);
        }
        List<Fragment> fragments=new ArrayList<>();
        RecommendFragment fragment=new RecommendFragment();
        fragments.add(fragment);
        ClassifyFragment fragment2=new ClassifyFragment();
        fragments.add(fragment2);
        FilmFragment fragment3=new FilmFragment();
        fragments.add(fragment3);
        ActivityFragment fragment4=new ActivityFragment();
        fragments.add(fragment4);
        FragmentTabUtils utils=new FragmentTabUtils(getSupportFragmentManager(), fragments, R.id.layout_framelayout_main, radioGroupMain, new FragmentTabUtils.OnRgsExtraCheckedChangedListener() {
            @Override
            public void OnRgsExtraCheckedChanged(RadioGroup radioGroup, int checkedId, int index) {
                for (int i = 0; i < bariobutton.length; i++) {
                    bariobutton[i].setTextColor(Color.GRAY);
                    if (i==index){
                        bariobutton[i].setTextColor(Color.RED);
                    }
                }
            }
        });

    }
}
