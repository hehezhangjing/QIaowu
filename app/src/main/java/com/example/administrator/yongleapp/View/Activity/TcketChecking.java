package com.example.administrator.yongleapp.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.administrator.yongleapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TcketChecking extends AppCompatActivity {

    @Bind(R.id.textView_wenzi)
    TextView textViewWenzi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.print("-----------------------");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tcket_checking);
        ButterKnife.bind(this);
        // 接受第一个页面传值
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String choose_first = bundle.getString("choose_first");
        textViewWenzi.setText(choose_first);
    }
}
