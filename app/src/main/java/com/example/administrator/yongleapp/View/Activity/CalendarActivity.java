package com.example.administrator.yongleapp.View.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.yongleapp.Adapter.Calendar_listview_Adapter;
import com.example.administrator.yongleapp.Base.BaseActivity;
import com.example.administrator.yongleapp.Bean.liujiahe.CalendarBean;
import com.example.administrator.yongleapp.Presenter.liujiahe.CalendarPresenter;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Activity.impl.ActivityImpl;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CalendarActivity extends BaseActivity implements ActivityImpl<CalendarBean> {
    @Bind(R.id.textview_calendar)
    TextView textviewCalendar;
    @Bind(R.id.linearLayout_calendar)
    LinearLayout linearLayoutCalendar;
    private CalendarActivity activity = this;
    private Calendar_listview_Adapter adapter;
    private CalendarPresenter calendarPresenter;
    private String Tims;
    private int size = 10;
    @Bind(R.id.calendarView)
    CalendarView calendarView;
    @Bind(R.id.recyclerView_calendar)
    XRecyclerView recyclerViewCalendar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_calendar;
    }

    @Override
    public void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewCalendar.setLayoutManager(linearLayoutManager);
        recyclerViewCalendar.setRefreshProgressStyle(ProgressStyle.SquareSpin);
        recyclerViewCalendar.setRefreshing(true);
      //  recyclerViewCalendar.addHeaderView(linearLayoutCalendar);
        recyclerViewCalendar.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {


            }

            @Override
            public void onLoadMore() {
                size += 10;
                Tims = getDate(calendarView.getDate());
                calendarPresenter.loadInfo(Tims, size);
                recyclerViewCalendar.loadMoreComplete();
            }
        });
        Tims = getDate(calendarView.getDate());
        textviewCalendar.setText(Tims + "演出：");
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                textviewCalendar.setText(year + "-" + (month + 1) + "-" + dayOfMonth + "演出：");
                if (calendarPresenter == null) {
                    calendarPresenter = new CalendarPresenter(activity);
                }
                calendarPresenter.loadInfo(year + "-" + (month + 1) + "-" + dayOfMonth, size);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void onSuccess(CalendarBean bean) {

        if (adapter == null) {
            adapter = new Calendar_listview_Adapter(this, R.layout.fragemnt_recommend_list);
            for (int i = 0; i < bean.getData().getRecords().size(); i++) {
                adapter.add(bean.getData().getRecords().get(i));
            }
            recyclerViewCalendar.setAdapter(adapter);
        } else {
            adapter.clear();
            for (int i = 0; i < bean.getData().getRecords().size(); i++) {
                adapter.add(bean.getData().getRecords().get(i));
            }
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onFailure() {

    }

    public String getDate(long Time) {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return sDateFormat.format(new Date(Time + 0));
    }
}
