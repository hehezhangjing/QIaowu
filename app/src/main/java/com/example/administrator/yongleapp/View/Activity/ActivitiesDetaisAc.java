package com.example.administrator.yongleapp.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import android.widget.ListView;
import android.widget.TextView;


import com.example.administrator.yongleapp.Adapterqzn.DetailslistAD;
import com.example.administrator.yongleapp.Bean.liujiahe.FilmBean;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Fragment.FilmFragment;
import com.example.administrator.yongleapp.utils.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * ----------BigGod be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━
 * Administrator on 2016/7/5.
 * Class description:活动页面的详情
 * Created by:qzn
 * Creation time:2016/7/7.
 * Modified by:qzn
 * Modified time:2016/7/7.
 * Modified remarks:
 */
public class ActivitiesDetaisAc extends AppCompatActivity {
    private static final String TAG = "ActivitiesDetaisAc";
    private Context context = this;
    private List<FilmBean.DataBean.PromotionLinkListBean.ProductsBean> list =
            new ArrayList<>();
    private DetailslistAD mAdapter = null;
    private String remark = "";
    private String text = "";
    @Bind(R.id.backdrop)
    ImageView backdrop;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @Bind(R.id.appbar)
    AppBarLayout appbar;
    //    @Bind(R.id.textView_activities_test)
//    TextView textViewDetailsText;
    @Bind(R.id.listView_details_activities)
    ListView mListView;
    @Bind(R.id.main_content)
    CoordinatorLayout mainContent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities_detais);
        ButterKnife.bind(this);

        initData();
        initView();
        loadBackdrop();


    }

    /**
     * 初始化控件
     */
    private void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        collapsingToolbar.setTitle(remark);

        mAdapter = new DetailslistAD(context, list);

        View view = getLayoutInflater().inflate(R.layout.item_listview_detailsheader, null, false);
        TextView mTextView = (TextView) view.findViewById(R.id.textView_details_test);
        mTextView.setText(text);

        mListView.addHeaderView(view);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("productsId", list.get(position).getProductId());
                intent.setClass(context, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * 初始化数据
     */
    private void initData() {
        list = FilmFragment.list;
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        remark = bundle.getString("remark");
        text = bundle.getString("text");

    }

    /**
     * 设置背景图片
     */
    private void loadBackdrop() {
        final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        String urlImg = "http://static.228.cn" + list.get(0).getImgPath();
        ImageLoader.getInstance().displayImage(context, urlImg, imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sample_actions, menu);
        return true;
    }
}
