package com.example.administrator.yongleapp.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.administrator.yongleapp.R;

import cn.sharesdk.framework.ShareSDK;

public class GuideActivity extends AppCompatActivity {
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);

        intentActivity();
        ShareSDK.initSDK(this);
    }

    private void intentActivity() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(GuideActivity.this, HomeActivity.class);
                startActivity(intent);

            }
        }, 3000);
        this.finish();
    }
}
