package com.example.administrator.yongleapp.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ClassifyBean;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.utils.BaseRcAdapterHelper;
import com.example.administrator.yongleapp.utils.ImageLoader;
import com.example.administrator.yongleapp.utils.RcQuickAdapter;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：YongLeApp
 * BY：zzZ ON 2016/7/5 15:05
 * 类描述：
 */
public class AllFragmentAdapter extends RcQuickAdapter<ClassifyBean.DataBean.RecordsBean> {


    public AllFragmentAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    protected void convert(BaseRcAdapterHelper helper, ClassifyBean.DataBean.RecordsBean item) {
        ImageView imageView = helper.getImageView(R.id.imageView_item_icon);
        ImageLoader.getInstance().displayImage(context, "http://static.228.cn/" + item.getImgPath
                (), imageView);

        TextView textViewTitle = helper.getTextView(R.id.textView_item_title);
        textViewTitle.setText(item.getName());

        TextView textViewDate = helper.getTextView(R.id.textView_item_date);
        textViewDate.setText(item.getBeginDate());

        TextView textViewAddress = helper.getTextView(R.id.textView_item_address);
        textViewAddress.setText(item.getVenueName());

        TextView textViewPrice = helper.getTextView(R.id.textView_item_price);
        textViewPrice.setText(item.getMinPrice() + "元起");
    }



}
