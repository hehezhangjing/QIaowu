package com.example.administrator.yongleapp.Medol.liujiahe.zhangjing;

import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ZDetailsBean;
import com.example.administrator.yongleapp.Service.Service;
import com.example.administrator.yongleapp.utils.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 18:36
 * 类描述：
 */
public class ZDetailsModelImpl implements ZDetailsModel{

    @Override
    public void loadInfo(String type_id, final OnLoadInfoListListener listener) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Call<ZDetailsBean> call = retrofit.create(Service.class).getZDetailsData(type_id);

        call.enqueue(new Callback<ZDetailsBean>() {
            @Override
            public void onResponse(Call<ZDetailsBean> call, Response<ZDetailsBean> response) {
                if (response.isSuccessful() && response.body() != null){
                    ZDetailsBean bean =  response.body();
                    listener.onSuccess(bean);
                }
            }

            @Override
            public void onFailure(Call<ZDetailsBean> call, Throwable t) {
                listener.onFailure("load news list failure.", (Exception) t);

            }
        });
    }

    public interface OnLoadInfoListListener {
        void onSuccess(ZDetailsBean bean);

        void onFailure(String msg, Exception ex);
    }
}
