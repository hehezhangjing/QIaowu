package com.example.administrator.yongleapp.View.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.example.administrator.yongleapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BaiduMap extends AppCompatActivity {
    @Bind(R.id.bmapView)
    MapView bmapView;
    private Context mContext = this;
    private com.baidu.mapapi.map.BaiduMap mBaiduMap;
    //定位
    private LocationClient mLocationClient;
    private MyLocationListener mLocationListener;
    private boolean isFirstIn = true;
    private double mLatitude;
    private double mLongtitude;
    //定义图标
    private BitmapDescriptor mIconLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.baidumap);
        ButterKnife.bind(this);
        initView();
        //初始化定位
        initLocation();

        initIntent();
    }

    private void initIntent() {
        mLatitude = Double.parseDouble(getIntent().getStringExtra("latitude"));
        mLongtitude = Double.parseDouble(getIntent().getStringExtra("longtitude"));
    }

    private void initLocation() {
        mLocationClient = new LocationClient(this);
        mLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mLocationListener);

        //注册定位
        LocationClientOption option = new LocationClientOption();
        //获取地址信息设置
        option.setCoorType("bd09ll");
        //获取设置的Prod字段值
        option.setIsNeedAddress(true);
        //是否打开gps进行定位
        option.setOpenGps(true);
        //获取 设置的扫描间隔，单位是毫秒
        option.setScanSpan(1000);
        //必须设置
        mLocationClient.setLocOption(option);
        //初始化图标
        mIconLocation = BitmapDescriptorFactory.fromResource(R.drawable.img_mark);
    }

    private void initView() {
        mBaiduMap = bmapView.getMap();
        MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(15.0f);
        mBaiduMap.setMapStatus(msu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        bmapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        bmapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //是否开启定位
        mBaiduMap.setMyLocationEnabled(true);
        //开启定位
        if (!mLocationClient.isStarted()) {
            mLocationClient.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        bmapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //停止定位
        mBaiduMap.setMyLocationEnabled(false);
        mLocationClient.stop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Mapsite:
                mBaiduMap.setMapType(com.baidu.mapapi.map.BaiduMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.Maptraffic:
                if (mBaiduMap.isTrafficEnabled()) {
                    mBaiduMap.setTrafficEnabled(false);
                    item.setTitle("实时交通关闭");
                } else {
                    mBaiduMap.setTrafficEnabled(true);
                    item.setTitle("实时交通开启");
                }
                break;
            case R.id.Maplocayion:
                centerToMylocation();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    * 定位到我的位置
    * */
    private void centerToMylocation() {
        //设置经纬度
        LatLng latLng = new LatLng(mLatitude, mLongtitude);
        //设置第一次已自己为中心
        MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latLng);
        //做个动画效果
        mBaiduMap.animateMapStatus(msu);
    }

    private class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            MyLocationData data = new MyLocationData.Builder()//
                    .accuracy(location.getRadius())//
                    .latitude(mLatitude)//
                    .longitude(mLongtitude)//
                    .build();
            mBaiduMap.setMyLocationData(data);
            //设置图标
            MyLocationConfiguration config = new MyLocationConfiguration(MyLocationConfiguration
                    .LocationMode.NORMAL, true, mIconLocation);
            mBaiduMap.setMyLocationConfigeration(config);
            //重新定位
           /* mLatitude = location.getLatitude();
            mLongtitude = location.getLongitude();*/
            if (isFirstIn) {
                //设置经纬度
                LatLng latLng = new LatLng(mLatitude, mLongtitude);
                //设置第一次已自己为中心
                MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latLng);
                //做个动画效果
                mBaiduMap.animateMapStatus(msu);
                isFirstIn = false;
                Toast.makeText(mContext, mLatitude + "" + mLongtitude + "", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
