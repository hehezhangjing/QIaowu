package com.example.administrator.yongleapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.List;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：YongLeApp
 * BY：zzZ ON 2016/7/5 14:09
 * 类描述：
 */
public class ClassifyFragmentAdapter extends FragmentPagerAdapter {
    private List<Fragment> list = null;
    private String[] arrTabTitles = null;
    private FragmentManager fm;

    public ClassifyFragmentAdapter(FragmentManager fm, List<Fragment> list, String[] arrTabTitles) {
        super(fm);
        this.list = list;
        this.arrTabTitles = arrTabTitles;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return arrTabTitles[position];
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }
}
