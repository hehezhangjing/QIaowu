package com.example.administrator.yongleapp.Presenter.liujiahe;

import com.example.administrator.yongleapp.Bean.liujiahe.CalendarBean;
import com.example.administrator.yongleapp.Bean.liujiahe.RankBean;
import com.example.administrator.yongleapp.Medol.liujiahe.CalendarMedol;
import com.example.administrator.yongleapp.Medol.liujiahe.RankMedol;
import com.example.administrator.yongleapp.Presenter.liujiahe.impl.PresenterImpl;
import com.example.administrator.yongleapp.View.Activity.CalendarActivity;
import com.example.administrator.yongleapp.View.Fragment.liujiahe.impl.RankFragment;

/**
 * Created by Administrator on 2016/7/7.
 */
public class CalendarPresenter implements PresenterImpl {

    private CalendarActivity calendarActivity;
    private CalendarMedol calendarMedol;

    public CalendarPresenter(CalendarActivity calendarActivity){
        this.calendarActivity=calendarActivity;
        calendarMedol=new CalendarMedol();
    }



    @Override
    public void loadInfo(String date,int size) {
        calendarMedol.loadNetworkData(new CalendarMedol.OnLoadInfoListListener() {
            @Override
            public void onSuccess(CalendarBean calendarBean) {

                calendarActivity.onSuccess(calendarBean);
            }

            @Override
            public void onFailure() {
                calendarActivity.onFailure();
            }
        },date,size);
    }
}
