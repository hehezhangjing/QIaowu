package com.example.administrator.yongleapp.View.Fragment.liujiahe.impl;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.administrator.yongleapp.Adapter.Rank_listview_Adapter;
import com.example.administrator.yongleapp.Base.BaseFragment;
import com.example.administrator.yongleapp.Base.BaseRcQuickAdapter;
import com.example.administrator.yongleapp.Bean.liujiahe.RankBean;
import com.example.administrator.yongleapp.Presenter.liujiahe.RankPresenter;
import com.example.administrator.yongleapp.R;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/7/6.
 */
public class RankFragment extends BaseFragment implements FragemntImpl<RankBean> {
    @Bind(R.id.recyclerView_rank)
    XRecyclerView recyclerViewRank;
    private int index;
    private Rank_listview_Adapter adapter;
    private  RankPresenter rankPresenter;
    public RankFragment(int index) {
        this.index = index;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_rank;
    }

    @Override
    public void initView() {
        rankPresenter = new RankPresenter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewRank.setLayoutManager(linearLayoutManager);
        recyclerViewRank.setRefreshProgressStyle(ProgressStyle.SquareSpin);
        recyclerViewRank.setRefreshing(true);
        recyclerViewRank.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                loadData();
                recyclerViewRank.refreshComplete();
            }
            @Override
            public void onLoadMore() {
                loadData();
                recyclerViewRank.loadMoreComplete();
            }
        });
        recyclerViewRank.refreshComplete();
        loadData();
        recyclerViewRank.refreshComplete();
    }
    private void loadData(){
        rankPresenter.loadInfo("",0);
    }
    @Override
    public void onSuccess(RankBean bean) {
        if (adapter==null) {
            adapter = new Rank_listview_Adapter(getActivity(), R.layout.fragemnt_recommend_list);
            for (int i = 0; i < bean.getData().getRanking_list().get(index).size(); i++) {
                adapter.add(bean.getData().getRanking_list().get(index).get(i));
            }
            recyclerViewRank.setAdapter(adapter);
        }else {
            for (int i = 0; i < bean.getData().getRanking_list().get(index).size(); i++) {
                adapter.add(bean.getData().getRanking_list().get(index).get(i));
            }
            adapter.notifyDataSetChanged();
        }
        adapter.setOnItemClickListener(new BaseRcQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Toast.makeText(getContext(),position+"",Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onFailure() {
        Log.i("RecommendFragment", "----->>Presenter:loadInfo: " + "连接错误");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
