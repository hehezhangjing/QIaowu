package com.example.administrator.yongleapp.Medol.liujiahe.zhangjing;

import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ClassifyBean;
import com.example.administrator.yongleapp.Service.Service;
import com.example.administrator.yongleapp.utils.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 14:05
 * 类描述：
 */
public class AllModelImpl implements AllModel {


    @Override
    public void loadInfo(String id, int pager, final OnLoadInfoListListener listener) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Call<ClassifyBean> call = retrofit.create(Service.class).getClassifyBean(id, pager);

        call.enqueue(new Callback<ClassifyBean>() {
            @Override
            public void onResponse(Call<ClassifyBean> call, Response<ClassifyBean> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<ClassifyBean.DataBean.RecordsBean> list = response.body().getData()
                            .getRecords();
                    listener.onSuccess(list);
                }
            }

            @Override
            public void onFailure(Call<ClassifyBean> call, Throwable t) {
                listener.onFailure("load news list failure.", (Exception) t);
            }
        });
    }

    public interface OnLoadInfoListListener {
        void onSuccess(List<ClassifyBean.DataBean.RecordsBean> list);

        void onFailure(String msg, Exception ex);
    }
}
