package com.example.administrator.yongleapp.Medol.liujiahe;

import android.util.Log;

import com.example.administrator.yongleapp.Bean.liujiahe.CalendarBean;
import com.example.administrator.yongleapp.Bean.liujiahe.RankBean;
import com.example.administrator.yongleapp.Interfaces.ServiceInterface;
import com.example.administrator.yongleapp.utils.liujiaheConstant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 2016/7/7.
 */
public class CalendarMedol {
    public void loadNetworkData(final OnLoadInfoListListener onLoadInfoListListener,String days,int size){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(liujiaheConstant.TUIJIAN)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Call<CalendarBean> call = retrofit.create(ServiceInterface.class).getCalendarBean(days,size+"");

        call.enqueue(new Callback<CalendarBean>() {
            @Override
            public void onResponse(Call<CalendarBean> call, Response<CalendarBean> response) {
                if (response.isSuccessful() && response.body() != null) {
                    CalendarBean calendarBean=response.body();
                    onLoadInfoListListener.onSuccess(calendarBean);
                }
            }

            @Override
            public void onFailure(Call<CalendarBean> call, Throwable t) {
            }
        });

    }
    public interface OnLoadInfoListListener {
        void onSuccess(CalendarBean calendarBean);

        void onFailure();
    }
}
