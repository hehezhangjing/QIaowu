package com.example.administrator.yongleapp.View.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.example.administrator.yongleapp.Adapter.Rank_viewPager_Adapter;
import com.example.administrator.yongleapp.Base.BaseActivity;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Fragment.liujiahe.impl.RankFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RankActivity extends BaseActivity {
    @Bind(R.id.tabLayout_rank)
    TabLayout tabLayoutRank;
    @Bind(R.id.viewPager_rank)
    ViewPager viewPagerRank;
    private List<Fragment> list = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_rank;
    }

    @Override
    public void initView() {
        String[] arr = getResources().getStringArray(R.array.arrayRank);
        for (int i = 0; i < arr.length; i++) {
            RankFragment fragment = new RankFragment(i);
            list.add(fragment);
        }
        Rank_viewPager_Adapter adapter = new Rank_viewPager_Adapter(getSupportFragmentManager(), list, arr);
        viewPagerRank.setAdapter(adapter);
       //viewPagerRank.setOffscreenPageLimit(2);
        tabLayoutRank.setupWithViewPager(viewPagerRank);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
