package com.example.administrator.yongleapp.Presenter.liujiahe;

import com.example.administrator.yongleapp.Bean.liujiahe.RankBean;
import com.example.administrator.yongleapp.Bean.liujiahe.RecommendBean;
import com.example.administrator.yongleapp.Medol.liujiahe.RankMedol;
import com.example.administrator.yongleapp.Medol.liujiahe.RecommendMedol;
import com.example.administrator.yongleapp.Presenter.liujiahe.impl.PresenterImpl;
import com.example.administrator.yongleapp.View.Fragment.RecommendFragment;
import com.example.administrator.yongleapp.View.Fragment.liujiahe.impl.RankFragment;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

/**
 * Created by Administrator on 2016/7/6.
 */
public class RankPresenter implements PresenterImpl {
    private RankFragment rankFragment;
    private RankMedol rankMedol;

    public RankPresenter(RankFragment rankFragment){
        this.rankFragment=rankFragment;
        if (rankMedol==null) {
            rankMedol = new RankMedol();
        }
    }



    @Override
    public void loadInfo(String date,int siez) {
        rankMedol.loadNetworkData(new RankMedol.OnLoadInfoListListener() {
            @Override
            public void onSuccess(RankBean rankBean) {

                rankFragment.onSuccess(rankBean);
            }

            @Override
            public void onFailure() {
                rankFragment.onFailure();
            }
        });
    }

}
