package com.example.administrator.yongleapp.Medol.liujiahe;

import android.util.Log;

import com.example.administrator.yongleapp.Bean.liujiahe.RecommendBean;
import com.example.administrator.yongleapp.Interfaces.ServiceInterface;
import com.example.administrator.yongleapp.Presenter.liujiahe.RecommendPresenter;
import com.example.administrator.yongleapp.Presenter.liujiahe.impl.RecommendPresenterImpl;
import com.example.administrator.yongleapp.utils.liujiaheConstant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 2016/7/5.
 */
public class RecommendMedol {
    public void loadNetworkData(final OnLoadInfoListListener onLoadInfoListListener){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(liujiaheConstant.TUIJIAN)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Call<RecommendBean> call = retrofit.create(ServiceInterface.class).getInfoList();

        call.enqueue(new Callback<RecommendBean>() {
            @Override
            public void onResponse(Call<RecommendBean> call, Response<RecommendBean> response) {
                if (response.isSuccessful() && response.body() != null) {
                    RecommendBean recommendBean=response.body();
                    onLoadInfoListListener.onSuccess(recommendBean);
                }
            }

            @Override
            public void onFailure(Call<RecommendBean> call, Throwable t) {
            }
        });

    }
    public interface OnLoadInfoListListener {
        void onSuccess(RecommendBean recommendBean);

        void onFailure();
    }
}
