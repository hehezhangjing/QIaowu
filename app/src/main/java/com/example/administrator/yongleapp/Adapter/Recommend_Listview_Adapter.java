package com.example.administrator.yongleapp.Adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liujiahe.worthtobuy.Model.Worth_Home_ListModel;
import com.liujiahe.worthtobuy.Model.Worth_Home_Model;
import com.liujiahe.worthtobuy.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/6/6.
 */
public class Recommend_Listview_Adapter extends RecyclerView.Adapter {
    private Context context = null;
    private Worth_Home_imagePagerAdapter myimagePageradapter;
    private Worth_Home_ListModel.DataBean list = null;
    private LayoutInflater inflater = null;
    private   List<String> imagelist = new ArrayList<>();
    private Worth_Home_Model.DataBean databean;
    private ViewHolder1 viewHolder1;
    private ViewHolder3 viewHolder3;
    private OnItemClickLitener mOnItemClickLitener;
    private Handler handler = new Handler();
    private ViewPager viewPager;
    private int index=0;
    private Handler handler1 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    viewPager.setCurrentItem(index++);
                    if (index >= imagelist.size() - 1) {
                        index = 0;
                    }
                    handler1.sendEmptyMessageDelayed(0, 5000);
                    break;
            }
        }
    };
    public Recommend_Listview_Adapter(Context context, Worth_Home_ListModel.DataBean list, List<String> imagerlist, Worth_Home_Model.DataBean databean) {
        this.context = context;
        this.list = list;
        this.databean=databean;
        this.imagelist=imagerlist;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemViewType(int position) {

            return position;
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.getRows().size();
    }

    public void reloadListView(Worth_Home_ListModel.DataBean _list, boolean isClear) {
        list=_list;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1=null;
        if(viewType==0){
             view1 = inflater.inflate(R.layout.wor_home_viewpager_item, parent, false);
            return new ViewHolder1(view1);
        }
        else if (viewType==1){
             view1 = inflater.inflate(R.layout.wor_home_imagers_item, parent, false);
            return new ViewHolder3(view1);
        }
        else {
             view1 = inflater.inflate(R.layout.wor_home_fra_listview_item, parent, false);
            return new ViewHolder2(view1);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        if (mOnItemClickLitener != null) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = viewHolder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(viewHolder.itemView, position);
                }
            });

        }

        if (viewHolder instanceof ViewHolder2) {
            ViewHolder2 viewHolder2 = (ViewHolder2) viewHolder;
            viewHolder2.textview1_wor_home_listview_item.setText(list.getRows().get(position).getArticle_channel_name());
            viewHolder2.textview2_wor_home_listview_item.setText(list.getRows().get(position).getArticle_title());
            viewHolder2.textview3_wor_home_listview_item.setText(list.getRows().get(position).getArticle_price());
            viewHolder2.textview4_wor_home_listview_item.setText(list.getRows().get(position).getArticle_mall()+"|"+list.getRows().get(position).getArticle_format_date());
            viewHolder2.textview5_wor_home_listview_item.setText("评论：" + list.getRows().get(position).getArticle_comment());
            viewHolder2.textview6_wor_home_listview_item.setText("值："+list.getRows().get(position).getArticle_worthy()+"%");
            Glide.with(context).load(list.getRows().get(position).getArticle_pic())
                    .placeholder(R.drawable.rec_subscribe_default_half)
                    .error(R.drawable.rec_subscribe_default_half)
                    .into(viewHolder2.image_wor_home_listview_item);
        }
       else if (viewHolder instanceof ViewHolder1) {
            viewHolder1 = (ViewHolder1) viewHolder;
            myimagePageradapter = new Worth_Home_imagePagerAdapter(imagelist, context);
            viewHolder1.viewpager_worth_home_fra.setAdapter(myimagePageradapter);
            viewPager=viewHolder1.viewpager_worth_home_fra;switchImage();
        }
        else if (viewHolder instanceof ViewHolder3) {
            viewHolder3 = (ViewHolder3) viewHolder;
            Picasso.with(context).load(databean.getLittle_banner().get(0).getImg()).into(viewHolder3.imagerview_wor_home_fra1);
            Picasso.with(context).load(databean.getLittle_banner().get(1).getImg()).into(viewHolder3.imagerview_wor_home_fra2);
            Picasso.with(context).load(databean.getLittle_banner().get(2).getImg()).into(viewHolder3.imagerview_wor_home_fra3);
        }

    }


    class ViewHolder2 extends RecyclerView.ViewHolder {
        private ImageView image_wor_home_listview_item;
        private TextView textview1_wor_home_listview_item;
        private TextView textview2_wor_home_listview_item;
        private TextView textview3_wor_home_listview_item;
        private TextView textview4_wor_home_listview_item;
        private TextView textview5_wor_home_listview_item;
        private TextView textview6_wor_home_listview_item;


        public ViewHolder2(View convertView) {
            super(convertView);
            image_wor_home_listview_item= (ImageView) convertView.findViewById(R.id.image_wor_home_listview_item);
            textview1_wor_home_listview_item= (TextView) convertView.findViewById(R.id.textview1_wor_home_listview_item);
            textview2_wor_home_listview_item= (TextView) convertView.findViewById(R.id.textview2_wor_home_listview_item);
            textview3_wor_home_listview_item= (TextView) convertView.findViewById(R.id.textview3_wor_home_listview_item);
            textview4_wor_home_listview_item= (TextView) convertView.findViewById(R.id.textview4_wor_home_listview_item);
            textview5_wor_home_listview_item= (TextView) convertView.findViewById(R.id.textview5_wor_home_listview_item);
            textview6_wor_home_listview_item= (TextView) convertView.findViewById(R.id.textview6_wor_home_listview_item);
        }
    }
    class ViewHolder1 extends RecyclerView.ViewHolder {
        private ViewPager viewpager_worth_home_fra;

        public ViewHolder1(View convertView) {
            super(convertView);
            viewpager_worth_home_fra = (ViewPager) convertView.findViewById(R.id.viewpager_worth_home_fra);
        }
    }
    class ViewHolder3 extends RecyclerView.ViewHolder {
        private ImageView imagerview_wor_home_fra1;
        private ImageView imagerview_wor_home_fra2;
        private ImageView imagerview_wor_home_fra3;

        public ViewHolder3(View convertView) {
            super(convertView);
            imagerview_wor_home_fra1 = (ImageView) convertView.findViewById(R.id.imagerview_wor_home_fra1);
            imagerview_wor_home_fra2 = (ImageView) convertView.findViewById(R.id.imagerview_wor_home_fra2);
            imagerview_wor_home_fra3 = (ImageView) convertView.findViewById(R.id.imagerview_wor_home_fra3);
        }
    }

    private void switchImage() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler1.sendEmptyMessage(0);
            }
        }).start();
    }
}
