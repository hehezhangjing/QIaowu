package com.example.administrator.yongleapp.Bean.liujiahe;

import java.util.List;

/**
 * Created by Administrator on 2016/7/6.
 */
public class RankBean {


    /**
     * code : 0
     * message : 请求成功
     * bonus : 0
     * timestamp : 2016-07-06 14:58:54
     */

    private ResultBean result;
    private DataBean data;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ResultBean {
        private int code;
        private String message;
        private int bonus;
        private String timestamp;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getBonus() {
            return bonus;
        }

        public void setBonus(int bonus) {
            this.bonus = bonus;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

    public static class DataBean {
        /**
         * isRobTicket : 1
         * status : 1
         * finishDate : 2016-07-23 19:30
         * publishchannel : a,b,c,d,f,g
         * beginDate : 2016-07-23 19:30
         * productId : 134326913
         * productMarkings : 0
         * onlineseat : 0
         * name : 希天才秀—金希澈北京见面会
         * imgPath : /upload/2016/06/17/AfterTreatment/1466159869391_r2s9-0.jpg
         * path : null
         * special : 【温馨提示】本演出将于6月17日19:00正式开启预售，建议您提前注册、登录永乐票务网站或客户端，便于第一时间秒杀成功；为了保证您能够更顺利购票，请下载最新版本的APP。
         * venueId : 11604700
         * venueName : 北京奥体中心体育馆
         * isrobseat : 0
         * minPrice : 780
         */

        private List<List<RankingListBean>> ranking_list;
        private List<String> catalog;

        public List<List<RankingListBean>> getRanking_list() {
            return ranking_list;
        }

        public void setRanking_list(List<List<RankingListBean>> ranking_list) {
            this.ranking_list = ranking_list;
        }

        public List<String> getCatalog() {
            return catalog;
        }

        public void setCatalog(List<String> catalog) {
            this.catalog = catalog;
        }

        public static class RankingListBean {
            private int isRobTicket;
            private int status;
            private String finishDate;
            private String publishchannel;
            private String beginDate;
            private String productId;
            private int productMarkings;
            private int onlineseat;
            private String name;
            private String imgPath;
            private Object path;
            private String special;
            private int venueId;
            private String venueName;
            private int isrobseat;
            private int minPrice;

            public int getIsRobTicket() {
                return isRobTicket;
            }

            public void setIsRobTicket(int isRobTicket) {
                this.isRobTicket = isRobTicket;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getFinishDate() {
                return finishDate;
            }

            public void setFinishDate(String finishDate) {
                this.finishDate = finishDate;
            }

            public String getPublishchannel() {
                return publishchannel;
            }

            public void setPublishchannel(String publishchannel) {
                this.publishchannel = publishchannel;
            }

            public String getBeginDate() {
                return beginDate;
            }

            public void setBeginDate(String beginDate) {
                this.beginDate = beginDate;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public int getProductMarkings() {
                return productMarkings;
            }

            public void setProductMarkings(int productMarkings) {
                this.productMarkings = productMarkings;
            }

            public int getOnlineseat() {
                return onlineseat;
            }

            public void setOnlineseat(int onlineseat) {
                this.onlineseat = onlineseat;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImgPath() {
                return imgPath;
            }

            public void setImgPath(String imgPath) {
                this.imgPath = imgPath;
            }

            public Object getPath() {
                return path;
            }

            public void setPath(Object path) {
                this.path = path;
            }

            public String getSpecial() {
                return special;
            }

            public void setSpecial(String special) {
                this.special = special;
            }

            public int getVenueId() {
                return venueId;
            }

            public void setVenueId(int venueId) {
                this.venueId = venueId;
            }

            public String getVenueName() {
                return venueName;
            }

            public void setVenueName(String venueName) {
                this.venueName = venueName;
            }

            public int getIsrobseat() {
                return isrobseat;
            }

            public void setIsrobseat(int isrobseat) {
                this.isrobseat = isrobseat;
            }

            public int getMinPrice() {
                return minPrice;
            }

            public void setMinPrice(int minPrice) {
                this.minPrice = minPrice;
            }
        }
    }
}
