package com.example.administrator.yongleapp.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.liujiahe.worthtobuy.Model.Discover_bagefragment_Model;
import com.liujiahe.worthtobuy.R;

import java.util.List;

/**
 * Created by Administrator on 2016/6/28 0028.
 */
public class Recommend_viewpager_Adapter extends PagerAdapter {
    private Context context = null;
    private List<Discover_bagefragment_Model.DataBean.RowsBean> list = null;
    private LayoutInflater inflater = null;
    public Recommend_viewpager_Adapter(Context context, List<Discover_bagefragment_Model.DataBean.RowsBean> list) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view== object;
    }
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View view = inflater.inflate(R.layout.hu_myfirstpager_item, container , false);
        //初始化填充的布局上的UI控件
        ImageView firstpager_item = (ImageView) view.findViewById(R.id.firstpager_item);


        //加载图片Picasso
        String imageUrl = list.get(position).getImg();
        if (imageUrl != null) {
            // 使用Glide框架加载图片
            Glide.with(context).load(imageUrl)
                    .placeholder(R.drawable.rec_subscribe_default_half)
                    .error(R.drawable.rec_subscribe_default_half)
                    .into(firstpager_item);
        }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
