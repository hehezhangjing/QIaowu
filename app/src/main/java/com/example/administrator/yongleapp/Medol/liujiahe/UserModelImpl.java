package com.example.administrator.yongleapp.Medol.liujiahe;

import android.content.Context;

import com.example.administrator.yongleapp.Bean.liujiahe.UserBean;

import cn.bmob.v3.listener.SaveListener;

/**
 * ----------BigGod be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━
 * Administrator on 2016/7/9.
 * Class description:userModel
 * Created by:qzn
 * Creation time:2016/7/9.
 * Modified by:qzn
 * Modified time:2016/7/9.
 * Modified remarks:
 */
public class UserModelImpl implements UserModel {


    /**
     * 用户登录
     *
     * @param user
     * @param lister
     */
    @Override
    public void login(UserBean user, Context context, final UserLitster lister) {
        user.login(context, new SaveListener() {
            @Override
            public void onSuccess() {
                lister.onSucc();

            }

            @Override
            public void onFailure(int i, String s) {
                lister.onFail();

            }
        });

    }

    /**
     * 用户注册
     *
     * @param user
     * @param lister
     */
    @Override
    public void register(UserBean user, Context context, final UserLitster lister) {
        user.signUp(context, new SaveListener() {
            @Override
            public void onSuccess() {
                lister.onSucc();
            }

            @Override
            public void onFailure(int i, String s) {
                lister.onFail();
            }
        });

    }

    /**
     * 用户登录和注册监听器
     */
    public interface UserLitster {
        void onSucc();

        void onFail();
    }
}
