package com.example.administrator.yongleapp.View.Fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.administrator.yongleapp.Base.BaseFragment;
import com.example.administrator.yongleapp.Bean.liujiahe.RecommendBean;
import com.example.administrator.yongleapp.Presenter.liujiahe.RecommendPresenter;
import com.example.administrator.yongleapp.Presenter.liujiahe.impl.RecommendPresenterImpl;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Fragment.liujiahe.impl.RecommendFragemntImpl;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/7/4.
 */
public class RecommendFragment extends BaseFragment implements RecommendFragemntImpl {
    @Bind(R.id.recyclerView_recommend)
    RecyclerView recyclerViewRecommend;
    @Bind(R.id.swipeRefreshLayout_main)
    SwipeRefreshLayout swipeRefreshLayoutMain;

    @Override
    public int getLayoutId() {
        return R.layout.fragemnt_recommend;
    }

    @Override
    public void initView() {
        RecommendPresenter recommendPresenter=new RecommendPresenter(this);
        recommendPresenter.loadInfo();
    }

    @Override
    public void onSuccess(RecommendBean bean) {
        Log.i("RecommendFragment", "----->>Presenter:loadInfo: " +bean.getData().getAd().get(0).getPicurl());
    }

    @Override
    public void onFailure() {
        Log.i("RecommendFragment", "----->>Presenter:loadInfo: " + "连接错误");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
