package com.example.administrator.yongleapp.Medol.liujiahe;

import com.example.administrator.yongleapp.Bean.liujiahe.RankBean;
import com.example.administrator.yongleapp.Bean.liujiahe.RecommendBean;
import com.example.administrator.yongleapp.Interfaces.ServiceInterface;
import com.example.administrator.yongleapp.utils.liujiaheConstant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 2016/7/6.
 */
public class RankMedol {

    public void loadNetworkData(final OnLoadInfoListListener onLoadInfoListListener){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(liujiaheConstant.TUIJIAN)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Call<RankBean> call = retrofit.create(ServiceInterface.class).getRankBean();

        call.enqueue(new Callback<RankBean>() {
            @Override
            public void onResponse(Call<RankBean> call, Response<RankBean> response) {
                if (response.isSuccessful() && response.body() != null) {
                    RankBean rankBean=response.body();
                    onLoadInfoListListener.onSuccess(rankBean);
                }
            }

            @Override
            public void onFailure(Call<RankBean> call, Throwable t) {
            }
        });

    }
    public interface OnLoadInfoListListener {
        void onSuccess(RankBean recommendBean);

        void onFailure();
    }
}
