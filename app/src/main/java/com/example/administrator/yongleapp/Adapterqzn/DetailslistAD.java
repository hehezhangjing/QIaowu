package com.example.administrator.yongleapp.Adapterqzn;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.yongleapp.Bean.liujiahe.FilmBean;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.utils.ImageLoader;
import java.util.List;

/**
 * ----------BigGod be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━
 * Administrator on 2016/7/7.
 * Class description:点击活动进入活动详情页面的适配器
 * Created by:qzn
 * Creation time:2016/7/7.
 * Modified by:qzn
 * Modified time:2016/7/7.
 * Modified remarks:
 */
public class DetailslistAD extends BaseAdapter {
    private Context context = null;
    private List<FilmBean.DataBean.PromotionLinkListBean.ProductsBean> list = null;
    private LayoutInflater layoutInflater = null;



    public interface OnItemClickedListener {
        void onItemClick(int postion);

    }
    public DetailslistAD(Context context, List<FilmBean.DataBean.PromotionLinkListBean
            .ProductsBean> list) {
        this.context = context;
        this.list = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_listview_details, parent, false);
            mHolder = new ViewHolder(convertView);
            convertView.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) convertView.getTag();
        }
        String urlImg = "http://static.228.cn" + list.get(position).getImgPath();
        ImageLoader.getInstance().displayImage(context, urlImg, mHolder.imageView_detais_pic);
        mHolder.textView_detais_name.setText(list.get(position).getName());
        mHolder.textView_detais_datebg.setText(list.get(position).getBeginDate());
        mHolder.textView_detais_dateed.setText(list.get(position).getFinishDate());
        mHolder.textView_detais_venuename.setText(list.get(position).getVenueName());
        mHolder.textView_detais_minprice.setText(list.get(position).getMinPrice() + "");
        return convertView;
    }

    class ViewHolder {
        private ImageView imageView_detais_pic;
        private TextView textView_detais_name;
        private TextView textView_detais_datebg;
        private TextView textView_detais_dateed;
        private TextView textView_detais_venuename;
        private TextView textView_detais_minprice;


        public ViewHolder(View convertView) {

            imageView_detais_pic = (ImageView) convertView.findViewById(R.id.imageView_detais_pic);
            textView_detais_name = (TextView) convertView.findViewById(R.id.textView_detais_name);
            textView_detais_datebg = (TextView) convertView.findViewById(R.id
                    .textView_detais_datebg);
            textView_detais_dateed = (TextView) convertView.findViewById(R.id
                    .textView_detais_dateed);
            textView_detais_venuename = (TextView) convertView.findViewById(R.id
                    .textView_detais_venuename);
            textView_detais_minprice = (TextView) convertView.findViewById(R.id
                    .textView_detais_minprice);
        }

    }
}
