package com.example.administrator.yongleapp.View.Activity.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.administrator.yongleapp.Base.BaseActivity;
import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ZDetailsBean;
import com.example.administrator.yongleapp.Presenter.liujiahe.zhangjing.ZDetailsPresenterImpl;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Activity.BaiduMap;
import com.example.administrator.yongleapp.View.Activity.ShowActivity;
import com.example.administrator.yongleapp.View.Fragment.zhangjing.ZDetails;
import com.example.administrator.yongleapp.utils.ImageLoader;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.onekeyshare.OnekeyShare;

public class ZDetailsActivityImpl extends BaseActivity implements ZDetails {
    @Bind(R.id.iv_back)
    ImageView ivBack;
    @Bind(R.id.iv_collection)
    ImageView ivCollection;
    @Bind(R.id.iv_share)
    ImageView ivShare;
    @Bind(R.id.iv_time)
    ImageView ivTime;
    @Bind(R.id.iv_money)
    ImageView ivMoney;
    @Bind(R.id.tv_buy)
    TextView tvBuy;

    private String url;
    private String coordinates;  //经纬度
    private String latitude;  //纬度
    private String longtitude;  //经度
    private Context mContext = this;
    private ZDetailsPresenterImpl zDetailsPresenter = null;

    @Bind(R.id.zdetails_top_title)
    TextView zdetailsTopTitle;
    @Bind(R.id.imageView_zdetails)
    ImageView imageViewZdetails;
    @Bind(R.id.tv_zdetails_title)
    TextView tvZdetailsTitle;
    @Bind(R.id.tv_zdetails_content_date)
    TextView tvZdetailsContentDate;
    @Bind(R.id.tv_zdetails_address)
    TextView tvZdetailsAddress;
    @Bind(R.id.tv_zdetails_money)
    TextView tvZdetailsMoney;
    @Bind(R.id.tv_zdetails_info)
    TextView tvZdetailsInfo;
    @Bind(R.id.listView_zdetails)
    ListView listViewZdetails;

    @Override
    public int getLayoutId() {
        return R.layout.activity_zdetails;
    }

    @Override
    public void initView() {
        String type_id = getIntent().getStringExtra("type_id");
        zDetailsPresenter = new ZDetailsPresenterImpl(this);
        zDetailsPresenter.loadInfo(type_id);
    }

    @Override
    public void addInfo(ZDetailsBean infoBean) {
        coordinates = infoBean.getData().getProduct().getVenue().getCoordinates();
        latitude = coordinates.substring(0, 9);
        Log.i("TAG", "---->latitude"+latitude);
        longtitude = coordinates.substring(11, 20);
        Log.i("TAG", "---->longtitude"+longtitude);

        zdetailsTopTitle.setText(infoBean.getData().getProduct().getName());
        tvZdetailsTitle.setText(infoBean.getData().getProduct().getName());
        tvZdetailsContentDate.setText(infoBean.getData().getProduct().getBeginDate());
        tvZdetailsAddress.setText(infoBean.getData().getProduct().getVenue().getName());
        tvZdetailsMoney.setText(infoBean.getData().getProduct().getMinPrice() + "-" + infoBean
                .getData().getProduct().getMaxPrice());
        tvZdetailsInfo.setText(infoBean.getData().getProduct().getIntroduction1());
        url = "http://static.228.cn/" + infoBean.getData().getProduct().getImg();
        ImageLoader.getInstance().displayImage(mContext, url, imageViewZdetails);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.iv_back, R.id.iv_collection, R.id.iv_share, R.id.tv_buy, R.id.tv_zdetails_address, R.id
            .imageView_zdetails})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                ZDetailsActivityImpl.this.finish();
                break;
            case R.id.iv_collection:
                break;
            case R.id.iv_share:
                showShare();
                break;
            case R.id.tv_buy:
                break;
            case R.id.tv_zdetails_address:
                Intent intent_map = new Intent(ZDetailsActivityImpl.this, BaiduMap.class);
                intent_map.putExtra("latitude",latitude);
                intent_map.putExtra("longtitude",longtitude);
                startActivity(intent_map);
                break;
            case R.id.imageView_zdetails:
                Intent intent = new Intent(ZDetailsActivityImpl.this, ShowActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
                break;
        }
    }

    public void showShare() {
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();

        // 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
        //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle("分享");
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        oks.setTitleUrl("http://sharesdk.cn");
        // text是分享文本，所有平台都需要这个字段
        oks.setText("我是分享文本");
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl("www.baidu.com");
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment("我是测试评论文本");
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite(getString(R.string.app_name));
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl("http://sharesdk.cn");

        // 启动分享GUI
        oks.show(this);
    }

}
