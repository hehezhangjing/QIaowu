package com.example.administrator.yongleapp.View.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.example.administrator.yongleapp.Base.BaseActivity;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Activity.impl.ViewPagerAdapter;
import com.example.administrator.yongleapp.View.Fragment.OrderFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ThecashcouponActivity extends BaseActivity {


    @Bind(R.id.imageView_back)
    ImageView imageViewBack;
    @Bind(R.id.tabLayout_main)
    TabLayout tabLayoutMain;
    @Bind(R.id.viewPager_main)
    ViewPager viewPagerMain;
    private List<Fragment> totalList = new ArrayList<>();
    @Override
    public int getLayoutId() {
        return R.layout.activity_thecashcoupon;
    }

    @Override
    public void initView() {
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String[]arrTabTitles = getResources().getStringArray(R.array.arrTabTitle2);
        for (int i = 0;i<arrTabTitles.length;i++){
            OrderFragment fragment = OrderFragment.getInstance(i);
            totalList.add(fragment);
        }

        viewPagerMain.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), totalList, arrTabTitles));
        tabLayoutMain.setupWithViewPager(viewPagerMain);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
