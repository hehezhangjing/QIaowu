package com.example.administrator.yongleapp.View.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.yongleapp.Bean.liujiahe.UserBean;
import com.example.administrator.yongleapp.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.sms.BmobSMS;
import cn.bmob.sms.exception.BmobException;
import cn.bmob.sms.listener.RequestSMSCodeListener;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.LogInListener;
import cn.bmob.v3.listener.SaveListener;

public class RegisterActivity extends AppCompatActivity {
    private Context context = this;
    private myCountTimer timer;
    private boolean register = true;
    @Bind(R.id.imageView_register_returen)
    ImageView imageViewRegisterReturen;
    @Bind(R.id.edit_phoneNum)
    TextInputEditText editPhoneNum;
    @Bind(R.id.edit_code)
    EditText editCode;
    @Bind(R.id.textView_getCode)
    TextView textViewGetCode;
    @Bind(R.id.edit_nickName)
    TextInputEditText editNickName;
    @Bind(R.id.edit_psw)
    TextInputEditText editPsw;
    @Bind(R.id.button_regiter)
    Button buttonRegiter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        BmobSMS.initialize(context, "37c5d78a92f9392fd79118294b596d84");
    }

    @OnClick({R.id.imageView_register_returen, R.id.textView_getCode, R.id.button_regiter})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_register_returen:
                RegisterActivity.this.finish();
                break;
            case R.id.textView_getCode:
                requestSMSCode();
                break;
            case R.id.button_regiter:
                final String name = editNickName.getText().toString();
                String code = editCode.getText() + "";
                final String psw = editPsw.getText().toString();
                final String phoen = editPhoneNum.getText() + "";
                if (TextUtils.isEmpty(phoen)) {
                    Toast.makeText(getApplicationContext(),
                            "手机号码不可为空", Toast.LENGTH_LONG).show();
                    return;
                } else if (TextUtils.isEmpty(code)) {
                    Toast.makeText(context,
                            "验证码不可为空", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(name)) {
                    Toast.makeText(context,
                            "昵称不可为空", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(psw)) {
                    Toast.makeText(context,
                            "密码不可为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                final ProgressDialog progress = new ProgressDialog(RegisterActivity.this);
                progress.setMessage("正在验证短信验证码");
                progress.setCanceledOnTouchOutside(false);
                progress.show();

                BmobUser.signOrLoginByMobilePhone(context, phoen, code, new LogInListener<Object>
                        () {


                    @Override
                    public void done(Object o, cn.bmob.v3.exception.BmobException e) {
                        progress.dismiss();
                        if (e == null) {
                            final UserBean user = new UserBean();
                            user.setUsername(name);
                            user.setPassword(psw);
                            user.setMobilePhoneNumber(phoen);
                            BmobQuery<UserBean> bmobQuery = new BmobQuery<UserBean>();
                            bmobQuery.findObjects(context, new FindListener<UserBean>() {
                                @Override
                                public void onSuccess(List<UserBean> list) {
                                    if (list != null) {
                                        for (int i = 0; i < list.size(); i++) {
                                            if (name.equals(list.get(i).getUsername())) {
                                                Toast.makeText(context, "用户名已被注册", Toast
                                                        .LENGTH_SHORT).show();
                                                register = false;
                                            }
                                        }
                                    }
                                    if (register == false) {
                                        user.signUp(context, new SaveListener() {
                                            @Override
                                            public void onSuccess() {
                                                Toast.makeText(RegisterActivity.this, "注册成功", Toast
                                                        .LENGTH_SHORT).show();

                                            }

                                            @Override
                                            public void onFailure(int i, String s) {
                                                Toast.makeText(RegisterActivity.this, "注册失败", Toast
                                                        .LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onError(int i, String s) {
                                    Toast.makeText(RegisterActivity.this, "网络连接失败", Toast
                                            .LENGTH_SHORT).show();

                                }
                            });
                            if (register) {
                                requestSMSCode();
                            }
                        }

                    }
                });

                break;
        }
    }

    private void requestSMSCode() {
        String phoneNum = editPhoneNum.getText() + "";

        if (!TextUtils.isEmpty(phoneNum)) {
            timer = new myCountTimer(60000, 1000);
            timer.start();
            BmobSMS.requestSMSCode(this, phoneNum,
                    "一键注册或登录模板", new RequestSMSCodeListener() {
                        @Override
                        public void done(Integer integer, BmobException e) {
                            if (e == null) {
                                Toast.makeText(RegisterActivity.this,
                                        "验证码发送成功", Toast.LENGTH_SHORT).show();
                            } else {
                                timer.cancel();
                            }
                        }
                    });

        } else {
            Toast.makeText(RegisterActivity.this,
                    "请输入手机号", Toast.LENGTH_SHORT).show();
        }
    }

    class myCountTimer extends CountDownTimer {
        public myCountTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            textViewGetCode.setText((millisUntilFinished / 1000) + "秒后重发");
        }

        @Override
        public void onFinish() {
            textViewGetCode.setText("重新接受验证码");
        }
    }

}
