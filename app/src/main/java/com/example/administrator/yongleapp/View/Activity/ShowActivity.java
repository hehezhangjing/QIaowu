package com.example.administrator.yongleapp.View.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bm.library.PhotoView;
import com.bumptech.glide.Glide;
import com.example.administrator.yongleapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowActivity extends AppCompatActivity {

    @Bind(R.id.iv_show)
    PhotoView ivShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        ButterKnife.bind(this);


        ivShow.enable();
        String url = getIntent().getStringExtra("url");
        Glide.with(this).load(url).into(ivShow);
    }

    @OnClick(R.id.iv_show)
    public void onClick() {
        ShowActivity.this.finish();
    }
}
