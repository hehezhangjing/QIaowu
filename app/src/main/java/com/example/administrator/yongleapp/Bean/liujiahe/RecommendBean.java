package com.example.administrator.yongleapp.Bean.liujiahe;

import java.util.List;

/**
 * Created by Administrator on 2016/7/5.
 */
public class RecommendBean {

    /**
     * code : 0
     * message : 请求成功
     * bonus : 0
     * timestamp : 2016-07-05 09:30:55
     */

    private ResultBean result;
    /**
     * ad : [{"linkid":992367,"remark":"2016北京海洋沙滩狂欢节","orders":1,"picurl":"/upload/2016/06/29/1467195131709_g9z4.jpg","fconfigid":1,"urlType":3},{"linkid":992370,"remark":"2016张北草原音乐节","orders":2,"picurl":"/upload/2016/06/29/1467195151857_j4q8.jpg","fconfigid":1,"urlType":3},{"linkid":964615,"remark":"2016国际冠军杯中国赛北京站   曼彻斯特城队 VS 曼彻斯特联队","orders":3,"picurl":"/upload/2016/07/01/1467344383576_h3y6.jpg","fconfigid":1,"urlType":3},{"linkid":960748,"remark":"张杰2016我想巡回演唱会\u2014北京站","orders":4,"picurl":"/upload/2016/06/17/1466156799364_x5t7.jpg","fconfigid":1,"urlType":3},{"linkid":957162,"remark":"大道文化出品\u2014陈佩斯导演悬疑喜剧《老宅》","orders":5,"picurl":"/upload/2016/06/15/1465960671370_v6k9.jpg","fconfigid":1,"urlType":3},{"linkid":936161,"remark":"CSI平台找错活动","orders":6,"picurl":"/upload/2016/06/01/1464765829011_w1n1.jpg","fconfigid":1,"urlType":1},{"linkid":991505,"remark":"孟京辉作品集","orders":7,"picurl":"/upload/2016/06/29/1467165861932_d5g9.jpg","fconfigid":1,"urlType":2},{"linkid":993910,"remark":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014北京站","orders":8,"picurl":"/upload/2016/06/30/1467266155367_f0v0.jpg","fconfigid":1,"urlType":3}]
     * heavyRecommend : {"linkid":632951,"remark":"Eason降临 朝圣之路不能停","orders":1,"img_url":"/upload/2016/07/04/1467610452389_i6t7_m1.jpg","fconfigid":-1,"urlType":null,"isdisplaytime":0,"active_begindate":1451318400000,"active_enddate":1472659199000,"heavyRecommendForwordType":null,"isShow":1}
     * products : [{"productId":"145107813","name":"2016赛季中超联赛 北京国安VS天津泰达","enddate":"2016-07-09","beginDate":"2016-07-09","finishDate":"2016-07-09","venueId":143638,"path":"http://www.228.com.cn/newonline-6547.html","venueName":"[北京市]工人体育场","status":0,"imgPath":"/upload/2016/07/02/AfterTreatment/1467436599293_z4j1-0.jpg","psmillimg":"/upload/2016/07/02/AfterTreatment/1467436599293_z4j1-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g","minPrice":120,"special":null,"orders":1,"productMarkings":1,"newbegindate":"2016-07-09 19:35"},{"productId":"141189487","name":"浪漫中国\u2014理查德\u2022克莱德曼中国巡演2017北京新春音乐会","enddate":"2017-01-21","beginDate":"2017-01-21","finishDate":"2017-01-21","venueId":143808,"path":null,"venueName":"[北京市]人民大会堂","status":1,"imgPath":"/upload/2016/06/27/AfterTreatment/1467001079921_o3u2-0.jpg","psmillimg":"/upload/2016/06/27/AfterTreatment/1467001079921_o3u2-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":380,"special":"【超值套票】仅￥1000元，即可购买价值1360元双人套票(680*2)<br/>【超值套票】仅￥1500元，即可购买价值2040元家庭套票(680*3)<br/>【超值套票】仅￥1600元，即可购买价值1960元情侣套票(980*2)","orders":2,"productMarkings":0,"newbegindate":"2017-01-21 19:30"},{"productId":"106572314","name":"孟京辉戏剧作品《我爱×××》","enddate":"2016-07-10","beginDate":"2016-07-05","finishDate":"2016-07-10","venueId":143803,"path":null,"venueName":"[北京市]蜂巢剧场","status":0,"imgPath":"/upload/2016/04/26/AfterTreatment/1461653204127_t2m1-0.jpg","psmillimg":"/upload/2016/04/26/AfterTreatment/1461653204127_t2m1-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":50,"special":null,"orders":3,"productMarkings":0,"newbegindate":"2016-07-05至2016-07-10"},{"productId":"106581693","name":"孟京辉经典戏剧作品《恋爱的犀牛》","enddate":"2016-08-07","beginDate":"2016-07-12","finishDate":"2016-08-07","venueId":143803,"path":null,"venueName":"[北京市]蜂巢剧场","status":0,"imgPath":"/upload/2016/04/26/AfterTreatment/1461653552793_c8q4-0.jpg","psmillimg":"/upload/2016/04/26/AfterTreatment/1461653552793_c8q4-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":50,"special":null,"orders":4,"productMarkings":0,"newbegindate":"2016-07-12至2016-08-07"},{"productId":"143710264","name":"2016斯坦科维奇杯洲际篮球赛","enddate":"2016-07-09","beginDate":"2016-07-05","finishDate":"2016-07-09","venueId":11604700,"path":null,"venueName":"[北京市]北京奥体中心体育馆","status":0,"imgPath":"/upload/2016/06/29/AfterTreatment/1467181379550_d7q9-0.jpg","psmillimg":"/upload/2016/06/29/AfterTreatment/1467181379550_d7q9-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":80,"special":null,"orders":5,"productMarkings":0,"newbegindate":" 2016-07-05-15:30 法国VS阿根廷"},{"productId":"138877009","name":"重窥国殇 田沁鑫作品话剧《北京法源寺》","enddate":"2016-11-04","beginDate":"2016-11-02","finishDate":"2016-11-04","venueId":143614,"path":null,"venueName":"[上海市]上海大剧院-大剧场","status":0,"imgPath":"/upload/2016/06/23/AfterTreatment/1466649686502_z4p5-0.jpg","psmillimg":"/upload/2016/06/23/AfterTreatment/1466649686502_z4p5-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":"大剧场","publishchannel":"a,b,c,d,f,g,e","minPrice":80,"special":null,"orders":6,"productMarkings":0,"newbegindate":"2016-11-02至2016-11-04"},{"productId":"133675192","name":"世界风情大马戏朝阳公园暑期亲子动物夏令营","enddate":"2016-07-31","beginDate":"2016-07-16","finishDate":"2016-07-31","venueId":481506,"path":null,"venueName":"[北京市]朝阳公园","status":0,"imgPath":"/upload/2016/06/16/AfterTreatment/1466061497369_w1o0-0.jpg","psmillimg":"/upload/2016/06/16/AfterTreatment/1466061497369_w1o0-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":80,"special":"【超值套票】仅￥450元，即可购买价值560元双人套票(280*2)<br/>【超值套票】仅￥600元，即可购买价值760元双人套票(380*2)<br/>【超值套票】仅￥680元，即可购买价值840元三人套票(280*3)<br/>【超值套票】仅￥900元，即可购买价值1140元三人套票(380*3)","orders":7,"productMarkings":0,"newbegindate":"2016-07-16 10:30"},{"productId":"133335333","name":" 蔡健雅2016巡回演唱会 列穆尼亚 LEMURIA\u2014北京站","enddate":"2016-10-29","beginDate":"2016-10-29","finishDate":"2016-10-29","venueId":143817,"path":null,"venueName":"[北京市]首都体育馆","status":1,"imgPath":"/upload/2016/06/15/AfterTreatment/1465979242388_d7l8-0.jpg","psmillimg":"/upload/2016/06/15/AfterTreatment/1465979242388_d7l8-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":1,"officeName":null,"publishchannel":"a,b,c,d,f,g","minPrice":280,"special":"【温馨提示】本演出将于6月16日10:18开启预订，建议您提前注册、登录永乐票务网站或客户端，便于第一时间秒杀成功；为了保证您能够更顺利购票，请下载最新版本的APP。","orders":8,"productMarkings":0,"newbegindate":"2016-10-29 19:30"},{"productId":"106662892","name":"张杰2016我想巡回演唱会\u2014北京站","enddate":"2016-07-16","beginDate":"2016-07-16","finishDate":"2016-07-16","venueId":143638,"path":null,"venueName":"[北京市]工人体育场","status":0,"imgPath":"/upload/2016/04/27/AfterTreatment/1461719724327_j5x1-0.jpg","psmillimg":"/upload/2016/04/27/AfterTreatment/1461719724327_j5x1-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":1,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":280,"special":"【温馨提示】为满足歌迷的购票需求，特向组委会协调张杰北京演唱会部分赞助商的280元门票，于6月17日（今天）14：00在永乐网客户端独抢，请提前下载永乐客户端参与抢票。 ","orders":9,"productMarkings":0,"newbegindate":"2016-07-16 19:30"},{"productId":"133207901","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014北京站","enddate":"2016-09-03","beginDate":"2016-09-03","finishDate":"2016-09-03","venueId":143512,"path":"http://www.228.com.cn/newonline-6966.html","venueName":"[北京市]北京展览馆剧场","status":0,"imgPath":"/upload/2016/06/15/AfterTreatment/1465964081823_l4y8-0.jpg","psmillimg":"/upload/2016/06/15/AfterTreatment/1465964081823_l4y8-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":1,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":100,"special":"【超值套票】仅￥600元，即可购买价值760元双人套票(380*2)<br/>【超值套票】仅￥900元，即可购买价值1160元双人套票(580*2)<br/>【温馨提示】本演出100、900（580*2）价位暂不支持在线选座购买。","orders":10,"productMarkings":1,"newbegindate":"2016-09-03 19:30"},{"productId":"119588537","name":"全新2.0升级版,裸眼3D超炫视觉,5D全方位魔幻体验舞台剧《仙剑奇侠传》","enddate":"2016-07-16","beginDate":"2016-07-14","finishDate":"2016-07-16","venueId":143645,"path":null,"venueName":"[北京市]保利剧院","status":0,"imgPath":"/upload/2016/05/26/AfterTreatment/1464245386478_a4i9-0.jpg","psmillimg":"/upload/2016/05/26/AfterTreatment/1464245386478_a4i9-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":180,"special":"【超值套票】仅￥1660元，即可购买价值1760元双人套票(880*2)<br/>【购票赠礼】5月26日起，凡购买本演出门票1张，随票附赠舞台剧《仙剑奇侠传I》官方周边「不忘」文件夹1个。多买多赠，送完即止。","orders":11,"productMarkings":0,"newbegindate":"2016-07-14至2016-07-16"},{"productId":"121011626","name":"百老汇票房冠军《STOMP\u2022破铜烂铁》北京站","enddate":"2016-07-17","beginDate":"2016-06-29","finishDate":"2016-07-17","venueId":143642,"path":null,"venueName":"[北京市]解放军歌剧院","status":0,"imgPath":"/upload/2016/05/24/AfterTreatment/1464084560905_n4i5-0.jpg","psmillimg":"/upload/2016/05/24/AfterTreatment/1464084560905_n4i5-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":180,"special":"【超值套票】仅￥800元，即可购买价值960元双人套票(480*2)<br/>【超值套票】仅￥1100元，即可购买价值1360元双人套票(680*2)<br/>【超值套票】仅￥1500元，即可购买价值1760元双人套票(880*2)<br/>【超值套票】仅￥2000元，即可购买价值3240元三人套票(1080*3)","orders":12,"productMarkings":0,"newbegindate":"2016-06-29至2016-07-17"},{"productId":"131103994","name":"\u201c拳力前行\u201dWBO职业拳击系列赛","enddate":"2016-11-05","beginDate":"2016-07-08","finishDate":"2016-11-05","venueId":143709,"path":"http://www.228.com.cn/newonline-7322.html ","venueName":"[北京市]国家游泳中心\u2014水立方-多功能活动中心","status":0,"imgPath":"/upload/2016/06/21/AfterTreatment/1466499070627_j5z7-0.jpg","psmillimg":"/upload/2016/06/21/AfterTreatment/1466499070627_j5z7-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":0,"officeName":"多功能活动中心","publishchannel":"a,b,c,d,f,g,e","minPrice":100,"special":"【温馨提示】电子票验证地点：北一门入口。","orders":13,"productMarkings":1,"newbegindate":"2016-07-08至2016-11-05"},{"productId":"123925050","name":"拉斯维加斯必看大秀《蓝人秀》北京站 Blue Man Group World Tour \u2014Beijing","enddate":"2016-11-13","beginDate":"2016-10-27","finishDate":"2016-11-13","venueId":62285012,"path":null,"venueName":"[北京市]天桥艺术中心-大剧场","status":0,"imgPath":"/upload/2016/07/04/AfterTreatment/1467595971794_u8h5-0.jpg","psmillimg":"/upload/2016/07/04/AfterTreatment/1467595971794_u8h5-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":"大剧场","publishchannel":"a,b,c,d,f,g,e","minPrice":100,"special":"【开票场次】10月27-10月30日（7场）开售。","orders":14,"productMarkings":0,"newbegindate":"2016-10-27至2016-11-13"},{"productId":"106298015","name":"2016周华健今天唱什么 再团圆场北京演唱会","enddate":"2016-09-03","beginDate":"2016-09-03","finishDate":"2016-09-03","venueId":143817,"path":null,"venueName":"[北京市]首都体育馆","status":1,"imgPath":"/upload/2016/04/25/AfterTreatment/1461579933495_o8o3-0.jpg","psmillimg":"/upload/2016/04/25/AfterTreatment/1461579933495_o8o3-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":1,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":280,"special":"1.【温馨提示】本演出将于4月26日11点开启预订，建议您提前注册、登录永乐票务网站或客户端，便于第一时间秒杀成功；为了保证您能够更顺利购票，请下载最新版本的APP。<br>2.【超值套票】仅￥1180元，即可购买价值1360元双人套票(680*2)","orders":16,"productMarkings":0,"newbegindate":"2016-09-03 19:00"},{"productId":"117700446","name":"2016 BTS LIVE <花样年华 on stage: epilogue>in beijing","enddate":"2016-07-23","beginDate":"2016-07-23","finishDate":"2016-07-23","venueId":143817,"path":null,"venueName":"[北京市]首都体育馆","status":0,"imgPath":"/upload/2016/05/19/AfterTreatment/1463643224262_l3a0-0.jpg","psmillimg":"/upload/2016/05/19/AfterTreatment/1463643224262_l3a0-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":1,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":380,"special":null,"orders":18,"productMarkings":0,"newbegindate":"2016-07-23 19:30"},{"productId":"113471047","name":"2016崔健北京演唱会","enddate":"2016-09-30","beginDate":"2016-09-30","finishDate":"2016-09-30","venueId":143638,"path":null,"venueName":"[北京市]工人体育场","status":1,"imgPath":"/upload/2016/06/17/AfterTreatment/1466151770266_w8l8-0.jpg","psmillimg":"/upload/2016/06/17/AfterTreatment/1466151770266_w8l8-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":280,"special":null,"orders":19,"productMarkings":0,"newbegindate":"2016-09-30 19:00"},{"productId":"127739151","name":"大道文化出品\u2014陈佩斯导演悬疑喜剧《老宅》","enddate":"2016-07-31","beginDate":"2016-07-15","finishDate":"2016-07-31","venueId":12098151,"path":"theatre-1018999.html","venueName":"[北京市]北京喜剧院（原东方剧院）","status":0,"imgPath":"/upload/2016/06/07/AfterTreatment/1465264445846_f2r9-0.jpg","psmillimg":"/upload/2016/06/07/AfterTreatment/1465264445846_f2r9-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":100,"special":"【套票优惠】7月15/16/22/23/29/30日（周五、周六场）各档票价尽享第二张半价优惠（680元除外）；<br/>【套票优惠】7月17/20/21/24/27/28/31日（周三、周四、周日场）各档票价买一赠一（680元除外）；<br/>【温馨提示】优惠折扣/套票暂不支持选座购买，请选择相应套票价位、点击「立即购买」购买，各价位对应座位可参考票区图。","orders":22,"productMarkings":1,"newbegindate":"2016-07-15至2016-07-31"},{"productId":"89362799","name":"大型舞台秀《CAVALIA\u2022舞马》","enddate":"2016-07-31","beginDate":"2016-04-28","finishDate":"2016-07-31","venueId":481506,"path":null,"venueName":"[北京市]朝阳公园-东三门停车场","status":0,"imgPath":"/upload/2016/06/13/AfterTreatment/1465810884425_e8f1-0.jpg","psmillimg":"/upload/2016/06/13/AfterTreatment/1465810884425_e8f1-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":"东三门停车场","publishchannel":"a,b,c,d,f,g,e","minPrice":102,"special":"【温馨提示】180/120元的票有部分遮挡；<br/>【超值套票】仅￥2240元，即可购买价值2640元三人套票(880*3)<br/>【超值套票】仅￥2500元，即可购买价值2940元三人套票(980*3)<br/>【超值套票】仅￥3520元，即可购买价值4400元五人套票(880*5)<br/>【超值套票】仅￥3920元，即可购买价值4900元五人套票(980*5)<br/>【超值套票】仅￥3800元，即可购买价值4740元三人套票(1580*3)<br/>【超值套票】仅￥4000元，即可购买价值5040元三人套票(1680*3)<br/>【超值套票】仅￥5900元，即可购买价值7900元五人套票(1580*5)<br/>【超值套票】仅￥6300元，即可购买价值8400元五人套票(1680*5)","orders":23,"productMarkings":0,"newbegindate":"2016-04-28至2016-07-31"},{"productId":"100512070","name":"国家大剧院歌剧节·2016：国家大剧院制作意大利罗西尼歌剧《塞维利亚理发师》","enddate":"2016-07-17","beginDate":"2016-07-13","finishDate":"2016-07-17","venueId":143649,"path":"theatre-1014034.html","venueName":"[北京市]国家大剧院-歌剧院","status":0,"imgPath":"/upload/2016/06/15/AfterTreatment/1465967316330_i8s2-0.jpg","psmillimg":"/upload/2016/06/15/AfterTreatment/1465967316330_i8s2-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":0,"officeName":"歌剧院","publishchannel":"a,b,c,d,f,g,e","minPrice":100,"special":"【套票优惠】本场演出680、600、550三档票价尽享2张85折，3张8折！  <br/>【温馨提示】购优惠折扣/套票暂不支持选座购买，请选择相应套票价位、点击「立即购买」购买，各价位对应座位可参考票区图。","orders":24,"productMarkings":1,"newbegindate":"2016-07-13至2016-07-17"},{"productId":"96602085","name":"云门舞集《水月》北京站","enddate":"2016-08-28","beginDate":"2016-08-25","finishDate":"2016-08-28","venueId":143649,"path":"theatre-1011768.html","venueName":"[北京市]国家大剧院-歌剧院","status":0,"imgPath":"/upload/2016/04/19/AfterTreatment/1461053041509_c6b9-0.jpg","psmillimg":"/upload/2016/04/19/AfterTreatment/1461053041509_c6b9-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":0,"officeName":"歌剧院","publishchannel":"a,b,c,d,f,g,e","minPrice":100,"special":null,"orders":25,"productMarkings":1,"newbegindate":"2016-08-25至2016-08-28"},{"productId":"101091912","name":"加拿大国宝级舞台巨制《大都会》","enddate":"2016-07-30","beginDate":"2016-07-27","finishDate":"2016-07-30","venueId":143645,"path":null,"venueName":"[北京市]保利剧院","status":0,"imgPath":"/upload/2016/04/07/AfterTreatment/1460015246166_n8p6-0.jpg","psmillimg":"/upload/2016/04/07/AfterTreatment/1460015246166_n8p6-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":180,"special":"【超值套票】仅￥1000元，即可购买价值1160元双人套票(580*2)<br/>【超值套票】仅￥1200元，即可购买价值1360元双人套票(680*2)","orders":26,"productMarkings":0,"newbegindate":"2016-07-27至2016-07-30"},{"productId":"119437631","name":"国家大剧院歌剧节\u20222016：国家大剧院制作莫扎特歌剧《费加罗的婚礼》","enddate":"2016-08-07","beginDate":"2016-08-03","finishDate":"2016-08-07","venueId":143649,"path":"theatre-1017653.html","venueName":"[北京市]国家大剧院-戏剧场","status":0,"imgPath":"/upload/2016/06/29/AfterTreatment/1467190127130_e4y5-0.jpg","psmillimg":"/upload/2016/06/29/AfterTreatment/1467190127130_e4y5-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":0,"officeName":"戏剧场","publishchannel":"a,b,c,d,f,g,e","minPrice":180,"special":"【套票优惠】本场演出580、500两档票价尽享同等价位2张85折，3张8折。 <br/>【温馨提示】优惠折扣/套票暂不支持选座购买，请选择相应套票价位、点击「立即购买」购买，各价位对应座位可参考票区图。","orders":27,"productMarkings":1,"newbegindate":"2016-08-03至2016-08-07"},{"productId":"108003878","name":"大型实景互动体验触电·鬼吹灯","enddate":"2016-07-31","beginDate":"2016-04-30","finishDate":"2016-07-31","venueId":71869050,"path":"http://www.228.com.cn/dznewonline-6841.html","venueName":"[北京市]西单大悦城-9层","status":0,"imgPath":"/upload/2016/04/29/AfterTreatment/1461914934186_e1z5-0.jpg","psmillimg":"/upload/2016/04/29/AfterTreatment/1461914934186_e1z5-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":0,"officeName":"9层","publishchannel":"a,b,c,d,f,g","minPrice":80,"special":null,"orders":28,"productMarkings":0,"newbegindate":" 2016-04-30-2016-07-31-假日版"},{"productId":"89133116","name":"周传雄时不知归世界巡回演唱会\u2014北京站","enddate":"2016-09-03","beginDate":"2016-09-03","finishDate":"2016-09-03","venueId":143719,"path":"http://www.228.com.cn/newonline-5959.html","venueName":"[北京市]乐视体育生态中心 LeSports Center","status":0,"imgPath":"/upload/2016/03/02/AfterTreatment/1456908588690_x3n6-0.jpg","psmillimg":"/upload/2016/03/02/AfterTreatment/1456908588690_x3n6-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":1,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":395,"special":"【温馨提示】此演出为四面看台，无内场","orders":29,"productMarkings":1,"newbegindate":"2016-09-03 19:30"},{"productId":"98510977","name":"徐佳莹日全蚀北京演唱会","enddate":"2016-07-09","beginDate":"2016-07-09","finishDate":"2016-07-09","venueId":143512,"path":null,"venueName":"[北京市]北京展览馆剧场","status":0,"imgPath":"/upload/2016/03/28/AfterTreatment/1459138854718_x3k9-0.jpg","psmillimg":"/upload/2016/03/28/AfterTreatment/1459138854718_x3k9-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":280,"special":null,"orders":31,"productMarkings":0,"newbegindate":"2016-07-09 19:30"},{"productId":"96792741","name":"2016如果 田馥甄巡回演唱会PLUS 北京站","enddate":"2016-07-30","beginDate":"2016-07-30","finishDate":"2016-07-30","venueId":143719,"path":null,"venueName":"[北京市]乐视体育生态中心 LeSports Center","status":0,"imgPath":"/upload/2016/03/17/AfterTreatment/1458194230536_a6q5-0.jpg","psmillimg":"/upload/2016/03/17/AfterTreatment/1458194230536_a6q5-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":1,"officeName":null,"publishchannel":"a,b,c,d,f,g,e","minPrice":280,"special":null,"orders":32,"productMarkings":0,"newbegindate":"2016-07-30 19:30"},{"productId":"88872666","name":"易车丨呈现\u2014CSI犯罪现场调查体验之旅","enddate":"2016-07-28","beginDate":"2016-04-23","finishDate":"2016-07-28","venueId":2531000,"path":"http://www.228.com.cn/dznewonline-6327.html","venueName":"[北京市]朝阳北路101号朝阳大悦城（青年路口）-10楼","status":0,"imgPath":"/upload/2016/04/28/AfterTreatment/1461832578039_k0f4-0.jpg","psmillimg":"/upload/2016/04/28/AfterTreatment/1461832578039_k0f4-1.jpg","onlineseat":1,"isrobseat":0,"isRobTicket":0,"officeName":"10楼","publishchannel":"a,b,c,d,f,g,e","minPrice":100,"special":null,"orders":33,"productMarkings":0,"newbegindate":" 2016-04-23-2016-04-30-早鸟票"},{"productId":"85016443","name":"巴洛克时光机\u2014马汉\u2022埃斯法哈尼羽管键琴独奏音乐会","enddate":"2016-09-17","beginDate":"2016-09-17","finishDate":"2016-09-17","venueId":143647,"path":null,"venueName":"[北京市]中山音乐堂","status":0,"imgPath":"/upload/2016/01/19/AfterTreatment/1453192569673_t0g3-0.jpg","psmillimg":"/upload/2016/01/19/AfterTreatment/1453192569673_t0g3-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,e,f,g","minPrice":50,"special":null,"orders":36,"productMarkings":0,"newbegindate":"2016-09-17 19:30"},{"productId":"85026262","name":"挚爱肖邦\u2014肖邦钢琴独奏曲全集音乐会之一","enddate":"2016-09-24","beginDate":"2016-09-24","finishDate":"2016-09-24","venueId":143647,"path":null,"venueName":"[北京市]中山音乐堂","status":0,"imgPath":"/upload/2016/01/19/AfterTreatment/1453195690069_a8q6-0.jpg","psmillimg":"/upload/2016/01/19/AfterTreatment/1453195690069_a8q6-1.jpg","onlineseat":0,"isrobseat":0,"isRobTicket":0,"officeName":null,"publishchannel":"a,b,c,d,e,f,g","minPrice":50,"special":null,"orders":37,"productMarkings":0,"newbegindate":"2016-09-24 19:30"}]
     * subjectList : [{"rownum":1,"linkid":811255,"remark":"下周看什么","img":"/upload/2016/07/05/1467681643261_e0w5.jpg"},{"rownum":2,"linkid":811256,"remark":"疯抢低价票","img":"/upload/2016/07/05/1467681643264_r0c8.jpg"}]
     */

    private DataBean data;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ResultBean {
        private int code;
        private String message;
        private int bonus;
        private String timestamp;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getBonus() {
            return bonus;
        }

        public void setBonus(int bonus) {
            this.bonus = bonus;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

    public static class DataBean {
        /**
         * linkid : 632951
         * remark : Eason降临 朝圣之路不能停
         * orders : 1
         * img_url : /upload/2016/07/04/1467610452389_i6t7_m1.jpg
         * fconfigid : -1
         * urlType : null
         * isdisplaytime : 0
         * active_begindate : 1451318400000
         * active_enddate : 1472659199000
         * heavyRecommendForwordType : null
         * isShow : 1
         */

        private HeavyRecommendBean heavyRecommend;
        /**
         * linkid : 992367
         * remark : 2016北京海洋沙滩狂欢节
         * orders : 1
         * picurl : /upload/2016/06/29/1467195131709_g9z4.jpg
         * fconfigid : 1
         * urlType : 3
         */

        private List<AdBean> ad;
        /**
         * productId : 145107813
         * name : 2016赛季中超联赛 北京国安VS天津泰达
         * enddate : 2016-07-09
         * beginDate : 2016-07-09
         * finishDate : 2016-07-09
         * venueId : 143638
         * path : http://www.228.com.cn/newonline-6547.html
         * venueName : [北京市]工人体育场
         * status : 0
         * imgPath : /upload/2016/07/02/AfterTreatment/1467436599293_z4j1-0.jpg
         * psmillimg : /upload/2016/07/02/AfterTreatment/1467436599293_z4j1-1.jpg
         * onlineseat : 1
         * isrobseat : 0
         * isRobTicket : 0
         * officeName : null
         * publishchannel : a,b,c,d,f,g
         * minPrice : 120
         * special : null
         * orders : 1
         * productMarkings : 1
         * newbegindate : 2016-07-09 19:35
         */

        private List<ProductsBean> products;
        /**
         * rownum : 1
         * linkid : 811255
         * remark : 下周看什么
         * img : /upload/2016/07/05/1467681643261_e0w5.jpg
         */

        private List<SubjectListBean> subjectList;

        public HeavyRecommendBean getHeavyRecommend() {
            return heavyRecommend;
        }

        public void setHeavyRecommend(HeavyRecommendBean heavyRecommend) {
            this.heavyRecommend = heavyRecommend;
        }

        public List<AdBean> getAd() {
            return ad;
        }

        public void setAd(List<AdBean> ad) {
            this.ad = ad;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public List<SubjectListBean> getSubjectList() {
            return subjectList;
        }

        public void setSubjectList(List<SubjectListBean> subjectList) {
            this.subjectList = subjectList;
        }

        public static class HeavyRecommendBean {
            private int linkid;
            private String remark;
            private int orders;
            private String img_url;
            private int fconfigid;
            private Object urlType;
            private int isdisplaytime;
            private long active_begindate;
            private long active_enddate;
            private Object heavyRecommendForwordType;
            private int isShow;

            public int getLinkid() {
                return linkid;
            }

            public void setLinkid(int linkid) {
                this.linkid = linkid;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public int getOrders() {
                return orders;
            }

            public void setOrders(int orders) {
                this.orders = orders;
            }

            public String getImg_url() {
                return img_url;
            }

            public void setImg_url(String img_url) {
                this.img_url = img_url;
            }

            public int getFconfigid() {
                return fconfigid;
            }

            public void setFconfigid(int fconfigid) {
                this.fconfigid = fconfigid;
            }

            public Object getUrlType() {
                return urlType;
            }

            public void setUrlType(Object urlType) {
                this.urlType = urlType;
            }

            public int getIsdisplaytime() {
                return isdisplaytime;
            }

            public void setIsdisplaytime(int isdisplaytime) {
                this.isdisplaytime = isdisplaytime;
            }

            public long getActive_begindate() {
                return active_begindate;
            }

            public void setActive_begindate(long active_begindate) {
                this.active_begindate = active_begindate;
            }

            public long getActive_enddate() {
                return active_enddate;
            }

            public void setActive_enddate(long active_enddate) {
                this.active_enddate = active_enddate;
            }

            public Object getHeavyRecommendForwordType() {
                return heavyRecommendForwordType;
            }

            public void setHeavyRecommendForwordType(Object heavyRecommendForwordType) {
                this.heavyRecommendForwordType = heavyRecommendForwordType;
            }

            public int getIsShow() {
                return isShow;
            }

            public void setIsShow(int isShow) {
                this.isShow = isShow;
            }
        }

        public static class AdBean {
            private int linkid;
            private String remark;
            private int orders;
            private String picurl;
            private int fconfigid;
            private int urlType;

            public int getLinkid() {
                return linkid;
            }

            public void setLinkid(int linkid) {
                this.linkid = linkid;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public int getOrders() {
                return orders;
            }

            public void setOrders(int orders) {
                this.orders = orders;
            }

            public String getPicurl() {
                return picurl;
            }

            public void setPicurl(String picurl) {
                this.picurl = picurl;
            }

            public int getFconfigid() {
                return fconfigid;
            }

            public void setFconfigid(int fconfigid) {
                this.fconfigid = fconfigid;
            }

            public int getUrlType() {
                return urlType;
            }

            public void setUrlType(int urlType) {
                this.urlType = urlType;
            }
        }

        public static class ProductsBean {
            private String productId;
            private String name;
            private String enddate;
            private String beginDate;
            private String finishDate;
            private int venueId;
            private String path;
            private String venueName;
            private int status;
            private String imgPath;
            private String psmillimg;
            private int onlineseat;
            private int isrobseat;
            private int isRobTicket;
            private Object officeName;
            private String publishchannel;
            private int minPrice;
            private Object special;
            private int orders;
            private int productMarkings;
            private String newbegindate;

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEnddate() {
                return enddate;
            }

            public void setEnddate(String enddate) {
                this.enddate = enddate;
            }

            public String getBeginDate() {
                return beginDate;
            }

            public void setBeginDate(String beginDate) {
                this.beginDate = beginDate;
            }

            public String getFinishDate() {
                return finishDate;
            }

            public void setFinishDate(String finishDate) {
                this.finishDate = finishDate;
            }

            public int getVenueId() {
                return venueId;
            }

            public void setVenueId(int venueId) {
                this.venueId = venueId;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public String getVenueName() {
                return venueName;
            }

            public void setVenueName(String venueName) {
                this.venueName = venueName;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getImgPath() {
                return imgPath;
            }

            public void setImgPath(String imgPath) {
                this.imgPath = imgPath;
            }

            public String getPsmillimg() {
                return psmillimg;
            }

            public void setPsmillimg(String psmillimg) {
                this.psmillimg = psmillimg;
            }

            public int getOnlineseat() {
                return onlineseat;
            }

            public void setOnlineseat(int onlineseat) {
                this.onlineseat = onlineseat;
            }

            public int getIsrobseat() {
                return isrobseat;
            }

            public void setIsrobseat(int isrobseat) {
                this.isrobseat = isrobseat;
            }

            public int getIsRobTicket() {
                return isRobTicket;
            }

            public void setIsRobTicket(int isRobTicket) {
                this.isRobTicket = isRobTicket;
            }

            public Object getOfficeName() {
                return officeName;
            }

            public void setOfficeName(Object officeName) {
                this.officeName = officeName;
            }

            public String getPublishchannel() {
                return publishchannel;
            }

            public void setPublishchannel(String publishchannel) {
                this.publishchannel = publishchannel;
            }

            public int getMinPrice() {
                return minPrice;
            }

            public void setMinPrice(int minPrice) {
                this.minPrice = minPrice;
            }

            public Object getSpecial() {
                return special;
            }

            public void setSpecial(Object special) {
                this.special = special;
            }

            public int getOrders() {
                return orders;
            }

            public void setOrders(int orders) {
                this.orders = orders;
            }

            public int getProductMarkings() {
                return productMarkings;
            }

            public void setProductMarkings(int productMarkings) {
                this.productMarkings = productMarkings;
            }

            public String getNewbegindate() {
                return newbegindate;
            }

            public void setNewbegindate(String newbegindate) {
                this.newbegindate = newbegindate;
            }
        }

        public static class SubjectListBean {
            private int rownum;
            private int linkid;
            private String remark;
            private String img;

            public int getRownum() {
                return rownum;
            }

            public void setRownum(int rownum) {
                this.rownum = rownum;
            }

            public int getLinkid() {
                return linkid;
            }

            public void setLinkid(int linkid) {
                this.linkid = linkid;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }
        }
    }
}
