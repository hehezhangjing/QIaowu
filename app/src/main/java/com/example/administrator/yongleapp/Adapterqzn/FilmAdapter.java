package com.example.administrator.yongleapp.Adapterqzn;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.yongleapp.Bean.liujiahe.FilmBean;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.utils.ImageLoader;

import java.util.List;

/**
 * ----------BigGod be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━
 * Administrator on 2016/7/5.
 * Class description:活动页面的adapter
 * Created by:qzn
 * Creation time:2016/7/5.
 * Modified by:qzn
 * Modified time:2016/7/5.
 * Modified remarks:
 */
public class FilmAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context = null;
    private List<FilmBean.DataBean.PromotionLinkListBean> list = null;
    private LayoutInflater inflater = null;
    private OnItemClickedListener onItemClickedListener = null;
    private RecyclerView recyclerView = null;


    public interface OnItemClickedListener {
        void onItemClick(int postion);

    }

    public void setOnItemClickedListener(
            OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public FilmAdapter(Context context, List<FilmBean.DataBean.PromotionLinkListBean> list,
                       RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_fragment_film, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).textView_recyclerview_activity.setText(list.get(position)
                .getRemark());


       String urlImg = "http://static.228.cn"+list.get(position).getImgaltinfo();
        //加载图片
        ImageLoader.getInstance().displayImage(context, urlImg, ((ViewHolder) holder).imageView_recyclerview_activity);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //添加数据
    public void reloadListView(List<FilmBean.DataBean.PromotionLinkListBean> _list, boolean isClear) {
        if (isClear) {
            list.clear();
        }
        list.addAll(_list);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        private ImageView imageView_recyclerview_activity;
        private TextView textView_recyclerview_activity;


        public ViewHolder(View convertView) {
            super(convertView);
            imageView_recyclerview_activity = (ImageView) convertView.findViewById(R.id
                    .imageView_recyclerview_film);
            textView_recyclerview_activity = (TextView) convertView.findViewById(R.id
                    .textView_recyclerview_film);
            convertView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (null != onItemClickedListener) {
                int position = recyclerView.getChildPosition(v);
                onItemClickedListener.onItemClick(position);
            }
        }
    }

}
