package com.example.administrator.yongleapp.Bean.liujiahe;

import java.util.List;

/**
 * ----------BigGod be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━
 * Administrator on 2016/7/5.
 * Class description:活动Bean
 * Created by:qzn
 * Creation time:2016/7/5.
 * Modified by:qzn
 * Modified time:2016/7/5.
 * Modified remarks:
 */
public class FilmBean {


    /**
     * code : 0
     * message : 请求成功
     * bonus : 0
     * timestamp : 2016-07-05 08:50:25
     */

    private ResultBean result;
    /**
     * page : {"nextPageNo":2,"totalPageCount":3,"lastPageNo":3,"totalCount":30,"pageNo":1,
     * "pageSize":10,"firstPageNo":1,"previousPageNo":1}
     * PromotionLinkList : [{"venuesid":null,
     * "text":"\u201c歌神\u201d
     * 张学友是如此令人期待，每一天，都有更多的人被他的歌声感动；每一年，都在期盼再多一首张学友经典，他为华语乐坛创造出太多奇迹，也缔造了无数经典，听张学友，感受现场的震撼！",
     * "imgaltinfo":"/upload/2016/06/14/1465901346119_j7c8_m1.jpg","remark":"歌神张学友，经典来袭",
     * "img":"/upload/2016/07/01/1467355630778_c1e9_m1.jpg","linkType":3,
     * "picurl":"/upload/2016/07/01/1467355630778_c1e9_m1.jpg","isdisplaytime":null,
     * "url":"http://m.228.cn/zhuanti/mb/zhangxueyou614","productid":null,"name":"歌神张学友，经典来袭",
     * "active_enddate":"2016-07-30","linkid":956284,"shareUrl":"http://m.228
     * .cn/zhuanti/mb/zhangxueyou614","active_begindate":"2016-06-14","products":[{"status":1,
     * "newbegindate":null,"productMarkings":0,
     * "imgPath":"/upload/2016/06/28/AfterTreatment/1467115838959_t7q0-0.jpg","name":"2016[A
     * CLASSIC TOUR 学友.经典]世界巡回演唱会\u2014广州站","finishDate":"2016-11-13",
     * "special":"【温馨提示】本演出将于2016年7月4日11:01
     * 开启预订。建议您提前注册、登陆永乐票务网站或客户端，便于第一时间抢票成功；为了保证您能够更顺利购票，请下载最新版本的APP。","beginDate":"2016-11-11",
     * "venueName":"[广州市]广州国际体育演艺中心","venueId":481813,"minPrice":580,"productId":"143017822"},
     * {"status":1,"newbegindate":null,"productMarkings":0,
     * "imgPath":"/upload/2016/06/28/AfterTreatment/1467084900947_n8t4-0.jpg","name":"2016[A
     * CLASSIC TOUR 学友.经典]世界巡回演唱会\u2014深圳站","finishDate":"2016-11-06","special":"【温馨提示】<br/>1
     * .本演出为四面台。<br/>2
     * .本演出将于2016年6月29日11:00开启预订。建议您提前注册、登陆永乐票务网站或客户端，便于第一时间抢票成功；为了保证您能够更顺利购票，请下载最新版本的APP。",
     * "beginDate":"2016-11-04","venueName":"[深圳市]华润深圳湾体育中心-\u201c春茧\u201d体育馆","venueId":659262,
     * "minPrice":380,"productId":"141695368"},{"status":0,"newbegindate":null,
     * "productMarkings":0,"imgPath":"/upload/2016/06/14/AfterTreatment/1465866719145_n8m1-0
     * .jpg","name":"2016[A CLASSIC TOUR 学友.经典]世界巡回演唱会\u2014上海站","finishDate":"2016-11-20",
     * "special":null,"beginDate":"2016-11-18","venueName":"[上海市]上海梅赛德斯-奔驰文化中心","venueId":481411,
     * "minPrice":680,"productId":"131609505"},{"status":0,"newbegindate":null,
     * "productMarkings":0,"imgPath":"/upload/2016/06/18/AfterTreatment/1466220899823_l3d5-0
     * .jpg","name":"2016[A CLASSIC TOUR 学友.经典]世界巡回演唱会\u2014重庆站","finishDate":"2016-10-30",
     * "special":"温馨提示：此项目为四面台。","beginDate":"2016-10-28","venueName":"[重庆市]重庆国际博览中心-中央大厅",
     * "venueId":44993265,"minPrice":480,"productId":"117564124"}],"isShow":1},{"venuesid":null,
     * "text":"今夏IU李智恩给你更多惊喜！为你而唱~多首经典曲目以及精心准备的中文歌，与你共同度过GOOD DAY！\u200b",
     * "imgaltinfo":"/upload/2016/06/24/1466734957247_l3j1_m1.jpg","remark":"甜美暖人的IU李知恩今夏为你而唱",
     * "img":"/upload/2016/06/24/1466734957230_m7d9_m1.jpg","linkType":3,
     * "picurl":"/upload/2016/06/24/1466734957230_m7d9_m1.jpg","isdisplaytime":null,
     * "url":"http://m.228.cn/zhuanti/mb/IU624","productid":null,"name":"甜美暖人的IU李知恩今夏为你而唱",
     * "active_enddate":"2016-07-30","linkid":975014,"shareUrl":"http://m.228
     * .cn/zhuanti/mb/IU624","active_begindate":"2016-06-24","products":[{"status":0,
     * "newbegindate":null,"productMarkings":0,
     * "imgPath":"/upload/2016/06/20/AfterTreatment/1466386071782_m5l1-0.jpg","name":"IU GOOD DAY
     * IN WUHAN","finishDate":"2016-07-31 19:30","special":null,"beginDate":"2016-07-31 19:30",
     * "venueName":"[武汉市]湖北洪山体育馆","venueId":7180650,"minPrice":280,"productId":"135022907"},
     * {"status":0,"newbegindate":null,"productMarkings":0,
     * "imgPath":"/upload/2016/06/21/AfterTreatment/1466476164566_a0m6-0.jpg","name":"IU GOODDAY
     * IN NANJING","finishDate":"2016-07-23 19:30","special":null,"beginDate":"2016-07-23 19:30",
     * "venueName":"[南京市]南京太阳宫剧场 ","venueId":51452126,"minPrice":280,"productId":"117086143"},
     * {"status":1,"newbegindate":null,"productMarkings":0,
     * "imgPath":"/upload/2016/06/20/AfterTreatment/1466398600991_y5v9-0.jpg","name":"IU GOODDAY
     * IN SHENZHEN","finishDate":"2016-08-27 19:30","special":null,"beginDate":"2016-08-27
     * 19:30","venueName":"[深圳市]深圳体育馆","venueId":2273000,"minPrice":380,"productId":"135181640"},
     * {"status":0,"newbegindate":null,"productMarkings":0,
     * "imgPath":"/upload/2016/05/03/AfterTreatment/1462238507585_c1q8-0.jpg","name":"IU GOODDAY
     * IN CHONGQING","finishDate":"2016-07-30 20:00","special":null,"beginDate":"2016-07-30
     * 20:00","venueName":"[重庆市]重庆市人民大礼堂","venueId":143506,"minPrice":380,
     * "productId":"108885978"},{"status":0,"newbegindate":null,"productMarkings":0,
     * "imgPath":"/upload/2016/06/20/AfterTreatment/1466398888313_x0p0-0.jpg","name":"IU GOODDAY
     * IN GUANGZHOU","finishDate":"2016-07-09 20:00","special":null,"beginDate":"2016-07-09
     * 20:00","venueName":"[广州市]中山纪念堂","venueId":481224,"minPrice":380,"productId":"112130568"},
     * {"status":0,"newbegindate":null,"productMarkings":0,
     * "imgPath":"/upload/2016/06/20/AfterTreatment/1466398893968_r0d8-0.jpg","name":"IU GOODDAY
     * IN CHANGSHA","finishDate":"2016-07-16 20:00","special":null,"beginDate":"2016-07-16
     * 20:00","venueName":"[长沙市]长沙中南林业科技大学体育艺术馆","venueId":45010551,"minPrice":280,
     * "productId":"106014890"}],"isShow":1},{"venuesid":null,
     * "text":"一个歌手有几首标志性的作品已经足以被人称道，Eason
     * 拥有的大热金曲不下数十首，无一不是经典。他的歌有一种魔力，或妙趣横生或深情款款，他的歌让我们深陷其中，不能自已。每个人心中都有一首歌，它属于陈奕迅，在他的歌声中寻找自己，发现故事\u2026","imgaltinfo":"/upload/2016/06/22/1466583021564_k9y1_m1.jpg","remark":"Eason降临 朝圣之路不能停","img":"/upload/2016/07/04/1467598532404_f7k5_m1.jpg","linkType":3,"picurl":"/upload/2016/07/04/1467598532404_f7k5_m1.jpg","isdisplaytime":null,"url":"http://m.228.cn/zhuanti/mb/Easonapp","productid":null,"name":"Eason降临 朝圣之路不能停","active_enddate":"2016-08-31","linkid":742130,"shareUrl":"http://m.228.cn/zhuanti/mb/Easonapp","active_begindate":"2015-12-29","products":[{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/07/01/AfterTreatment/1467352129477_p6f8-0.jpg","name":"2016陈奕迅ANOTHER EASONS LIFE演唱会\u2014大连站","finishDate":"2016-10-08 19:30","special":"【温馨提示】<br>1. 本演出将于7月4日12:58开启预订，票价380(看台)为手机端专享，建议您提前注册、登录永乐票务网站或客户端，便于第一时间秒杀成功；为了保证您能够更顺利购票，请下载最新版本的APP；<br>2.由于此项目订单量大，故预售期间不支持合并订单及修改地址配送。","beginDate":"2016-10-08 19:30","venueName":"[大连市]大连体育中心-体育场","venueId":46189957,"minPrice":380,"productId":"144246038"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/05/13/AfterTreatment/1463108308323_k4g4-0.jpg","name":"2016陈奕迅ANOTHER EASONS LIFE演唱会\u2014合肥站","finishDate":"2016-09-24 19:30","special":"【温馨提示】本演出将于6月20日10:18正式开票，并同步开启在线选座。建议您提前注册、登录永乐票务网站或客户端，便于第一时间秒杀成功；为了保证您能够更顺利购票，请下载最新版本的APP。","beginDate":"2016-09-24 19:30","venueName":"[合肥市]合肥体育中心体育场","venueId":2535701,"minPrice":380,"productId":"113763236"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/06/AfterTreatment/1462521988116_m0o2-0.jpg","name":"2016陈奕迅ANOTHER EASONS LIFE演唱会\u2014北京鸟巢站加场","finishDate":"2016-10-21 19:00","special":null,"beginDate":"2016-10-21 19:00","venueName":"[北京市]国家体育场\u2014鸟巢","venueId":143619,"minPrice":380,"productId":"110381502"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/05/10/AfterTreatment/1462874642336_q4o9-0.jpg","name":"2016陈奕迅ANOTHER EASONS LIFE演唱会\u2014南昌站","finishDate":"2016-07-09 19:30","special":"【特惠信息】6月15日起，凡在「永乐票务」购买该演出门票1280/1680价位门票，将随订单附赠「定制笔记本」1本（1笔订单1本），数量有限，送完即止。","beginDate":"2016-07-09 19:30","venueName":"[南昌市]南昌国际体育中心-体育场","venueId":48914221,"minPrice":280,"productId":"111858752"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/23/AfterTreatment/1463995898917_i5g6-0.jpg","name":"2016陈奕迅ANOTHER EASONS LIFE演唱会\u2014深圳站加场","finishDate":"2016-07-17 20:00","special":"【特惠信息】从6月25号起，每天购买门票产生订单金额最高的两位用户。即可获得陈奕迅亲笔签名CD或者亲笔签名海报一份。每天获奖名单请留意官网微博：永乐票务深圳站。奖品有限，送完即止。","beginDate":"2016-07-17 20:00","venueName":"[深圳市]华润深圳湾体育中心-\u201c春茧\u201d体育场","venueId":659262,"minPrice":380,"productId":"119715638"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/12/AfterTreatment/1460427620299_r6h8-0.jpg","name":"2016陈奕迅ANOTHER EASONS LIFE演唱会\u2014深圳站","finishDate":"2016-07-16 20:00","special":"【补票提示】本演出将于2016年5月11日11:00进行补票销售。建议您提前注册、登陆永乐票务网站或客户端，便于第一时间抢票成功；为了保证您能够更顺利购票，请下载最新版本的APP。","beginDate":"2016-07-16 20:00","venueName":"[深圳市]华润深圳湾体育中心-\u201c春茧\u201d体育场","venueId":659262,"minPrice":380,"productId":"102056932"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/09/AfterTreatment/1460180749338_a9y8-0.jpg","name":"2016陈奕迅ANOTHER EASONS LIFE演唱会\u2014苏州站","finishDate":"2016-09-17 19:30","special":"【补票提示】此演出部分价位将于2016年6月30日11:00进行补票销售。建议您提前注册、登录永乐票务网站或客户端，便于第一时间秒杀成功；为了保证您能够更顺利购票，请下载最新版本的APP。","beginDate":"2016-09-17 19:30","venueName":"[苏州市]苏州体育中心-体育场","venueId":47031716,"minPrice":280,"productId":"101620939"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/01/14/AfterTreatment/1452761863397_m1m1-0.jpg","name":"2016陈奕迅ANOTHER EASONS LIFE演唱会\u2014北京鸟巢站","finishDate":"2016-10-22 19:00","special":"【温馨提示】由于演出火爆，第一波门票现已售完，乐乐正在抓紧沟通补票事宜，请您做好缺货登记，后期补票会第一时间以短信或电话的形式通知您，感谢您的配合。","beginDate":"2016-10-22 19:00","venueName":"[北京市]国家体育场\u2014鸟巢","venueId":143619,"minPrice":380,"productId":"76983367"}],"isShow":1},{"venuesid":null,"text":"天太热，约戏正当时！\r\n再也不要压抑你的天性，这里有人有故事有情节~\r\n带上一张票，一场有关情绪的冒险~\r\n一场引人深思的场景，纷纷呈现！","imgaltinfo":"/upload/2016/06/21/1466503436075_v6u6_m1.jpg","remark":"一言不合就约戏，入戏太深看不停","img":"/upload/2016/06/15/1465978749441_r5s3_m1.jpg","linkType":3,"picurl":"/upload/2016/06/15/1465978749441_r5s3_m1.jpg","isdisplaytime":null,"url":"http://m.228.cn/zhuanti/mb/xiju615","productid":null,"name":"一言不合就约戏，入戏太深看不停","active_enddate":"2016-07-30","linkid":957685,"shareUrl":"http://m.228.cn/zhuanti/mb/xiju615","active_begindate":"2016-06-15","products":[{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/29/AfterTreatment/1467190127130_e4y5-0.jpg","name":"国家大剧院歌剧节\u20222016：国家大剧院制作莫扎特歌剧《费加罗的婚礼》","finishDate":"2016-08-07","special":"【套票优惠】本场演出580、500两档票价尽享同等价位2张85折，3张8折。 <br/>【温馨提示】优惠折扣/套票暂不支持选座购买，请选择相应套票价位、点击「立即购买」购买，各价位对应座位可参考票区图。","beginDate":"2016-08-03","venueName":"[北京市]国家大剧院-戏剧场","venueId":143649,"minPrice":180,"productId":"119437631"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/15/AfterTreatment/1465967316330_i8s2-0.jpg","name":"国家大剧院歌剧节·2016：国家大剧院制作意大利罗西尼歌剧《塞维利亚理发师》","finishDate":"2016-07-17","special":"【套票优惠】本场演出680、600、550三档票价尽享2张85折，3张8折！  <br/>【温馨提示】购优惠折扣/套票暂不支持选座购买，请选择相应套票价位、点击「立即购买」购买，各价位对应座位可参考票区图。","beginDate":"2016-07-13","venueName":"[北京市]国家大剧院-歌剧院","venueId":143649,"minPrice":100,"productId":"100512070"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/07/AfterTreatment/1465264445846_f2r9-0.jpg","name":"大道文化出品\u2014陈佩斯导演悬疑喜剧《老宅》","finishDate":"2016-07-31","special":"【套票优惠】7月15/16/22/23/29/30日（周五、周六场）各档票价尽享第二张半价优惠（680元除外）；<br/>【套票优惠】7月17/20/21/24/27/28/31日（周三、周四、周日场）各档票价买一赠一（680元除外）；<br/>【温馨提示】优惠折扣/套票暂不支持选座购买，请选择相应套票价位、点击「立即购买」购买，各价位对应座位可参考票区图。","beginDate":"2016-07-15","venueName":"[北京市]北京喜剧院（原东方剧院）","venueId":12098151,"minPrice":100,"productId":"127739151"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/31/AfterTreatment/1464663971184_i1s7-0.jpg","name":"Live Spectacle《NARUTO-火影忍者-》World Tour中国巡演\u2014上海站","finishDate":"2016-10-30","special":null,"beginDate":"2016-10-22","venueName":"[上海市]艺海剧院","venueId":143741,"minPrice":50,"productId":"118642446"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/21/AfterTreatment/1463798269586_w0d2-0.jpg","name":"Live Spectacle《NARUTO-火影忍者-》World Tour中国巡演\u2014杭州站","finishDate":"2016-11-06","special":null,"beginDate":"2016-11-04","venueName":"[杭州市]杭州大剧院","venueId":45701608,"minPrice":180,"productId":"118795888"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2015/09/06/AfterTreatment/1441510072441_j4w2-0.jpg","name":"托尼奖最佳剧本、劳伦斯奥利弗最佳喜剧奖/英国经典闹剧 《糊涂戏班》","finishDate":"2016-08-28","special":null,"beginDate":"2016-08-04","venueName":"[上海市]话剧艺术中心-艺术剧院","venueId":143714,"minPrice":50,"productId":"71998470"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/03/10/AfterTreatment/1457578460196_d3w3-0.jpg","name":"百老汇音乐剧《我,堂吉诃德》中文版","finishDate":"2016-08-28","special":"【超值套票】仅￥520元，即可购买价值560元双人套票(280*2)","beginDate":"2016-06-23","venueName":"[上海市]ET聚场（原上海共舞台）","venueId":659269,"minPrice":100,"productId":"95535224"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/09/AfterTreatment/1462772659135_r5i6-0.jpg","name":"七幕人生出品国民喜剧百老汇音乐剧《Q大道》中文版","finishDate":"2016-10-30","special":"【超值套票】仅￥520元，即可购买价值576元双人套票(288*2)","beginDate":"2016-10-14","venueName":"[上海市]ET聚场（原上海共舞台）","venueId":659269,"minPrice":100,"productId":"107240808"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/17/AfterTreatment/1463478100036_t5n3-0.jpg","name":"孟京辉戏剧作品《一个陌生女人的来信》","finishDate":"2016-09-04","special":null,"beginDate":"2016-08-09","venueName":"[北京市]蜂巢剧场","venueId":143803,"minPrice":50,"productId":"116768860"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/16/AfterTreatment/1463383642954_t4y2-0.jpg","name":"孟京辉戏剧作品 音乐剧《空中花园谋杀案》","finishDate":"2016-09-25","special":null,"beginDate":"2016-09-13","venueName":"[北京市]蜂巢剧场","venueId":143803,"minPrice":50,"productId":"115593444"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/16/AfterTreatment/1463383160109_i1p7-0.jpg","name":"孟京辉最新戏剧作品《他有两把左轮手枪和黑白相间的眼睛》","finishDate":"2016-10-09","special":null,"beginDate":"2016-09-27","venueName":"[北京市]蜂巢剧场","venueId":143803,"minPrice":50,"productId":"115578995"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/26/AfterTreatment/1461653204127_t2m1-0.jpg","name":"孟京辉戏剧作品《我爱×××》","finishDate":"2016-07-10","special":null,"beginDate":"2016-07-05","venueName":"[北京市]蜂巢剧场","venueId":143803,"minPrice":50,"productId":"106572314"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/12/AfterTreatment/1465723901008_w2b9-0.jpg","name":"以色列盖谢尔剧院 话剧《乡村》上海站","finishDate":"2016-11-05","special":null,"beginDate":"2016-11-04","venueName":"[上海市]东方艺术中心-歌剧厅","venueId":143718,"minPrice":80,"productId":"131095293"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/13/AfterTreatment/1465796983511_m9o2-0.jpg","name":"赤匹江湖首部原创黑色现实主义戏剧《埋葬》","finishDate":"2016-08-21","special":null,"beginDate":"2016-08-10","venueName":"[北京市]\u200b鼓楼西剧场","venueId":49195427,"minPrice":50,"productId":"131022097"}],"isShow":1},{"venuesid":null,"text":"足球，总能在不经意间给你好看，传奇总在瞬间，逆转不无可能！","imgaltinfo":"/upload/2016/06/24/1466748786302_r8x9_m1.jpg","remark":"激情足球，暑你好看！","img":"/upload/2016/06/24/1466748786286_y3a6_m1.jpg","linkType":3,"picurl":"/upload/2016/06/24/1466748786286_y3a6_m1.jpg","isdisplaytime":null,"url":"http://m.228.cn/zhuanti/mb/aiqiusai624","productid":null,"name":"激情足球，暑你好看！","active_enddate":"2016-07-14","linkid":976243,"shareUrl":"http://m.228.cn/zhuanti/mb/aiqiusai624","active_begindate":"2016-06-24","products":[{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/16/AfterTreatment/1466044562823_f8a9-0.jpg","name":"2016国际冠军杯中国赛上海站 曼彻斯特联队 VS 多特蒙德队","finishDate":"2016-07-22 20:00","special":"【温馨提示】此项目上门自取时间：7月5日起！","beginDate":"2016-07-22 20:00","venueName":"[上海市]上海体育场（八万人体育场）","venueId":143735,"minPrice":400,"productId":"127447185"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/16/AfterTreatment/1466044579771_l8f3-0.jpg","name":"2016国际冠军杯中国赛北京站 曼彻斯特城队 VS 曼彻斯特联队","finishDate":"2016-07-25 19:30","special":null,"beginDate":"2016-07-25 19:30","venueName":"[北京市]国家体育场\u2014鸟巢","venueId":143619,"minPrice":400,"productId":"119724529"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/16/AfterTreatment/1466044516908_n0x3-0.jpg","name":"2016国际冠军杯中国赛深圳站 多特蒙德队VS曼彻斯特城队","finishDate":"2016-07-28 19:30","special":"【温馨提示】此项目上门自取时间：7月5日起！","beginDate":"2016-07-28 19:30","venueName":"[深圳市]深圳龙岗大运中心-体育场","venueId":8385050,"minPrice":380,"productId":"132937543"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/23/AfterTreatment/1466648551581_v2x9-0.jpg","name":"2016意大利传奇巨星中国行 荔波站","finishDate":"2016-07-19 19:00","special":null,"beginDate":"2016-07-19 19:00","venueName":"[黔南布依族苗族自治州]荔波县民族体育活动中心","venueId":138877431,"minPrice":280,"productId":"138867461"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/20/AfterTreatment/1466407209289_m3y9-0.jpg","name":"2016亚洲足球俱乐部冠军联赛\u2014淘汰赛 中国上海上港 VS 韩国全北现代","finishDate":"2016-08-23 19:30","special":null,"beginDate":"2016-08-23 19:30","venueName":"[上海市]上海体育场（八万人体育场）","venueId":143735,"minPrice":100,"productId":"135324143"}],"isShow":1},{"venuesid":null,"text":"周杰伦2000年后亚洲流行乐坛最具革命性创作歌手，超强的现场即兴创作，对乐理，乐器的精通，颠覆常人不入俗套的创作思路，使得他的歌曲与任何一种音乐形式都不完全相同，形成极强的个人风格！听周杰伦的歌，听那时的自己，这一次和他一起唱~","imgaltinfo":"/upload/2016/06/21/1466503484763_i1n6_m1.jpg","remark":"每个人心中都有一首周杰伦","img":"/upload/2016/06/20/1466394055981_h9b1_m1.jpg","linkType":3,"picurl":"/upload/2016/06/20/1466394055981_h9b1_m1.jpg","isdisplaytime":null,"url":"http://m.228.cn/zhuanti/mb/zhoujileun223","productid":null,"name":"每个人心中都有一首周杰伦","active_enddate":"2016-07-30","linkid":799249,"shareUrl":"http://m.228.cn/zhuanti/mb/zhoujileun223","active_begindate":"2016-02-23","products":[{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/25/AfterTreatment/1461572281047_f7f3-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会太原站","finishDate":"2016-09-30 19:30","special":"【温馨提示】580看台套票和580看台原价不在同一个区域","beginDate":"2016-09-30 19:30","venueName":"[太原市]山西省体育中心-红灯笼体育场","venueId":51259767,"minPrice":280,"productId":"106167556"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/01/AfterTreatment/1464754373981_f4y3-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会大连站","finishDate":"2016-08-27 19:30","special":"【温馨提示】本演出将于7月5日11:00开启1880价位补票销售，建议您提前注册、登录永乐票务网站或客户端，便于第一时间秒杀成功；为了保证您能够更顺利购票，请下载最新版本的APP。","beginDate":"2016-08-27 19:30","venueName":"[大连市]大连体育中心-体育场","venueId":46189957,"minPrice":380,"productId":"125145250"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/20/AfterTreatment/1466387319717_h3w1-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会福州站","finishDate":"2016-11-05 20:00","special":"【温馨提示】<br/>1.本演出将于6月16日11:00，代主办方开启1980及380元价位少量补票，敬请期待；建议您提前注册、登陆永乐票务网站，便于第一时间抢票成功；为了保证您能够更顺利购票，请下载最新版本的APP。","beginDate":"2016-11-05 20:00","venueName":"[福州市]福州海峡奥林匹克体育中心 -体育场","venueId":54411443,"minPrice":280,"productId":"87963487"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/17/AfterTreatment/1466152921591_t5m1-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会青岛站","finishDate":"2016-09-16 19:30","special":"【温馨提示】本演出将于6月18日11:18开启预订，建议您提前注册、登录永乐票务网站或客户端，便于第一时间秒杀成功；为了保证您能够更顺利购票，请下载最新版本的APP。","beginDate":"2016-09-16 19:30","venueName":"[青岛市]青岛国信体育中心-体育场","venueId":47835043,"minPrice":380,"productId":"86064054"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/02/24/AfterTreatment/1456292146302_n9y6-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会广州站","finishDate":"2016-07-24","special":"【温馨提示】由于演出火爆，第一波门票现已售完，乐乐正在抓紧沟通补票事宜，请您做好缺货登记，后期补票会第一时间以短信或电话的形式通知您，感谢您的配合。","beginDate":"2016-07-22","venueName":"[广州市]广州国际体育演艺中心","venueId":481813,"minPrice":580,"productId":"84697576"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/03/16/AfterTreatment/1458100458829_u5v7-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会郑州站 ","finishDate":"2016-09-24 20:00","special":"【温馨提示】因预订订单较多，开票后会陆续配票发货，请耐心等待。","beginDate":"2016-09-24 20:00","venueName":"[郑州市]河南省体育中心 ","venueId":481524,"minPrice":280,"productId":"85654294"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/27/AfterTreatment/1461747729719_i7q1-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会合肥站","finishDate":"2016-10-22 19:30","special":"【即将开始】乐乐正在努力地准备项目开售事宜，开售时间待定，确定之后将第一时间公布，目前正在接受预订登记，您可以在当前页面填写手机号，开始预订前我们会以短信形式及时通知。","beginDate":"2016-10-22 19:30","venueName":"[合肥市]合肥体育中心-体育场","venueId":5416250,"minPrice":380,"productId":"107222687"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/15/AfterTreatment/1460710053593_i8k6-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会常州站","finishDate":"2016-10-15 19:30","special":"【温馨提示】本演出第一轮抢票已结束，乐乐正积极准备补票事宜，请做缺货登记，后期若补票将第一时间以短信或电话形式通知","beginDate":"2016-10-15 19:30","venueName":"[常州市]常州奥体中心-新城体育场","venueId":4344051,"minPrice":280,"productId":"103149643"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/03/08/AfterTreatment/1457431541436_o8d8-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会北京站","finishDate":"2016-07-10","special":"【温馨提示】由于演出火爆，第一波门票现已售完，乐乐正在抓紧沟通补票事宜，请您做好缺货登记，后期补票会第一时间以短信或电话的形式通知您，感谢您的配合。","beginDate":"2016-07-08","venueName":"[北京市]乐视体育生态中心 LeSports Center","venueId":143719,"minPrice":580,"productId":"83027246"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/03/11/AfterTreatment/1457674925552_p9h9-0.jpg","name":"周杰伦2016「地表最强」世界巡回演唱会长沙站","finishDate":"2016-11-19 19:30","special":"【温馨提示】由于演出火爆，第一波门票现已售完，乐乐正在抓紧沟通补票事宜，请您做好缺货登记，后期补票会第一时间以短信或电话的形式通知您，感谢您的配合。","beginDate":"2016-11-19 19:30","venueName":"[长沙市]长沙贺龙体育中心-体育场","venueId":47429746,"minPrice":280,"productId":"95737681"}],"isShow":1},{"venuesid":null,"text":"1956年9月24日，北京人民艺术剧院在首都剧场上演《日出》，从此首都剧场成为了北京人艺的专属剧场，到今年正好迎来了六十周年。六十年来，这座坐落在王府井大街22号的剧场里不仅上演了一部部作品，更留下了一个个故事。为纪念这段长达一个甲子的不凡历程，北京人艺将从7月8日起，开启一系列纪念演出及活动。为观众带来戏剧享受的同时，也带大家寻找关于自己的戏剧记忆。","imgaltinfo":"/upload/2016/06/14/1465889268856_e4f9_m1.jpg","remark":"首都剧场60年纪念演出","img":"/upload/2016/06/14/1465889268831_z6v9_m1.jpg","linkType":3,"picurl":"/upload/2016/06/14/1465889268831_z6v9_m1.jpg","isdisplaytime":null,"url":"http://m.228.cn/zhuanti/mb/shoudujuchang614","productid":null,"name":"首都剧场60年纪念演出","active_enddate":"2016-07-30","linkid":955704,"shareUrl":"http://m.228.cn/zhuanti/mb/shoudujuchang614","active_begindate":"2016-06-14","products":[{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/13/AfterTreatment/1465806257974_y8i8-0.jpg","name":"首都剧场60年纪念演出|2016首都剧场精品剧目邀请展演|2016北京南锣鼓巷戏剧展演季话剧：《长生》","finishDate":"2016-07-31","special":null,"beginDate":"2016-07-28","venueName":"[北京市]首都剧场","venueId":143648,"minPrice":40,"productId":"131451281"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/13/AfterTreatment/1465805477102_v1k1-0.jpg","name":"首都剧场60年纪念演出|2016首都剧场精品剧目邀请展演话剧：《等待戈多》","finishDate":"2016-07-24","special":null,"beginDate":"2016-07-22","venueName":"[北京市]首都剧场","venueId":143648,"minPrice":40,"productId":"131441187"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/13/AfterTreatment/1465805050996_j1p9-0.jpg","name":"首都剧场60年纪念演出|2016首都剧场精品剧目邀请展演 话剧：《大教堂》","finishDate":"2016-07-10","special":null,"beginDate":"2016-07-08","venueName":"[北京市]首都剧场","venueId":143648,"minPrice":40,"productId":"131436334"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/13/AfterTreatment/1465805575116_d0v0-0.jpg","name":"首都剧场60年纪念演出|2016首都剧场精品剧目邀请展演|2016北京南锣鼓巷戏剧展演季话剧：《榆树下的欲望》","finishDate":"2016-07-13","special":null,"beginDate":"2016-07-12","venueName":"[北京市]首都剧场","venueId":143648,"minPrice":40,"productId":"131445696"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/20/AfterTreatment/1466412226422_p2g9-0.jpg","name":"首都剧场60年纪念演出|2016首都剧场精品剧目邀请展演|2016北京南锣鼓巷戏剧展演季话剧：《一诺千金》","finishDate":"2016-07-17","special":null,"beginDate":"2016-07-16","venueName":"[北京市]首都剧场","venueId":143648,"minPrice":40,"productId":"135426291"}],"isShow":1},{"venuesid":null,"text":"有一种青春叫暴雪，有一种舞台叫游戏，有一种感动叫VGL！人类、星灵、异虫之间的星际战争，魔幻暗黑的西方神话世界，魔兽悲情的英雄故事与哀歌，那时候我们打一天游戏也不觉得累，那是被游戏包围简单而满足的少年时光，是每个角落都充斥着暴雪的青春。","imgaltinfo":"/upload/2016/06/01/1464773556421_b3y0_m1.jpg","remark":"\u200b有一种青春叫暴雪","img":"/upload/2016/06/24/1466750546540_g4g4_m1.jpg","linkType":3,"picurl":"/upload/2016/06/24/1466750546540_g4g4_m1.jpg","isdisplaytime":null,"url":"http://m.228.cn/zhuanti/mb/vgl601","productid":null,"name":"\u200b有一种青春叫暴雪","active_enddate":"2016-07-30","linkid":936315,"shareUrl":"http://m.228.cn/zhuanti/mb/vgl601","active_begindate":"2016-06-01","products":[{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/15/AfterTreatment/1465964081823_l4y8-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014北京站","finishDate":"2016-09-03 19:30","special":"【超值套票】仅￥600元，即可购买价值760元双人套票(380*2)<br/>【超值套票】仅￥900元，即可购买价值1160元双人套票(580*2)<br/>【温馨提示】本演出100、900（580*2）价位暂不支持在线选座购买。","beginDate":"2016-09-03 19:30","venueName":"[北京市]北京展览馆剧场","venueId":143512,"minPrice":100,"productId":"133207901"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/05/31/AfterTreatment/1464682998010_b8t4-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014南京站","finishDate":"2016-09-28 19:30","special":"【超值套票】仅￥1000元，即可购买价值1140元家庭套票(380*3)<br/>【超值套票】仅￥1800元，即可购买价值2040元家庭套票(680*3)<br/>【超值套票】仅￥2300元，即可购买价值2640元家庭套票(880*3)","beginDate":"2016-09-28 19:30","venueName":"[南京市]南京人民大会堂","venueId":481238,"minPrice":180,"productId":"102709631"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/05/31/AfterTreatment/1464682977957_m1c6-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014长沙站","finishDate":"2016-09-09 20:00","special":null,"beginDate":"2016-09-09 20:00","venueName":"[长沙市]湖南大剧院","venueId":45342388,"minPrice":180,"productId":"113393272"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/31/AfterTreatment/1464682988551_c9y5-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014武汉站","finishDate":"2016-09-08 19:30","special":null,"beginDate":"2016-09-08 19:30","venueName":"[武汉市]武汉琴台音乐厅","venueId":7010052,"minPrice":280,"productId":"108931890"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/31/AfterTreatment/1464683007729_m0q6-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014深圳站","finishDate":"2016-09-11 19:30","special":null,"beginDate":"2016-09-11 19:30","venueName":"[深圳市]南山文体中心聚橙剧院-大剧场","venueId":50591100,"minPrice":180,"productId":"101929905"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/31/AfterTreatment/1464682960684_v2r2-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014成都站","finishDate":"2016-09-18 19:30","special":null,"beginDate":"2016-09-18 19:30","venueName":"[成都市]四川省锦城艺术宫","venueId":143604,"minPrice":180,"productId":"122765443"},{"status":1,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/24/AfterTreatment/1466730866674_m8w0-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014上海站","finishDate":"2016-09-04 19:30","special":"【超值套票】仅￥600元，即可购买价值760元双人套票(380*2)<br/>【超值套票】仅￥800元，即可购买价值960元双人套票  (480*2)","beginDate":"2016-09-04 19:30","venueName":"[上海市]上海大舞台（上海体育馆）","venueId":143528,"minPrice":100,"productId":"123681083"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/24/AfterTreatment/1466736393824_b9b4-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014重庆站","finishDate":"2016-09-17 19:30","special":null,"beginDate":"2016-09-17 19:30","venueName":"[重庆市]重庆大剧院-大剧场","venueId":481233,"minPrice":40,"productId":"128880239"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/08/AfterTreatment/1465374174350_j2p2-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014昆明站","finishDate":"2016-09-14 20:00","special":"【超值套票】仅￥680元，即可购买价值760元双人套票(380*2)","beginDate":"2016-09-14 20:00","venueName":"[昆明市]云南省人民政府海埂会堂","venueId":51786594,"minPrice":180,"productId":"125861969"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/31/AfterTreatment/1464682867267_s4b6-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014郑州站 ","finishDate":"2016-09-21 19:30","special":null,"beginDate":"2016-09-21 19:30","venueName":"[郑州市]河南艺术中心-大剧院","venueId":481541,"minPrice":80,"productId":"124107696"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/21/AfterTreatment/1466501727028_y4s6-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014无锡站","finishDate":"2016-10-02 19:30","special":null,"beginDate":"2016-10-02 19:30","venueName":"[无锡市]无锡大剧院-歌剧厅","venueId":11136102,"minPrice":80,"productId":"137080415"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/21/AfterTreatment/1466503338369_i1t5-0.jpg","name":"2016 VIDEO GAMES LIVE 暴雪游戏音乐会\u2014常州站","finishDate":"2016-09-30 19:30","special":"【超值套票】仅￥600元，即可购买价值800元双人套票(400*2)<br/>【超值套票】仅￥1000元，即可购买价值1200元三人套票(400*3)<br/>【超值套票】仅￥1000元，即可购买价值1200元双人套票(600*2)<br/>【超值套票】仅￥1500元，即可购买价值1800元三人套票(600*3)","beginDate":"2016-09-30 19:30","venueName":"[常州市]常州大剧院-大剧场","venueId":45159925,"minPrice":100,"productId":"137153539"}],"isShow":1},{"venuesid":null,"text":"沃尔夫冈·阿玛多伊斯·莫扎特（Wolfgang Amadeus Mozart，1756年1月27日－1791年12月5日），出生于神圣罗马帝国时期的萨尔兹堡。欧洲古典主义音乐作曲家。1760年，4岁的莫扎特开始学习作曲；7岁至17岁的十年时间，莫扎特随父亲列奥波尔得·莫扎特在欧洲各国进行旅行演出；1781年，25岁的莫扎特到维也纳开始10年的创作生涯；1791年，莫扎特逝世，享年35岁，可谓英年早逝。2016年是莫扎特诞辰260周年，让我们以莫扎特之名纪念这位音乐天才。","imgaltinfo":"/upload/2016/06/20/1466410202016_q5o7_m1.jpg","remark":"以莫扎特之名\u2014\u2014纪念莫扎特诞辰260周年","img":"/upload/2016/06/20/1466410201997_n0g2_m1.jpg","linkType":3,"picurl":"/upload/2016/06/20/1466410201997_n0g2_m1.jpg","isdisplaytime":null,"url":"http://m.228.cn/zhuanti/mb/mozart620","productid":null,"name":"以莫扎特之名\u2014\u2014纪念莫扎特诞辰260周年","active_enddate":"2016-07-30","linkid":965307,"shareUrl":"http://m.228.cn/zhuanti/mb/mozart620","active_begindate":"2016-06-20","products":[{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/18/AfterTreatment/1463561825566_a9n9-0.jpg","name":"致敬莫扎特诞辰260周年 年末大戏经典德语音乐剧《莫扎特》","finishDate":"2016-12-23","special":null,"beginDate":"2016-12-13","venueName":"[上海市]上海文化广场","venueId":659259,"minPrice":80,"productId":"117153521"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/29/AfterTreatment/1467190127130_e4y5-0.jpg","name":"国家大剧院歌剧节\u20222016：国家大剧院制作莫扎特歌剧《费加罗的婚礼》","finishDate":"2016-08-07","special":"【套票优惠】本场演出580、500两档票价尽享同等价位2张85折，3张8折。 <br/>【温馨提示】优惠折扣/套票暂不支持选座购买，请选择相应套票价位、点击「立即购买」购买，各价位对应座位可参考票区图。","beginDate":"2016-08-03","venueName":"[北京市]国家大剧院-戏剧场","venueId":143649,"minPrice":180,"productId":"119437631"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/04/28/AfterTreatment/1461832209061_l9l0-0.jpg","name":"国家大剧院2016八月合唱节：德国声音集萃交响乐团及合唱团\u2014莫扎特《安魂曲》音乐会","finishDate":"2016-08-16 19:30","special":null,"beginDate":"2016-08-16 19:30","venueName":"[北京市]国家大剧院-音乐厅","venueId":143649,"minPrice":50,"productId":"107663706"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/24/AfterTreatment/1464059910362_u4x5-0.jpg","name":"莫扎特的音乐传记\u2014萨尔茨堡二重奏音乐会","finishDate":"2016-07-08 19:30","special":null,"beginDate":"2016-07-08 19:30","venueName":"[重庆市]国泰艺术中心-音乐厅","venueId":45216882,"minPrice":80,"productId":"119979541"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/04/26/AfterTreatment/1461642249913_p0g3-0.jpg","name":"\"打开音乐之门\" 2016北京音乐厅暑期系列音乐会 乐魂永恒\u2014纪念莫扎特诞辰260周年作品专场音乐会","finishDate":"2016-07-17 14:00","special":null,"beginDate":"2016-07-17 14:00","venueName":"[北京市]北京音乐厅","venueId":143646,"minPrice":20,"productId":"106499735"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/06/AfterTreatment/1465198280424_d2o5-0.jpg","name":"莫扎特的音乐传记二重奏音乐会","finishDate":"2016-07-21 19:30","special":null,"beginDate":"2016-07-21 19:30","venueName":"[上海市]上海大宁剧院","venueId":482002,"minPrice":20,"productId":"127673985"},{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/05/25/AfterTreatment/1464148995728_l6r1-0.jpg","name":"纪念莫扎特诞辰260周年专题音乐会","finishDate":"2016-07-22 19:30","special":"【超值套票】仅￥180元，即可购买价值240元双人套票(120*2)<br/>【超值套票】仅￥260元，即可购买价值360元双人套票(180*2)","beginDate":"2016-07-22 19:30","venueName":"[芜湖市]芜湖大剧院","venueId":53909912,"minPrice":50,"productId":"121285073"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/05/18/AfterTreatment/1463555826291_s4k4-0.jpg","name":"2016东方市民音乐会\u2022周末版 纪念莫扎特诞辰260周年 歌剧《唐璜》精选片段音乐会","finishDate":"2016-11-19 10:00","special":null,"beginDate":"2016-11-19 10:00","venueName":"[上海市]东方艺术中心-音乐厅","venueId":143718,"minPrice":30,"productId":"117090447"}],"isShow":1},{"venuesid":null,"text":"国际冠军杯中国赛时隔一年再度拉开帷幕，北京、上海、深圳，曼联、曼城、多特蒙德，穆里尼奥、瓜迪奥拉，鲁尼、阿奎罗、罗伊斯···真正的大牌来了，这个夏天，足球不寂寞！","imgaltinfo":"/upload/2016/06/20/1466390990804_q8x2_m1.jpg","remark":"2016国际冠军杯来了，这个夏天足球不寂寞！","img":"/upload/2016/06/20/1466408365775_c9l7_m1.jpg","linkType":3,"picurl":"/upload/2016/06/20/1466408365775_c9l7_m1.jpg","isdisplaytime":null,"url":"http://m.228.cn/zhuanti/mb/2016ICC","productid":null,"name":"2016国际冠军杯来了，这个夏天足球不寂寞！","active_enddate":"2016-07-22","linkid":942416,"shareUrl":"http://m.228.cn/zhuanti/mb/2016ICC","active_begindate":"2016-06-06","products":[{"status":0,"newbegindate":null,"productMarkings":1,"imgPath":"/upload/2016/06/16/AfterTreatment/1466044562823_f8a9-0.jpg","name":"2016国际冠军杯中国赛上海站 曼彻斯特联队 VS 多特蒙德队","finishDate":"2016-07-22 20:00","special":"【温馨提示】此项目上门自取时间：7月5日起！","beginDate":"2016-07-22 20:00","venueName":"[上海市]上海体育场（八万人体育场）","venueId":143735,"minPrice":400,"productId":"127447185"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/16/AfterTreatment/1466044579771_l8f3-0.jpg","name":"2016国际冠军杯中国赛北京站 曼彻斯特城队 VS 曼彻斯特联队","finishDate":"2016-07-25 19:30","special":null,"beginDate":"2016-07-25 19:30","venueName":"[北京市]国家体育场\u2014鸟巢","venueId":143619,"minPrice":400,"productId":"119724529"},{"status":0,"newbegindate":null,"productMarkings":0,"imgPath":"/upload/2016/06/16/AfterTreatment/1466044516908_n0x3-0.jpg","name":"2016国际冠军杯中国赛深圳站 多特蒙德队VS曼彻斯特城队","finishDate":"2016-07-28 19:30","special":"【温馨提示】此项目上门自取时间：7月5日起！","beginDate":"2016-07-28 19:30","venueName":"[深圳市]深圳龙岗大运中心-体育场","venueId":8385050,"minPrice":380,"productId":"132937543"}],"isShow":1}]
     */

    private DataBean data;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ResultBean {
        private int code;
        private String message;
        private int bonus;
        private String timestamp;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getBonus() {
            return bonus;
        }

        public void setBonus(int bonus) {
            this.bonus = bonus;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

    public static class DataBean {
        /**
         * nextPageNo : 2
         * totalPageCount : 3
         * lastPageNo : 3
         * totalCount : 30
         * pageNo : 1
         * pageSize : 10
         * firstPageNo : 1
         * previousPageNo : 1
         */

        private PageBean page;
        /**
         * venuesid : null
         * text :
         * “歌神”张学友是如此令人期待，每一天，都有更多的人被他的歌声感动；每一年，都在期盼再多一首张学友经典，他为华语乐坛创造出太多奇迹，也缔造了无数经典，听张学友，感受现场的震撼！
         * imgaltinfo : /upload/2016/06/14/1465901346119_j7c8_m1.jpg
         * remark : 歌神张学友，经典来袭
         * img : /upload/2016/07/01/1467355630778_c1e9_m1.jpg
         * linkType : 3
         * picurl : /upload/2016/07/01/1467355630778_c1e9_m1.jpg
         * isdisplaytime : null
         * url : http://m.228.cn/zhuanti/mb/zhangxueyou614
         * productid : null
         * name : 歌神张学友，经典来袭
         * active_enddate : 2016-07-30
         * linkid : 956284
         * shareUrl : http://m.228.cn/zhuanti/mb/zhangxueyou614
         * active_begindate : 2016-06-14
         * products : [{"status":1,"newbegindate":null,"productMarkings":0,
         * "imgPath":"/upload/2016/06/28/AfterTreatment/1467115838959_t7q0-0.jpg","name":"2016[A
         * CLASSIC TOUR 学友.经典]世界巡回演唱会\u2014广州站","finishDate":"2016-11-13",
         * "special":"【温馨提示】本演出将于2016年7月4日11:01
         * 开启预订。建议您提前注册、登陆永乐票务网站或客户端，便于第一时间抢票成功；为了保证您能够更顺利购票，请下载最新版本的APP。",
         * "beginDate":"2016-11-11","venueName":"[广州市]广州国际体育演艺中心","venueId":481813,
         * "minPrice":580,"productId":"143017822"},{"status":1,"newbegindate":null,
         * "productMarkings":0,"imgPath":"/upload/2016/06/28/AfterTreatment/1467084900947_n8t4-0
         * .jpg","name":"2016[A CLASSIC TOUR 学友.经典]世界巡回演唱会\u2014深圳站","finishDate":"2016-11-06",
         * "special":"【温馨提示】<br/>1.本演出为四面台。<br/>2
         * .本演出将于2016年6月29日11:00开启预订。建议您提前注册、登陆永乐票务网站或客户端，便于第一时间抢票成功；为了保证您能够更顺利购票，请下载最新版本的APP。",
         * "beginDate":"2016-11-04","venueName":"[深圳市]华润深圳湾体育中心-\u201c春茧\u201d体育馆",
         * "venueId":659262,"minPrice":380,"productId":"141695368"},{"status":0,
         * "newbegindate":null,"productMarkings":0,
         * "imgPath":"/upload/2016/06/14/AfterTreatment/1465866719145_n8m1-0.jpg","name":"2016[A
         * CLASSIC TOUR 学友.经典]世界巡回演唱会\u2014上海站","finishDate":"2016-11-20","special":null,
         * "beginDate":"2016-11-18","venueName":"[上海市]上海梅赛德斯-奔驰文化中心","venueId":481411,
         * "minPrice":680,"productId":"131609505"},{"status":0,"newbegindate":null,
         * "productMarkings":0,"imgPath":"/upload/2016/06/18/AfterTreatment/1466220899823_l3d5-0
         * .jpg","name":"2016[A CLASSIC TOUR 学友.经典]世界巡回演唱会\u2014重庆站","finishDate":"2016-10-30",
         * "special":"温馨提示：此项目为四面台。","beginDate":"2016-10-28","venueName":"[重庆市]重庆国际博览中心-中央大厅",
         * "venueId":44993265,"minPrice":480,"productId":"117564124"}]
         * isShow : 1
         */

        private List<PromotionLinkListBean> PromotionLinkList;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<PromotionLinkListBean> getPromotionLinkList() {
            return PromotionLinkList;
        }

        public void setPromotionLinkList(List<PromotionLinkListBean> PromotionLinkList) {
            this.PromotionLinkList = PromotionLinkList;
        }

        public static class PageBean {
            private int nextPageNo;
            private int totalPageCount;
            private int lastPageNo;
            private int totalCount;
            private int pageNo;
            private int pageSize;
            private int firstPageNo;
            private int previousPageNo;

            public int getNextPageNo() {
                return nextPageNo;
            }

            public void setNextPageNo(int nextPageNo) {
                this.nextPageNo = nextPageNo;
            }

            public int getTotalPageCount() {
                return totalPageCount;
            }

            public void setTotalPageCount(int totalPageCount) {
                this.totalPageCount = totalPageCount;
            }

            public int getLastPageNo() {
                return lastPageNo;
            }

            public void setLastPageNo(int lastPageNo) {
                this.lastPageNo = lastPageNo;
            }

            public int getTotalCount() {
                return totalCount;
            }

            public void setTotalCount(int totalCount) {
                this.totalCount = totalCount;
            }

            public int getPageNo() {
                return pageNo;
            }

            public void setPageNo(int pageNo) {
                this.pageNo = pageNo;
            }

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public int getFirstPageNo() {
                return firstPageNo;
            }

            public void setFirstPageNo(int firstPageNo) {
                this.firstPageNo = firstPageNo;
            }

            public int getPreviousPageNo() {
                return previousPageNo;
            }

            public void setPreviousPageNo(int previousPageNo) {
                this.previousPageNo = previousPageNo;
            }
        }

        public static class PromotionLinkListBean {
            private Object venuesid;
            private String text;
            private String imgaltinfo;
            private String remark;
            private String img;
            private int linkType;
            private String picurl;
            private Object isdisplaytime;
            private String url;
            private Object productid;
            private String name;
            private String active_enddate;
            private int linkid;
            private String shareUrl;
            private String active_begindate;
            private int isShow;
            /**
             * status : 1
             * newbegindate : null
             * productMarkings : 0
             * imgPath : /upload/2016/06/28/AfterTreatment/1467115838959_t7q0-0.jpg
             * name : 2016[A CLASSIC TOUR 学友.经典]世界巡回演唱会—广州站
             * finishDate : 2016-11-13
             * special :
             * 【温馨提示】本演出将于2016年7月4日11:01
             * 开启预订。建议您提前注册、登陆永乐票务网站或客户端，便于第一时间抢票成功；为了保证您能够更顺利购票，请下载最新版本的APP。
             * beginDate : 2016-11-11
             * venueName : [广州市]广州国际体育演艺中心
             * venueId : 481813
             * minPrice : 580
             * productId : 143017822
             */

            private List<ProductsBean> products;

            public Object getVenuesid() {
                return venuesid;
            }

            public void setVenuesid(Object venuesid) {
                this.venuesid = venuesid;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public String getImgaltinfo() {
                return imgaltinfo;
            }

            public void setImgaltinfo(String imgaltinfo) {
                this.imgaltinfo = imgaltinfo;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public int getLinkType() {
                return linkType;
            }

            public void setLinkType(int linkType) {
                this.linkType = linkType;
            }

            public String getPicurl() {
                return picurl;
            }

            public void setPicurl(String picurl) {
                this.picurl = picurl;
            }

            public Object getIsdisplaytime() {
                return isdisplaytime;
            }

            public void setIsdisplaytime(Object isdisplaytime) {
                this.isdisplaytime = isdisplaytime;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public Object getProductid() {
                return productid;
            }

            public void setProductid(Object productid) {
                this.productid = productid;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getActive_enddate() {
                return active_enddate;
            }

            public void setActive_enddate(String active_enddate) {
                this.active_enddate = active_enddate;
            }

            public int getLinkid() {
                return linkid;
            }

            public void setLinkid(int linkid) {
                this.linkid = linkid;
            }

            public String getShareUrl() {
                return shareUrl;
            }

            public void setShareUrl(String shareUrl) {
                this.shareUrl = shareUrl;
            }

            public String getActive_begindate() {
                return active_begindate;
            }

            public void setActive_begindate(String active_begindate) {
                this.active_begindate = active_begindate;
            }

            public int getIsShow() {
                return isShow;
            }

            public void setIsShow(int isShow) {
                this.isShow = isShow;
            }

            public List<ProductsBean> getProducts() {
                return products;
            }

            public void setProducts(List<ProductsBean> products) {
                this.products = products;
            }

            public static class ProductsBean {
                private int status;
                private Object newbegindate;
                private int productMarkings;
                private String imgPath;
                private String name;
                private String finishDate;
                private String special;
                private String beginDate;
                private String venueName;
                private int venueId;
                private int minPrice;
                private String productId;

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }

                public Object getNewbegindate() {
                    return newbegindate;
                }

                public void setNewbegindate(Object newbegindate) {
                    this.newbegindate = newbegindate;
                }

                public int getProductMarkings() {
                    return productMarkings;
                }

                public void setProductMarkings(int productMarkings) {
                    this.productMarkings = productMarkings;
                }

                public String getImgPath() {
                    return imgPath;
                }

                public void setImgPath(String imgPath) {
                    this.imgPath = imgPath;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getFinishDate() {
                    return finishDate;
                }

                public void setFinishDate(String finishDate) {
                    this.finishDate = finishDate;
                }

                public String getSpecial() {
                    return special;
                }

                public void setSpecial(String special) {
                    this.special = special;
                }

                public String getBeginDate() {
                    return beginDate;
                }

                public void setBeginDate(String beginDate) {
                    this.beginDate = beginDate;
                }

                public String getVenueName() {
                    return venueName;
                }

                public void setVenueName(String venueName) {
                    this.venueName = venueName;
                }

                public int getVenueId() {
                    return venueId;
                }

                public void setVenueId(int venueId) {
                    this.venueId = venueId;
                }

                public int getMinPrice() {
                    return minPrice;
                }

                public void setMinPrice(int minPrice) {
                    this.minPrice = minPrice;
                }

                public String getProductId() {
                    return productId;
                }

                public void setProductId(String productId) {
                    this.productId = productId;
                }
            }
        }
    }
}
