package com.example.administrator.yongleapp.View.Fragment.zhangjing;

import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ClassifyBean;

import java.util.List;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 14:09
 * 类描述：
 */
public interface AllFragmentView {
    void addInfo(List<ClassifyBean.DataBean.RecordsBean> infoList);
}
