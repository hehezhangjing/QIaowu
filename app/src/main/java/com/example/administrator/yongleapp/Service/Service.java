package com.example.administrator.yongleapp.Service;

import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ClassifyBean;
import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ZDetailsBean;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 13:24
 * 类描述：
 */
public interface Service {
    @GET("products/query?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&time_range=&sort_type=1&page_size=10&nc=300")
    Call<ClassifyBean> getClassifyBean(@Query("product_category_id") String id,@Query("page_no")int page);

    @GET("products/{type_id}?access_phone_type=android&app_version=My4yLjM%3D&token=&res_type=2")
    Call<ZDetailsBean> getZDetailsData(@Path("type_id") String type_id);

    //145107813
}
