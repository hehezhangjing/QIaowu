package com.example.administrator.yongleapp.Interfaces;


import com.example.administrator.yongleapp.Bean.liujiahe.RecommendBean;
import com.example.administrator.yongleapp.Medol.liujiahe.RecommendMedol;
import com.example.administrator.yongleapp.utils.liujiaheConstant;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by StevenWang on 16/6/22.
 */
public interface ServiceInterface {

    @GET("index?access_phone_type=android&app_version=My4yLjM%3D&token=&site_id=1&nc=60")
    Call<RecommendBean> getInfoList();
}
