package com.example.administrator.yongleapp.View.Fragment.zhangjing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.administrator.yongleapp.Base.BaseFragment;
import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ClassifyBean;
import com.example.administrator.yongleapp.Presenter.liujiahe.zhangjing.AllFPresenterImpl;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.View.Activity.impl.ZDetailsActivityImpl;
import com.example.administrator.yongleapp.adapter.AllFragmentAdapter;
import com.example.administrator.yongleapp.utils.BaseRcQuickAdapter;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 14:09
 * 类描述：
 */
public class AllFragment extends BaseFragment implements AllFragmentView {
    @Bind(R.id.x_fragment_all)
    XRecyclerView xFragmentAll;
    private int curPager = 1;
    private int tabIndex;
    private AllFPresenterImpl allFPresenter = null;
    private List<ClassifyBean.DataBean.RecordsBean> totalList = new ArrayList<>();
    private AllFragmentAdapter adapter = null;
    private String[] arrTabIndex = new String[]{"142450", "142450", "142452", "142452", "142453",
            "142454", "142455", "142458", "142456"};


    //单例模式
    public static AllFragment newInstance(int tabIndex) {
        AllFragment fragment = new AllFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("tabIndex", tabIndex);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void addInfo(List<ClassifyBean.DataBean.RecordsBean> infoList) {
        if (adapter == null) {
            totalList.addAll(infoList);
            adapter = new AllFragmentAdapter(getContext(), R.layout.item_listview_fragment);
            adapter.addAll(totalList);
            xFragmentAll.setAdapter(adapter);
        }else {
            totalList.addAll(infoList);
            adapter.addAll(totalList);
            adapter.notifyDataSetChanged();
        }


        adapter.setOnItemClickListener(new BaseRcQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), ZDetailsActivityImpl.class);
                intent.putExtra("type_id", totalList.get(position).getProductId());
                startActivity(intent);
            }
        });

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_all;
    }

    @Override
    public void initView() {
        tabIndex = getArguments().getInt("tabIndex");
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        xFragmentAll.setLayoutManager(layoutManager);
        xFragmentAll.setRefreshProgressStyle(ProgressStyle.SquareSpin);
        xFragmentAll.setRefreshing(true);
        xFragmentAll.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                loadNetWork();
                xFragmentAll.refreshComplete();
            }

            @Override
            public void onLoadMore() {
                curPager++;
                loadNetWork();
                xFragmentAll.loadMoreComplete();
            }
        });
        loadNetWork();
        xFragmentAll.refreshComplete();


    }

    private void loadNetWork() {
        allFPresenter = new AllFPresenterImpl(this);
        allFPresenter.loadInfo(arrTabIndex[tabIndex], curPager);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        Log.i("TAG", "--->>> " + "onCreateView");
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
