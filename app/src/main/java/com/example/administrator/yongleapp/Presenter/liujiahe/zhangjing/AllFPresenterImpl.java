package com.example.administrator.yongleapp.Presenter.liujiahe.zhangjing;

import com.example.administrator.yongleapp.Bean.liujiahe.zhangjing.ClassifyBean;
import com.example.administrator.yongleapp.Medol.liujiahe.zhangjing.AllModel;
import com.example.administrator.yongleapp.Medol.liujiahe.zhangjing.AllModelImpl;
import com.example.administrator.yongleapp.View.Fragment.zhangjing.AllFragmentView;

import java.util.List;

/**
 * │＼＿＿╭╭╭╭╭＿＿／│
 * │　　　　 GOD  　　　　│
 * │　　　　　　　　　　　│
 * │　－　　　　　　　－　│
 * │　≡　　　o　　　  ≡  │
 * │　　　　　　　　　　　│
 * ╰——┬ O◤▽◥O ┬——╯
 * 　　｜　　 o　  ｜
 * 　　｜╭－－－╮｜
 * ————————————————————
 * 项目名称：QIaowu
 * BY：zzZ ON 2016/7/8 14:08
 * 类描述：
 */
public class AllFPresenterImpl implements AllFPresenter {
    private AllFragmentView allFragmentView;
    private AllModel allModel;

    public AllFPresenterImpl(AllFragmentView allFragmentView) {
        this.allFragmentView = allFragmentView;
        this.allModel = new AllModelImpl();
    }

    @Override
    public void loadInfo(String id, int pager) {
        allModel.loadInfo(id, pager, new AllModelImpl.OnLoadInfoListListener() {
            @Override
            public void onSuccess(List<ClassifyBean.DataBean.RecordsBean> list) {
                allFragmentView.addInfo(list);
            }

            @Override
            public void onFailure(String msg, Exception ex) {

            }
        });
    }
}
