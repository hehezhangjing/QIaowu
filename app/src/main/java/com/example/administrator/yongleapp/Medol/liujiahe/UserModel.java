package com.example.administrator.yongleapp.Medol.liujiahe;

import android.content.Context;

import com.example.administrator.yongleapp.Bean.liujiahe.UserBean;

/**
 * ----------BigGod be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━
 * Administrator on 2016/7/9.
 * Class description
 * Created by:qzn
 * Creation time:2016/7/9.
 * Modified by:qzn
 * Modified time:2016/7/9.
 * Modified remarks:
 */
public interface UserModel {
    void login(UserBean user, Context context,UserModelImpl.UserLitster lister);

    void register(UserBean user,Context context, UserModelImpl.UserLitster lister);
}
