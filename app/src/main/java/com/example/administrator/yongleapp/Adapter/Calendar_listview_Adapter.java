package com.example.administrator.yongleapp.Adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.administrator.yongleapp.Base.BaseRcAdapterHelper;
import com.example.administrator.yongleapp.Base.BaseRcQuickAdapter;
import com.example.administrator.yongleapp.Bean.liujiahe.CalendarBean;
import com.example.administrator.yongleapp.Bean.liujiahe.RankBean;
import com.example.administrator.yongleapp.R;
import com.example.administrator.yongleapp.utils.liujiaheConstant;

/**
 * Created by Administrator on 2016/7/7.
 */
public class Calendar_listview_Adapter extends BaseRcQuickAdapter<CalendarBean.DataBean.RecordsBean, BaseRcAdapterHelper> {

    public Calendar_listview_Adapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    protected void convert(final BaseRcAdapterHelper helper, CalendarBean.DataBean.RecordsBean item) {

            ImageView imageView = helper.getImageView(R.id.image_recommend_list);
            TextView textView1 = helper.getTextView(R.id.Text1_recommend_list);
            TextView textView2 = helper.getTextView(R.id.Text2_recommend_list);
            TextView textView3 = helper.getTextView(R.id.Text3_recommend_list);
            TextView textView4 = helper.getTextView(R.id.Text4_recommend_list);

            textView1.setText(item.getName());
            if (item.getBeginDate().equals(item.getFinishDate())) {
                textView2.setText(item.getBeginDate());
            } else {
                textView2.setText(item.getBeginDate() + "至" + item.getFinishDate());
            }
            textView3.setText(item.getVenueName());
            textView4.setText(item.getMinPrice() + "元起");
            Glide.with(context).load(liujiaheConstant.URL + item.getImgPath())
                    .placeholder(R.drawable.img_product_default_bg)
                    .error(R.drawable.img_product_default_bg)
                    .into(imageView);


    }
}
